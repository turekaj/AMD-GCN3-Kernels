
#define VSWAP4(x)    ((((x) >> 24) & 0xFFU) | (((x) >> 8) & 0xFF00U) | (((x) << 8) & 0xFF0000U) | (((x) << 24) & 0xFF000000U))

typedef union {
  unsigned char h1[32];
  uint h4[8];
  ulong h8[4];
} hash_t;

typedef unsigned long sph_u64;

#define shl(x, n)            ((x) << (n))
#define shr(x, n)            ((x) >> (n))
#define SPH_ROTL32(x,n) rotate(x,(uint)n)
#define SPH_ROTL64(x,n) rotate(x,(ulong)n)
#define SPH_ROTR64(x,n) rotate(x,(ulong)(64-n))
#define SPH_C64(x)    ((sph_u64)(x ## UL))
#define SPH_T64(x)    ((x) & SPH_C64(0xFFFFFFFFFFFFFFFF))
#define SWAP4(x) as_uint(as_uchar4(x).wzyx)

static const __constant uint BLAKE256_CONSTS[16] =
{
    0x243F6A88U, 0x85A308D3U,
    0x13198A2EU, 0x03707344U,
    0xA4093822U, 0x299F31D0U,
    0x082EFA98U, 0xEC4E6C89U,
    0x452821E6U, 0x38D01377U,
    0xBE5466CFU, 0x34E90C6CU,
    0xC0AC29B7U, 0xC97C50DDU,
    0x3F84D5B5U, 0xB5470917U
};

static const __constant uint BLAKE256_SIGMA[16][16] =
{
    { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 },
    { 14, 10, 4, 8, 9, 15, 13, 6, 1, 12, 0, 2, 11, 7, 5, 3 },
    { 11, 8, 12, 0, 5, 2, 15, 13, 10, 14, 3, 6, 7, 1, 9, 4 },
    { 7, 9, 3, 1, 13, 12, 11, 14, 2, 6, 5, 10, 4, 0, 15, 8 },
    { 9, 0, 5, 7, 2, 4, 10, 15, 14, 1, 11, 12, 6, 8, 3, 13 },
    { 2, 12, 6, 10, 0, 11, 8, 3, 4, 13, 7, 5, 15, 14, 1, 9 },
    { 12, 5, 1, 15, 14, 13, 4, 10, 0, 7, 6, 3, 9, 2, 8, 11 },
    { 13, 11, 7, 14, 12, 1, 3, 9, 5, 0, 15, 4, 8, 6, 2, 10 },
    { 6, 15, 14, 9, 11, 3, 0, 8, 12, 2, 13, 7, 1, 4, 10, 5 },
    { 10, 2, 8, 4, 7, 6, 1, 5, 15, 11, 9, 14, 3, 12, 13, 0 },
    { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 },
    { 14, 10, 4, 8, 9, 15, 13, 6, 1, 12, 0, 2, 11, 7, 5, 3 },
    { 11, 8, 12, 0, 5, 2, 15, 13, 10, 14, 3, 6, 7, 1, 9, 4 },
    { 7, 9, 3, 1, 13, 12, 11, 14, 2, 6, 5, 10, 4, 0, 15, 8 },
    { 9, 0, 5, 7, 2, 4, 10, 15, 14, 1, 11, 12, 6, 8, 3, 13 },
    { 2, 12, 6, 10, 0, 11, 8, 3, 4, 13, 7, 5, 15, 14, 1, 9 }
};

// x should be 8 for the odd passes
#define BLAKE256_G_PARALLEL(st0, st1, st2, st3, rnd, x) { \
    st0 += st1 + ((uint4)(M[BLAKE256_SIGMA[rnd][0 + x]], M[BLAKE256_SIGMA[rnd][2 + x]], M[BLAKE256_SIGMA[rnd][4 + x]], M[BLAKE256_SIGMA[rnd][6 + x]]) ^ (uint4)(BLAKE256_CONSTS[BLAKE256_SIGMA[rnd][1 + x]], BLAKE256_CONSTS[BLAKE256_SIGMA[rnd][3 + x]], BLAKE256_CONSTS[BLAKE256_SIGMA[rnd][5 + x]], BLAKE256_CONSTS[BLAKE256_SIGMA[rnd][7 + x]])); \
    st3 = rotate(st0 ^ st3, 16U); \
    st2 += st3; \
    st1 = rotate(st2 ^ st1, 20U); \
    st0 += st1 + ((uint4)(M[BLAKE256_SIGMA[rnd][1 + x]], M[BLAKE256_SIGMA[rnd][3 + x]], M[BLAKE256_SIGMA[rnd][5 + x]], M[BLAKE256_SIGMA[rnd][7 + x]]) ^ (uint4)(BLAKE256_CONSTS[BLAKE256_SIGMA[rnd][0 + x]], BLAKE256_CONSTS[BLAKE256_SIGMA[rnd][2 + x]], BLAKE256_CONSTS[BLAKE256_SIGMA[rnd][4 + x]], BLAKE256_CONSTS[BLAKE256_SIGMA[rnd][6 + x]])); \
    st3 = rotate(st0 ^ st3, 24U); \
    st2 += st3; \
    st1 = rotate(st2 ^ st1, 25U); \
}

__attribute__((reqd_work_group_size(64, 1, 1)))
__kernel void blake256(__global const uint *input, __global hash_t *hashes)
{
    uint M[16];
    uint4 st[4];
    uint gid = get_global_id(0);
    uint idx = gid - get_global_offset(0);

    ((uint8 *)st)[0] = vload8(0U, input);
    ((uint8 *)st)[1] = vload8(0U, BLAKE256_CONSTS);
    st[3].s0 ^= 640U;
    st[3].s1 ^= 640U;

    ((uint16 *)M)[0] = (uint16)(input[8], input[9], input[10], as_uint(as_uchar4(gid).s3210), 0x80000000U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 1U, 0U, 640U);
    //((uint16 *)M)[0] = (uint16)(input[8], input[9], input[10], input[11], 0x80000000U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 1U, 0U, 640U);

    
    #pragma unroll
    for(int i = 0; i < 14; ++i)
    {
        BLAKE256_G_PARALLEL(st[0], st[1], st[2], st[3], i, 0);
        BLAKE256_G_PARALLEL(st[0], st[1].s1230, st[2].s2301, st[3].s3012, i, 8);
    }
    
    vstore8(VSWAP4(((uint8 *)st)[0] ^ vload8(0U, input) ^ ((uint8 *)st)[1]), 0U, hashes[idx].h4);
    barrier(CLK_GLOBAL_MEM_FENCE);
//    printf("%s NONCE = %08x 0 = %08x 1 = %08x 2 = %08x 3 = %08x 4 = %08x 5 = %08x 6 = %08x 7 = %08x\n", __func__, gid, hashes[idx].h4[0], hashes[idx].h4[1], hashes[idx].h4[2], hashes[idx].h4[3], hashes[idx].h4[4], hashes[idx].h4[5], hashes[idx].h4[6], hashes[idx].h4[7]);
}




__constant static const ulong RC[] = {
  0x0000000000000001, 0x0000000000008082,
  0x800000000000808A, 0x8000000080008000,
  0x000000000000808B, 0x0000000080000001,
  0x8000000080008081, 0x8000000000008009,
  0x000000000000008A, 0x0000000000000088,
  0x0000000080008009, 0x000000008000000A,
  0x000000008000808B, 0x800000000000008B,
  0x8000000000008089, 0x8000000000008003,
  0x8000000000008002, 0x8000000000000080,
  0x000000000000800A, 0x800000008000000A,
  0x8000000080008081, 0x8000000000008080,
  0x0000000080000001, 0x8000000080008008
};

__attribute__((reqd_work_group_size(64, 1, 1)))
__kernel void keccak256( __global hash_t * hashes)
{
  uint gid = get_global_id(0);
  uint idx = gid - get_global_offset(0);

   __global hash_t *hash = &hashes[idx];

    ulong keccak_gpu_state[25] = {0};


    keccak_gpu_state[0] = hash->h8[0];
    keccak_gpu_state[1] = hash->h8[1];
    keccak_gpu_state[2] = hash->h8[2];
    keccak_gpu_state[3] = hash->h8[3];
	keccak_gpu_state[4] = 0x0000000000000001;
	keccak_gpu_state[16] = 0x8000000000000000;

	ulong t[5], u[5], v, w;

    
    #pragma unroll
	for (size_t i = 0; i < 24; i++) 
    {
	    /* theta: c = a[0,i] ^ a[1,i] ^ .. a[4,i] */

        #pragma unroll
        for (int x = 0; x < 5; ++x)
        {
            t[x] = keccak_gpu_state[x] ^ keccak_gpu_state[x + 5] ^ keccak_gpu_state[x + 10] ^ keccak_gpu_state[x + 15] ^ keccak_gpu_state[x + 20];
        }

		/* theta: d[i] = c[i+4] ^ rotl(c[i+1],1) */
		u[0] = t[4] ^ rotate(t[1], (ulong) 1);
		u[1] = t[0] ^ rotate(t[2], (ulong) 1);
		u[2] = t[1] ^ rotate(t[3], (ulong) 1);
		u[3] = t[2] ^ rotate(t[4], (ulong) 1);
		u[4] = t[3] ^ rotate(t[0], (ulong) 1);

		/* theta: a[0,i], a[1,i], .. a[4,i] ^= d[i] */
		keccak_gpu_state[0] ^= u[0]; keccak_gpu_state[5] ^= u[0]; keccak_gpu_state[10] ^= u[0]; keccak_gpu_state[15] ^= u[0]; keccak_gpu_state[20] ^= u[0];
		keccak_gpu_state[1] ^= u[1]; keccak_gpu_state[6] ^= u[1]; keccak_gpu_state[11] ^= u[1]; keccak_gpu_state[16] ^= u[1]; keccak_gpu_state[21] ^= u[1];
		keccak_gpu_state[2] ^= u[2]; keccak_gpu_state[7] ^= u[2]; keccak_gpu_state[12] ^= u[2]; keccak_gpu_state[17] ^= u[2]; keccak_gpu_state[22] ^= u[2];
		keccak_gpu_state[3] ^= u[3]; keccak_gpu_state[8] ^= u[3]; keccak_gpu_state[13] ^= u[3]; keccak_gpu_state[18] ^= u[3]; keccak_gpu_state[23] ^= u[3];
		keccak_gpu_state[4] ^= u[4]; keccak_gpu_state[9] ^= u[4]; keccak_gpu_state[14] ^= u[4]; keccak_gpu_state[19] ^= u[4]; keccak_gpu_state[24] ^= u[4];

		/* rho pi: b[..] = rotl(a[..], ..) */
		v = keccak_gpu_state[1];
		keccak_gpu_state[1] = rotate(keccak_gpu_state[6], (ulong) 44);
		keccak_gpu_state[6] = rotate(keccak_gpu_state[9], (ulong) 20);
		keccak_gpu_state[9] = rotate(keccak_gpu_state[22],  (ulong) 61);
		keccak_gpu_state[22] = rotate(keccak_gpu_state[14], (ulong) 39);
		keccak_gpu_state[14] = rotate(keccak_gpu_state[20], (ulong) 18);
		keccak_gpu_state[20] = rotate(keccak_gpu_state[2],  (ulong) 62);
		keccak_gpu_state[2] = rotate(keccak_gpu_state[12],  (ulong) 43);
		keccak_gpu_state[12] = rotate(keccak_gpu_state[13], (ulong) 25);
		keccak_gpu_state[13] = rotate(keccak_gpu_state[19], (ulong) 8);
		keccak_gpu_state[19] = rotate(keccak_gpu_state[23], (ulong) 56);
		keccak_gpu_state[23] = rotate(keccak_gpu_state[15], (ulong) 41);
		keccak_gpu_state[15] = rotate(keccak_gpu_state[4],  (ulong) 27);
		keccak_gpu_state[4] = rotate(keccak_gpu_state[24],  (ulong) 14);
		keccak_gpu_state[24] = rotate(keccak_gpu_state[21], (ulong) 2);
		keccak_gpu_state[21] = rotate(keccak_gpu_state[8],  (ulong) 55);
		keccak_gpu_state[8] = rotate(keccak_gpu_state[16],  (ulong) 45);
		keccak_gpu_state[16] = rotate(keccak_gpu_state[5],  (ulong) 36);
		keccak_gpu_state[5] = rotate(keccak_gpu_state[3],   (ulong) 28);
		keccak_gpu_state[3] = rotate(keccak_gpu_state[18],  (ulong) 21);
		keccak_gpu_state[18] = rotate(keccak_gpu_state[17], (ulong) 15);
		keccak_gpu_state[17] = rotate(keccak_gpu_state[11], (ulong) 10);
		keccak_gpu_state[11] = rotate(keccak_gpu_state[7],  (ulong) 6);
		keccak_gpu_state[7] = rotate(keccak_gpu_state[10],  (ulong) 3);
		keccak_gpu_state[10] = rotate(v,     (ulong) 1);

		v = keccak_gpu_state[0];  w = keccak_gpu_state[1];  keccak_gpu_state[0] ^= (~w) & keccak_gpu_state[2]; keccak_gpu_state[1] ^= (~keccak_gpu_state[2]) & keccak_gpu_state[3]; keccak_gpu_state[2] ^= (~keccak_gpu_state[3]) & keccak_gpu_state[4]; keccak_gpu_state[3] ^= (~keccak_gpu_state[4]) & v; keccak_gpu_state[4] ^= (~v) & w;
		v = keccak_gpu_state[5];  w = keccak_gpu_state[6];  keccak_gpu_state[5] ^= (~w) & keccak_gpu_state[7]; keccak_gpu_state[6] ^= (~keccak_gpu_state[7]) & keccak_gpu_state[8]; keccak_gpu_state[7] ^= (~keccak_gpu_state[8]) & keccak_gpu_state[9]; keccak_gpu_state[8] ^= (~keccak_gpu_state[9]) & v; keccak_gpu_state[9] ^= (~v) & w;
		v = keccak_gpu_state[10]; w = keccak_gpu_state[11]; keccak_gpu_state[10] ^= (~w) & keccak_gpu_state[12]; keccak_gpu_state[11] ^= (~keccak_gpu_state[12]) & keccak_gpu_state[13]; keccak_gpu_state[12] ^= (~keccak_gpu_state[13]) & keccak_gpu_state[14]; keccak_gpu_state[13] ^= (~keccak_gpu_state[14]) & v; keccak_gpu_state[14] ^= (~v) & w;
		v = keccak_gpu_state[15]; w = keccak_gpu_state[16]; keccak_gpu_state[15] ^= (~w) & keccak_gpu_state[17]; keccak_gpu_state[16] ^= (~keccak_gpu_state[17]) & keccak_gpu_state[18]; keccak_gpu_state[17] ^= (~keccak_gpu_state[18]) & keccak_gpu_state[19]; keccak_gpu_state[18] ^= (~keccak_gpu_state[19]) & v; keccak_gpu_state[19] ^= (~v) & w;
		v = keccak_gpu_state[20]; w = keccak_gpu_state[21]; keccak_gpu_state[20] ^= (~w) & keccak_gpu_state[22]; keccak_gpu_state[21] ^= (~keccak_gpu_state[22]) & keccak_gpu_state[23]; keccak_gpu_state[22] ^= (~keccak_gpu_state[23]) & keccak_gpu_state[24]; keccak_gpu_state[23] ^= (~keccak_gpu_state[24]) & v; keccak_gpu_state[24] ^= (~v) & w;

		keccak_gpu_state[0] ^= RC[i];
	}
    
    #pragma unroll
    for (int i = 0; i<4; i++) 
    { 
        hash->h8[i] = keccak_gpu_state[i]; 
    }
    barrier(CLK_GLOBAL_MEM_FENCE);
//    printf("%s 0 = %08x 1 = %08x 2 = %08x 3 = %08x 4 = %08x 5 = %08x 6 = %08x 7 = %08x\n", __func__, hashes[idx].h4[0], hashes[idx].h4[1], hashes[idx].h4[2], hashes[idx].h4[3], hashes[idx].h4[4], hashes[idx].h4[5], hashes[idx].h4[6], hashes[idx].h4[7]);
}


#define CUBEHASH_ROUNDS 16 
#define ROTATEUPWARDS7(a)  rotate((uint)a,(uint)7)
#define ROTATEUPWARDS11(a) rotate((uint)a,(uint)11)


inline void rrounds(uint *x) {
        uint x0[16];                                         
        uint x1[16];                                         
        
        for (int r = 0; r < CUBEHASH_ROUNDS; r += 2) 
        {       
    		x0[0x0]  = ROTATEUPWARDS7(x[0x0]);           
    		x0[0x1]  = ROTATEUPWARDS7(x[0x1]);           
    		x0[0x2]  = ROTATEUPWARDS7(x[0x2]);           
    		x0[0x3]  = ROTATEUPWARDS7(x[0x3]);           
    		x0[0x4]  = ROTATEUPWARDS7(x[0x4]);           
    		x0[0x5]  = ROTATEUPWARDS7(x[0x5]);           
    		x0[0x6]  = ROTATEUPWARDS7(x[0x6]);           
    		x0[0x7]  = ROTATEUPWARDS7(x[0x7]);           
    		x0[0x8]  = ROTATEUPWARDS7(x[0x8]);           
    		x0[0x9]  = ROTATEUPWARDS7(x[0x9]);           
    		x0[0xa]  = ROTATEUPWARDS7(x[0xa]);           
    		x0[0xb]  = ROTATEUPWARDS7(x[0xb]);           
    		x0[0xc]  = ROTATEUPWARDS7(x[0xc]);           
    		x0[0xd]  = ROTATEUPWARDS7(x[0xd]);           
    		x0[0xe]  = ROTATEUPWARDS7(x[0xe]);           
    		x0[0xf]  = ROTATEUPWARDS7(x[0xf]);           
                                                         
    		x1[0x0] = x[0x10] + x[0x0];                  
    		x1[0x1] = x[0x11] + x[0x1];                  
    		x1[0x2] = x[0x12] + x[0x2];                  
    		x1[0x3] = x[0x13] + x[0x3];                  
    		x1[0x4] = x[0x14] + x[0x4];                  
    		x1[0x5] = x[0x15] + x[0x5];                  
    		x1[0x6] = x[0x16] + x[0x6];                  
    		x1[0x7] = x[0x17] + x[0x7];                  
    		x1[0x8] = x[0x18] + x[0x8];                  
    		x1[0x9] = x[0x19] + x[0x9];                  
    		x1[0xa] = x[0x1a] + x[0xa];                  
    		x1[0xb] = x[0x1b] + x[0xb];                  
    		x1[0xc] = x[0x1c] + x[0xc];                  
    		x1[0xd] = x[0x1d] + x[0xd];                  
    		x1[0xe] = x[0x1e] + x[0xe];                  
    		x1[0xf] = x[0x1f] + x[0xf];                  
                                                         
    		x[0x0] = x0[0x0] ^ x1[0x8];                   
    		x[0x1] = x0[0x1] ^ x1[0x9];                   
    		x[0x2] = x0[0x2] ^ x1[0xa];                   
    		x[0x3] = x0[0x3] ^ x1[0xb];                   
    		x[0x4] = x0[0x4] ^ x1[0xc];                   
    		x[0x5] = x0[0x5] ^ x1[0xd];                   
    		x[0x6] = x0[0x6] ^ x1[0xe];                   
    		x[0x7] = x0[0x7] ^ x1[0xf];                   
    		x[0x8] = x0[0x8] ^ x1[0x0];                   
    		x[0x9] = x0[0x9] ^ x1[0x1];                   
    		x[0xa] = x0[0xa] ^ x1[0x2];                   
    		x[0xb] = x0[0xb] ^ x1[0x3];                   
    		x[0xc] = x0[0xc] ^ x1[0x4];                   
    		x[0xd] = x0[0xd] ^ x1[0x5];                   
    		x[0xe] = x0[0xe] ^ x1[0x6];                   
    		x[0xf] = x0[0xf] ^ x1[0x7];                   
                                                         
    		x0[0x0] = ROTATEUPWARDS11(x[0x0]);           
    		x0[0x1] = ROTATEUPWARDS11(x[0x1]);           
    		x0[0x2] = ROTATEUPWARDS11(x[0x2]);           
    		x0[0x3] = ROTATEUPWARDS11(x[0x3]);           
    		x0[0x4] = ROTATEUPWARDS11(x[0x4]);           
    		x0[0x5] = ROTATEUPWARDS11(x[0x5]);           
    		x0[0x6] = ROTATEUPWARDS11(x[0x6]);           
    		x0[0x7] = ROTATEUPWARDS11(x[0x7]);           
    		x0[0x8] = ROTATEUPWARDS11(x[0x8]);           
    		x0[0x9] = ROTATEUPWARDS11(x[0x9]);           
    		x0[0xa] = ROTATEUPWARDS11(x[0xa]);           
    		x0[0xb] = ROTATEUPWARDS11(x[0xb]);           
    		x0[0xc] = ROTATEUPWARDS11(x[0xc]);           
    		x0[0xd] = ROTATEUPWARDS11(x[0xd]);           
    		x0[0xe] = ROTATEUPWARDS11(x[0xe]);           
    		x0[0xf] = ROTATEUPWARDS11(x[0xf]);           
                                                         
    		x[0x1a]  = x1[0xa] + x[0x0];                 
    		x[0x1b]  = x1[0xb] + x[0x1];                 
    		x[0x18]  = x1[0x8] + x[0x2];                 
    		x[0x19]  = x1[0x9] + x[0x3];                 
    		x[0x1e]  = x1[0xe] + x[0x4];                 
    		x[0x1f]  = x1[0xf] + x[0x5];                 
    		x[0x1c]  = x1[0xc] + x[0x6];                 
    		x[0x1d]  = x1[0xd] + x[0x7];                 
    		x[0x12]  = x1[0x2] + x[0x8];                 
    		x[0x13]  = x1[0x3] + x[0x9];                 
    		x[0x10]  = x1[0x0] + x[0xa];                 
    		x[0x11]  = x1[0x1] + x[0xb];                 
    		x[0x16]  = x1[0x6] + x[0xc];                 
    		x[0x17]  = x1[0x7] + x[0xd];                 
    		x[0x14]  = x1[0x4] + x[0xe];                 
    		x[0x15]  = x1[0x5] + x[0xf];                 
                                                         
    		x[0x00] = x0[0x0] ^ x[0x1e];                 
    		x[0x01] = x0[0x1] ^ x[0x1f];                 
    		x[0x02] = x0[0x2] ^ x[0x1c];                 
    		x[0x03] = x0[0x3] ^ x[0x1d];                 
    		x[0x04] = x0[0x4] ^ x[0x1a];                 
    		x[0x05] = x0[0x5] ^ x[0x1b];                 
    		x[0x06] = x0[0x6] ^ x[0x18];                 
    		x[0x07] = x0[0x7] ^ x[0x19];                 
    		x[0x08] = x0[0x8] ^ x[0x16];                 
    		x[0x09] = x0[0x9] ^ x[0x17];                 
    		x[0x0a] = x0[0xa] ^ x[0x14];                 
    		x[0x0b] = x0[0xb] ^ x[0x15];                 
    		x[0x0c] = x0[0xc] ^ x[0x12];                 
    		x[0x0d] = x0[0xd] ^ x[0x13];                 
    		x[0x0e] = x0[0xe] ^ x[0x10];                 
    		x[0x0f] = x0[0xf] ^ x[0x11];                 
                                                         
    		x0[0x0] = ROTATEUPWARDS7(x[0x00]);           
    		x0[0x1] = ROTATEUPWARDS7(x[0x01]);           
    		x0[0x2] = ROTATEUPWARDS7(x[0x02]);           
    		x0[0x3] = ROTATEUPWARDS7(x[0x03]);           
    		x0[0x4] = ROTATEUPWARDS7(x[0x04]);           
    		x0[0x5] = ROTATEUPWARDS7(x[0x05]);           
    		x0[0x6] = ROTATEUPWARDS7(x[0x06]);           
    		x0[0x7] = ROTATEUPWARDS7(x[0x07]);           
    		x0[0x8] = ROTATEUPWARDS7(x[0x08]);           
    		x0[0x9] = ROTATEUPWARDS7(x[0x09]);           
    		x0[0xa] = ROTATEUPWARDS7(x[0x0a]);           
    		x0[0xb] = ROTATEUPWARDS7(x[0x0b]);           
    		x0[0xc] = ROTATEUPWARDS7(x[0x0c]);           
    		x0[0xd] = ROTATEUPWARDS7(x[0x0d]);           
    		x0[0xe] = ROTATEUPWARDS7(x[0x0e]);           
    		x0[0xf] = ROTATEUPWARDS7(x[0x0f]);           
                                                         
    		x1[0xf] = x[0x1f] + x[0x00];                 
    		x1[0xe] = x[0x1e] + x[0x01];                 
    		x1[0xd] = x[0x1d] + x[0x02];                 
    		x1[0xc] = x[0x1c] + x[0x03];                 
    		x1[0xb] = x[0x1b] + x[0x04];                 
    		x1[0xa] = x[0x1a] + x[0x05];                 
    		x1[0x9] = x[0x19] + x[0x06];                 
    		x1[0x8] = x[0x18] + x[0x07];                 
    		x1[0x7] = x[0x17] + x[0x08];                 
    		x1[0x6] = x[0x16] + x[0x09];                 
    		x1[0x5] = x[0x15] + x[0x0a];                 
    		x1[0x4] = x[0x14] + x[0x0b];                 
    		x1[0x3] = x[0x13] + x[0x0c];                 
    		x1[0x2] = x[0x12] + x[0x0d];                 
    		x1[0x1] = x[0x11] + x[0x0e];                 
    		x1[0x0] = x[0x10] + x[0x0f];                 
                                                         
    		x[0x00] = x0[0x0] ^ x1[0x7];                 
    		x[0x01] = x0[0x1] ^ x1[0x6];                 
    		x[0x02] = x0[0x2] ^ x1[0x5];                 
    		x[0x03] = x0[0x3] ^ x1[0x4];                 
    		x[0x04] = x0[0x4] ^ x1[0x3];                 
    		x[0x05] = x0[0x5] ^ x1[0x2];                 
    		x[0x06] = x0[0x6] ^ x1[0x1];                 
    		x[0x07] = x0[0x7] ^ x1[0x0];                 
    		x[0x08] = x0[0x8] ^ x1[0xf];                 
    		x[0x09] = x0[0x9] ^ x1[0xe];                 
    		x[0x0a] = x0[0xa] ^ x1[0xd];                 
    		x[0x0b] = x0[0xb] ^ x1[0xc];                 
    		x[0x0c] = x0[0xc] ^ x1[0xb];                 
    		x[0x0d] = x0[0xd] ^ x1[0xa];                 
    		x[0x0e] = x0[0xe] ^ x1[0x9];                 
    		x[0x0f] = x0[0xf] ^ x1[0x8];                 
                                                         
    		x0[0x0] = ROTATEUPWARDS11(x[0x00]);          
    		x0[0x1] = ROTATEUPWARDS11(x[0x01]);          
    		x0[0x2] = ROTATEUPWARDS11(x[0x02]);          
    		x0[0x3] = ROTATEUPWARDS11(x[0x03]);          
    		x0[0x4] = ROTATEUPWARDS11(x[0x04]);          
    		x0[0x5] = ROTATEUPWARDS11(x[0x05]);          
    		x0[0x6] = ROTATEUPWARDS11(x[0x06]);          
    		x0[0x7] = ROTATEUPWARDS11(x[0x07]);          
    		x0[0x8] = ROTATEUPWARDS11(x[0x08]);          
    		x0[0x9] = ROTATEUPWARDS11(x[0x09]);          
    		x0[0xa] = ROTATEUPWARDS11(x[0x0a]);          
    		x0[0xb] = ROTATEUPWARDS11(x[0x0b]);          
    		x0[0xc] = ROTATEUPWARDS11(x[0x0c]);          
    		x0[0xd] = ROTATEUPWARDS11(x[0x0d]);          
    		x0[0xe] = ROTATEUPWARDS11(x[0x0e]);          
    		x0[0xf] = ROTATEUPWARDS11(x[0x0f]);          
                                                         
    		x[0x15] = x1[0x5] + x[0x00];                 
    		x[0x14] = x1[0x4] + x[0x01];                 
    		x[0x17] = x1[0x7] + x[0x02];                 
    		x[0x16] = x1[0x6] + x[0x03];                 
    		x[0x11] = x1[0x1] + x[0x04];                 
    		x[0x10] = x1[0x0] + x[0x05];                 
    		x[0x13] = x1[0x3] + x[0x06];                 
    		x[0x12] = x1[0x2] + x[0x07];                 
    		x[0x1d] = x1[0xd] + x[0x08];                 
    		x[0x1c] = x1[0xc] + x[0x09];                 
    		x[0x1f] = x1[0xf] + x[0x0a];                 
    		x[0x1e] = x1[0xe] + x[0x0b];                 
    		x[0x19] = x1[0x9] + x[0x0c];                 
    		x[0x18] = x1[0x8] + x[0x0d];                 
    		x[0x1b] = x1[0xb] + x[0x0e];                 
    		x[0x1a] = x1[0xa] + x[0x0f];                 
                                                         
    		x[0x00] = x0[0x0] ^ x[0x11];                 
    		x[0x01] = x0[0x1] ^ x[0x10];                 
    		x[0x02] = x0[0x2] ^ x[0x13];                 
    		x[0x03] = x0[0x3] ^ x[0x12];                 
    		x[0x04] = x0[0x4] ^ x[0x15];                 
    		x[0x05] = x0[0x5] ^ x[0x14];                 
    		x[0x06] = x0[0x6] ^ x[0x17];                 
    		x[0x07] = x0[0x7] ^ x[0x16];                 
    		x[0x08] = x0[0x8] ^ x[0x19];                 
    		x[0x09] = x0[0x9] ^ x[0x18];                 
    		x[0x0a] = x0[0xa] ^ x[0x1b];                 
    		x[0x0b] = x0[0xb] ^ x[0x1a];                 
    		x[0x0c] = x0[0xc] ^ x[0x1d];                 
    		x[0x0d] = x0[0xd] ^ x[0x1c];                 
    		x[0x0e] = x0[0xe] ^ x[0x1f];                 
    		x[0x0f] = x0[0xf] ^ x[0x1e];                 
    	}
}


__attribute__((reqd_work_group_size(64, 1, 1)))
__kernel void cubehash256( __global hash_t * hashes)
{
  uint gid = get_global_id(0);
  uint idx = gid - get_global_offset(0);

    uint x[32] = 
    {
        0xEA2BD4B4, 0xCCD6F29F, 0x63117E71, 0x35481EAE,
	    0x22512D5B, 0xE5D94E63, 0x7E624131, 0xF4CC12BE,
	    0xC2D0B696, 0x42AF2070, 0xD0720C35, 0x3361DA8C,
	    0x28CCECA4, 0x8EF8AD83, 0x4680AC00, 0x40E5FBAB,
	    0xD89041C3, 0x6107FBD5, 0x6C859D41, 0xF0B26679,
	    0x09392549, 0x5FA25603, 0x65C892FD, 0x93CB6285,
	    0x2AF2B5AE, 0x9E4B4E60, 0x774ABFDD, 0x85254725,
	    0x15815AEB, 0x4AB6AAD6, 0x9CDAF8AF, 0xD6032C0A
    };

    ((uint8 *)x)[0] ^= vload8(0U, hashes[idx].h4);

    rrounds(x);
    x[0] ^= 0x80U;

    rrounds(x);
	x[0x1f] ^= 1U;

    for (int i=0; i < 10; ++i) rrounds(x);


    vstore8(((uint8 *)x)[0], 0U, hashes[idx].h4);
    barrier(CLK_GLOBAL_MEM_FENCE);
//    printf("%s NONCE = %08x 0 = %08x 1 = %08x 2 = %08x 3 = %08x 4 = %08x 5 = %08x 6 = %08x 7 = %08x\n", __func__, gid, hashes[idx].h4[0], hashes[idx].h4[1], hashes[idx].h4[2], hashes[idx].h4[3], hashes[idx].h4[4], hashes[idx].h4[5], hashes[idx].h4[6], hashes[idx].h4[7]);
}



__constant static const sph_u64 SKEIN_IV512[] = {
  SPH_C64(0x4903ADFF749C51CE), SPH_C64(0x0D95DE399746DF03),
  SPH_C64(0x8FD1934127C79BCE), SPH_C64(0x9A255629FF352CB1),
  SPH_C64(0x5DB62599DF6CA7B0), SPH_C64(0xEABE394CA9D5C3F4),
  SPH_C64(0x991112C71A75B523), SPH_C64(0xAE18A40B660FCC33)
};

__constant static const sph_u64 SKEIN_IV512_256[8] = {
	0xCCD044A12FDB3E13UL, 0xE83590301A79A9EBUL,
	0x55AEA0614F816E6FUL, 0x2A2767A4AE9B94DBUL,
	0xEC06025E74DD7683UL, 0xE7A436CDC4746251UL,
	0xC36FBAF9393AD185UL, 0x3EEDBA1833EDFC13UL
};



__constant static const int ROT256[8][4] =
{
	46, 36, 19, 37,
	33, 27, 14, 42,
	17, 49, 36, 39,
	44, 9, 54, 56,
	39, 30, 34, 24,
	13, 50, 10, 17,
	25, 29, 39, 43,
	8, 35, 56, 22,
};

__constant static const sph_u64 skein_ks_parity = 0x1BD11BDAA9FC1A22;

__constant static const sph_u64 t12[6] =
{ 0x20UL,
0xf000000000000000UL,
0xf000000000000020UL,
0x08UL,
0xff00000000000000UL,
0xff00000000000008UL
};


#define Round512(p0,p1,p2,p3,p4,p5,p6,p7,ROT)  {          \
p0 += p1; p1 = SPH_ROTL64(p1, ROT256[ROT][0]);  p1 ^= p0; \
p2 += p3; p3 = SPH_ROTL64(p3, ROT256[ROT][1]);  p3 ^= p2; \
p4 += p5; p5 = SPH_ROTL64(p5, ROT256[ROT][2]);  p5 ^= p4; \
p6 += p7; p7 = SPH_ROTL64(p7, ROT256[ROT][3]);  p7 ^= p6; \
} 

#define Round_8_512(p0, p1, p2, p3, p4, p5, p6, p7, R) { \
	    Round512(p0, p1, p2, p3, p4, p5, p6, p7, 0); \
	    Round512(p2, p1, p4, p7, p6, p5, p0, p3, 1); \
	    Round512(p4, p1, p6, p3, p0, p5, p2, p7, 2); \
	    Round512(p6, p1, p0, p7, p2, p5, p4, p3, 3); \
	    p0 += h[((R)+0) % 9]; \
      p1 += h[((R)+1) % 9]; \
      p2 += h[((R)+2) % 9]; \
      p3 += h[((R)+3) % 9]; \
      p4 += h[((R)+4) % 9]; \
      p5 += h[((R)+5) % 9] + t[((R)+0) % 3]; \
      p6 += h[((R)+6) % 9] + t[((R)+1) % 3]; \
      p7 += h[((R)+7) % 9] + R; \
		Round512(p0, p1, p2, p3, p4, p5, p6, p7, 4); \
		Round512(p2, p1, p4, p7, p6, p5, p0, p3, 5); \
		Round512(p4, p1, p6, p3, p0, p5, p2, p7, 6); \
		Round512(p6, p1, p0, p7, p2, p5, p4, p3, 7); \
		p0 += h[((R)+1) % 9]; \
		p1 += h[((R)+2) % 9]; \
		p2 += h[((R)+3) % 9]; \
		p3 += h[((R)+4) % 9]; \
		p4 += h[((R)+5) % 9]; \
		p5 += h[((R)+6) % 9] + t[((R)+1) % 3]; \
		p6 += h[((R)+7) % 9] + t[((R)+2) % 3]; \
		p7 += h[((R)+8) % 9] + (R+1); \
}

__attribute__((reqd_work_group_size(64, 1, 1)))
__kernel void skein256(__global hash_t *hashes)
{
 uint gid = get_global_id(0);
 uint idx = gid - get_global_offset(0);
 // __global hash_t *hash = &(hashes[gid-get_global_offset(0)]);
 __global hash_t *hash = &hashes[idx];
 // __global hash_t *hash = (__global hash_t *)(hashes + (4 * sizeof(ulong)* (get_global_id(0) % MAX_GLOBAL_THREADS)));


		sph_u64 h[9];
		sph_u64 t[3];
        sph_u64 dt0,dt1,dt2,dt3;
		sph_u64 p0, p1, p2, p3, p4, p5, p6, p7;
        h[8] = skein_ks_parity;

		for (int i = 0; i<8; i++) {
			h[i] = SKEIN_IV512_256[i];
			h[8] ^= h[i];}
		    
			t[0]=t12[0];
			t[1]=t12[1];
			t[2]=t12[2];

        dt0=hash->h8[0];
        dt1=hash->h8[1];
        dt2=hash->h8[2];
        dt3=hash->h8[3];

		p0 = h[0] + dt0;
		p1 = h[1] + dt1;
		p2 = h[2] + dt2;
		p3 = h[3] + dt3;
		p4 = h[4];
		p5 = h[5] + t[0];
		p6 = h[6] + t[1];
		p7 = h[7];

        #pragma unroll 
		for (int i = 1; i<19; i+=2) {Round_8_512(p0,p1,p2,p3,p4,p5,p6,p7,i);}
        p0 ^= dt0;
        p1 ^= dt1;
        p2 ^= dt2;
        p3 ^= dt3;

		h[0] = p0;
		h[1] = p1;
		h[2] = p2;
		h[3] = p3;
		h[4] = p4;
		h[5] = p5;
		h[6] = p6;
		h[7] = p7;
		h[8] = skein_ks_parity;
        
		for (int i = 0; i<8; i++) { h[8] ^= h[i]; }
		
		t[0] = t12[3];
		t[1] = t12[4];
		t[2] = t12[5];
		p5 += t[0];  //p5 already equal h[5] 
		p6 += t[1];
       
        #pragma unroll
		for (int i = 1; i<19; i+=2) { Round_8_512(p0, p1, p2, p3, p4, p5, p6, p7, i); }

		hash->h8[0]      = p0;
		hash->h8[1]      = p1;
		hash->h8[2]      = p2;
		hash->h8[3]      = p3;
	barrier(CLK_GLOBAL_MEM_FENCE);
//    printf("%s NONCE = %08x 0 = %08x 1 = %08x 2 = %08x 3 = %08x 4 = %08x 5 = %08x 6 = %08x 7 = %08x\n", __func__, gid, hashes[idx].h4[0], hashes[idx].h4[1], hashes[idx].h4[2], hashes[idx].h4[3], hashes[idx].h4[4], hashes[idx].h4[5], hashes[idx].h4[6], hashes[idx].h4[7]);

}



#define ss0(x)  (shr((x), 1) ^ shl((x), 3) ^ SPH_ROTL32((x),  4) ^ SPH_ROTL32((x), 19))
#define ss1(x)  (shr((x), 1) ^ shl((x), 2) ^ SPH_ROTL32((x),  8) ^ SPH_ROTL32((x), 23))
#define ss2(x)  (shr((x), 2) ^ shl((x), 1) ^ SPH_ROTL32((x), 12) ^ SPH_ROTL32((x), 25))
#define ss3(x)  (shr((x), 2) ^ shl((x), 2) ^ SPH_ROTL32((x), 15) ^ SPH_ROTL32((x), 29))
#define ss4(x)  (shr((x), 1) ^ (x))
#define ss5(x)  (shr((x), 2) ^ (x))
#define rs1(x) SPH_ROTL32((x),  3)
#define rs2(x) SPH_ROTL32((x),  7)
#define rs3(x) SPH_ROTL32((x), 13)
#define rs4(x) SPH_ROTL32((x), 16)
#define rs5(x) SPH_ROTL32((x), 19)
#define rs6(x) SPH_ROTL32((x), 23)
#define rs7(x) SPH_ROTL32((x), 27)

/* Message expansion function 1 */
uint expand32_1(int i, uint *M32, uint *H, uint *Q)
{

	return (ss1(Q[i - 16]) + ss2(Q[i - 15]) + ss3(Q[i - 14]) + ss0(Q[i - 13])
		+ ss1(Q[i - 12]) + ss2(Q[i - 11]) + ss3(Q[i - 10]) + ss0(Q[i - 9])
		+ ss1(Q[i - 8]) + ss2(Q[i - 7]) + ss3(Q[i - 6]) + ss0(Q[i - 5])
		+ ss1(Q[i - 4]) + ss2(Q[i - 3]) + ss3(Q[i - 2]) + ss0(Q[i - 1])
		+ ((i*(0x05555555ul) + SPH_ROTL32(M32[(i - 16) % 16], ((i - 16) % 16) + 1) + SPH_ROTL32(M32[(i - 13) % 16], ((i - 13) % 16) + 1) - SPH_ROTL32(M32[(i - 6) % 16], ((i - 6) % 16) + 1)) ^ H[(i - 16 + 7) % 16]));

}

/* Message expansion function 2 */
uint expand32_2(int i, uint *M32, uint *H, uint *Q)
{

	return (Q[i - 16] + rs1(Q[i - 15]) + Q[i - 14] + rs2(Q[i - 13])
		+ Q[i - 12] + rs3(Q[i - 11]) + Q[i - 10] + rs4(Q[i - 9])
		+ Q[i - 8] + rs5(Q[i - 7]) + Q[i - 6] + rs6(Q[i - 5])
		+ Q[i - 4] + rs7(Q[i - 3]) + ss4(Q[i - 2]) + ss5(Q[i - 1])
		+ ((i*(0x05555555ul) + SPH_ROTL32(M32[(i - 16) % 16], ((i - 16) % 16) + 1) + SPH_ROTL32(M32[(i - 13) % 16], ((i - 13) % 16) + 1) - SPH_ROTL32(M32[(i - 6) % 16], ((i - 6) % 16) + 1)) ^ H[(i - 16 + 7) % 16]));

}

void Compression256(uint *M32, uint *H)
{

	int i;
	uint XL32, XH32, Q[32];


	Q[0] = (M32[5] ^ H[5]) - (M32[7] ^ H[7]) + (M32[10] ^ H[10]) + (M32[13] ^ H[13]) + (M32[14] ^ H[14]);
	Q[1] = (M32[6] ^ H[6]) - (M32[8] ^ H[8]) + (M32[11] ^ H[11]) + (M32[14] ^ H[14]) - (M32[15] ^ H[15]);
	Q[2] = (M32[0] ^ H[0]) + (M32[7] ^ H[7]) + (M32[9] ^ H[9]) - (M32[12] ^ H[12]) + (M32[15] ^ H[15]);
	Q[3] = (M32[0] ^ H[0]) - (M32[1] ^ H[1]) + (M32[8] ^ H[8]) - (M32[10] ^ H[10]) + (M32[13] ^ H[13]);
	Q[4] = (M32[1] ^ H[1]) + (M32[2] ^ H[2]) + (M32[9] ^ H[9]) - (M32[11] ^ H[11]) - (M32[14] ^ H[14]);
	Q[5] = (M32[3] ^ H[3]) - (M32[2] ^ H[2]) + (M32[10] ^ H[10]) - (M32[12] ^ H[12]) + (M32[15] ^ H[15]);
	Q[6] = (M32[4] ^ H[4]) - (M32[0] ^ H[0]) - (M32[3] ^ H[3]) - (M32[11] ^ H[11]) + (M32[13] ^ H[13]);
	Q[7] = (M32[1] ^ H[1]) - (M32[4] ^ H[4]) - (M32[5] ^ H[5]) - (M32[12] ^ H[12]) - (M32[14] ^ H[14]);
	Q[8] = (M32[2] ^ H[2]) - (M32[5] ^ H[5]) - (M32[6] ^ H[6]) + (M32[13] ^ H[13]) - (M32[15] ^ H[15]);
	Q[9] = (M32[0] ^ H[0]) - (M32[3] ^ H[3]) + (M32[6] ^ H[6]) - (M32[7] ^ H[7]) + (M32[14] ^ H[14]);
	Q[10] = (M32[8] ^ H[8]) - (M32[1] ^ H[1]) - (M32[4] ^ H[4]) - (M32[7] ^ H[7]) + (M32[15] ^ H[15]);
	Q[11] = (M32[8] ^ H[8]) - (M32[0] ^ H[0]) - (M32[2] ^ H[2]) - (M32[5] ^ H[5]) + (M32[9] ^ H[9]);
	Q[12] = (M32[1] ^ H[1]) + (M32[3] ^ H[3]) - (M32[6] ^ H[6]) - (M32[9] ^ H[9]) + (M32[10] ^ H[10]);
	Q[13] = (M32[2] ^ H[2]) + (M32[4] ^ H[4]) + (M32[7] ^ H[7]) + (M32[10] ^ H[10]) + (M32[11] ^ H[11]);
	Q[14] = (M32[3] ^ H[3]) - (M32[5] ^ H[5]) + (M32[8] ^ H[8]) - (M32[11] ^ H[11]) - (M32[12] ^ H[12]);
	Q[15] = (M32[12] ^ H[12]) - (M32[4] ^ H[4]) - (M32[6] ^ H[6]) - (M32[9] ^ H[9]) + (M32[13] ^ H[13]);

	/*  Diffuse the differences in every word in a bijective manner with ssi, and then add the values of the previous double pipe.*/
	Q[0] = ss0(Q[0]) + H[1];
	Q[1] = ss1(Q[1]) + H[2];
	Q[2] = ss2(Q[2]) + H[3];
	Q[3] = ss3(Q[3]) + H[4];
	Q[4] = ss4(Q[4]) + H[5];
	Q[5] = ss0(Q[5]) + H[6];
	Q[6] = ss1(Q[6]) + H[7];
	Q[7] = ss2(Q[7]) + H[8];
	Q[8] = ss3(Q[8]) + H[9];
	Q[9] = ss4(Q[9]) + H[10];
	Q[10] = ss0(Q[10]) + H[11];
	Q[11] = ss1(Q[11]) + H[12];
	Q[12] = ss2(Q[12]) + H[13];
	Q[13] = ss3(Q[13]) + H[14];
	Q[14] = ss4(Q[14]) + H[15];
	Q[15] = ss0(Q[15]) + H[0];

	/* This is the Message expansion or f_1 in the documentation.       */
	/* It has 16 rounds.                                                */
	/* Blue Midnight Wish has two tunable security parameters.          */
	/* The parameters are named EXPAND_1_ROUNDS and EXPAND_2_ROUNDS.    */
	/* The following relation for these parameters should is satisfied: */
	/* EXPAND_1_ROUNDS + EXPAND_2_ROUNDS = 16                           */
#pragma unroll
	for (i = 0; i<2; i++)
		Q[i + 16] = expand32_1(i + 16, M32, H, Q);

#pragma unroll
	for (i = 2; i<16; i++)
		Q[i + 16] = expand32_2(i + 16, M32, H, Q);

	/* Blue Midnight Wish has two temporary cummulative variables that accumulate via XORing */
	/* 16 new variables that are prooduced in the Message Expansion part.                    */
	XL32 = Q[16] ^ Q[17] ^ Q[18] ^ Q[19] ^ Q[20] ^ Q[21] ^ Q[22] ^ Q[23];
	XH32 = XL32^Q[24] ^ Q[25] ^ Q[26] ^ Q[27] ^ Q[28] ^ Q[29] ^ Q[30] ^ Q[31];


	/*  This part is the function f_2 - in the documentation            */

	/*  Compute the double chaining pipe for the next message block.    */
	H[0] = (shl(XH32, 5) ^ shr(Q[16], 5) ^ M32[0]) + (XL32    ^ Q[24] ^ Q[0]);
	H[1] = (shr(XH32, 7) ^ shl(Q[17], 8) ^ M32[1]) + (XL32    ^ Q[25] ^ Q[1]);
	H[2] = (shr(XH32, 5) ^ shl(Q[18], 5) ^ M32[2]) + (XL32    ^ Q[26] ^ Q[2]);
	H[3] = (shr(XH32, 1) ^ shl(Q[19], 5) ^ M32[3]) + (XL32    ^ Q[27] ^ Q[3]);
	H[4] = (shr(XH32, 3) ^ Q[20] ^ M32[4]) + (XL32    ^ Q[28] ^ Q[4]);
	H[5] = (shl(XH32, 6) ^ shr(Q[21], 6) ^ M32[5]) + (XL32    ^ Q[29] ^ Q[5]);
	H[6] = (shr(XH32, 4) ^ shl(Q[22], 6) ^ M32[6]) + (XL32    ^ Q[30] ^ Q[6]);
	H[7] = (shr(XH32, 11) ^ shl(Q[23], 2) ^ M32[7]) + (XL32    ^ Q[31] ^ Q[7]);

	H[8] = SPH_ROTL32(H[4], 9) + (XH32     ^     Q[24] ^ M32[8]) + (shl(XL32, 8) ^ Q[23] ^ Q[8]);
	H[9] = SPH_ROTL32(H[5], 10) + (XH32     ^     Q[25] ^ M32[9]) + (shr(XL32, 6) ^ Q[16] ^ Q[9]);
	H[10] = SPH_ROTL32(H[6], 11) + (XH32     ^     Q[26] ^ M32[10]) + (shl(XL32, 6) ^ Q[17] ^ Q[10]);
	H[11] = SPH_ROTL32(H[7], 12) + (XH32     ^     Q[27] ^ M32[11]) + (shl(XL32, 4) ^ Q[18] ^ Q[11]);
	H[12] = SPH_ROTL32(H[0], 13) + (XH32     ^     Q[28] ^ M32[12]) + (shr(XL32, 3) ^ Q[19] ^ Q[12]);
	H[13] = SPH_ROTL32(H[1], 14) + (XH32     ^     Q[29] ^ M32[13]) + (shr(XL32, 4) ^ Q[20] ^ Q[13]);
	H[14] = SPH_ROTL32(H[2], 15) + (XH32     ^     Q[30] ^ M32[14]) + (shr(XL32, 7) ^ Q[21] ^ Q[14]);
	H[15] = SPH_ROTL32(H[3], 16) + (XH32     ^     Q[31] ^ M32[15]) + (shr(XL32, 2) ^ Q[22] ^ Q[15]);

}


__attribute__((reqd_work_group_size(64, 1, 1)))
__kernel void bmw256(__global hash_t *  hashes, __global uint* output, const ulong target)
{
	uint gid = get_global_id(0);
    uint idx = gid - get_global_offset(0);
    __global hash_t *hash = &hashes[idx];

	uint dh[16] = {
		0x40414243, 0x44454647,
		0x48494A4B, 0x4C4D4E4F,
		0x50515253, 0x54555657,
		0x58595A5B, 0x5C5D5E5F,
		0x60616263, 0x64656667,
		0x68696A6B, 0x6C6D6E6F,
		0x70717273, 0x74757677,
		0x78797A7B, 0x7C7D7E7F
	};
	uint final_s[16] = {
		0xaaaaaaa0, 0xaaaaaaa1, 0xaaaaaaa2,
		0xaaaaaaa3, 0xaaaaaaa4, 0xaaaaaaa5,
		0xaaaaaaa6, 0xaaaaaaa7, 0xaaaaaaa8,
		0xaaaaaaa9, 0xaaaaaaaa, 0xaaaaaaab,
		0xaaaaaaac, 0xaaaaaaad, 0xaaaaaaae,
		0xaaaaaaaf
	};

	uint message[16];
	for (int i = 0; i<8; i++) message[i] = hash->h4[i];
	for (int i = 9; i<14; i++) message[i] = 0;
	message[8]= 0x80;
	message[14]=0x100;
	message[15]=0;

	Compression256(message, dh);
	Compression256(dh, final_s);
	barrier(CLK_GLOBAL_MEM_FENCE);
	
	hash->h4[0] = final_s[8];
	hash->h4[1] = final_s[9];
	hash->h4[2] = final_s[10];
	hash->h4[3] = final_s[11];
	hash->h4[4] = final_s[12];
	hash->h4[5] = final_s[13];
	hash->h4[6] = final_s[14];
	hash->h4[7] = final_s[15];



	ulong * answer = (ulong *) &final_s[14];
	if (answer[0] <= target)
	{
//		printf("nonce:%08x answer:%016x less than target: %016x\n", as_uint(as_uchar4(gid).s3210), answer[0], target);
		//output[atomic_inc(output + 0xFF)] = SWAP4(gid);
		output[atomic_inc(output + 0xFF)] = gid;
	}
	barrier(CLK_GLOBAL_MEM_FENCE);

//    bool result = ( ((ulong*)final_s)[7] <= target);
//	if (result) {
//        //printf("%s NONCE = %08x 0 = %008x 1 = %008x 2 = %008x 3 = %008x 4 = %008x 5 = %008x 6 = %008x 7 = %008x\n", __func__, gid, final_s[8], final_s[9], final_s[10], final_s[11], final_s[12], final_s[13], final_s[14], final_s[15]);
//        //printf("target = %016lx actual =  %016lx\n", target, ((ulong*)final_s[7]));
//	}
}

#define nRows 4
#define nCols 4

typedef struct
{
    ulong  cells[3];
} storage_element_t;

typedef struct
{
    storage_element_t elements[nRows][nCols];
} storage_t;


typedef union 
{
    ulong d8[4];
    ulong4 ds4;
} cell_t;

typedef struct
{
    cell_t cells[3];
} matrixElement_t;

typedef struct 
{
    matrixElement_t elements[nRows][nCols];
} Matrix_t;

#define PARALLEL_G(a, b, c, d) do {      \
    a += b; d ^= a; d = rotate(d, 32UL); \
    c += d; b ^= c; b = rotate(b, 40UL); \
    a += b; d ^= a; d = rotate(d, 48UL); \
    c += d; b ^= c;                      \
    b = rotate(b,  1UL); \
    } while(0)

#define FIND_ME barrier(CLK_GLOBAL_MEM_FENCE);


#define round_lyra_in_mem(pos, state) do {                                               \
                                                                                         \
    PARALLEL_G(state.d8[0], state.d8[1], state.d8[2], state.d8[3]);                      \
                                                                                         \
    _Pragma("unroll 3")                                                                  \
    for (cell_sel_rl=1; cell_sel_rl <=3; cell_sel_rl++)                                  \
    {                                                                                    \
        shared_cell[cell_sel_rl].d8[pos] = state.d8[cell_sel_rl];                        \
    }                                                                                    \
    barrier(CLK_GLOBAL_MEM_FENCE);                                                        \
                                                                                         \
                                                                                         \
    _Pragma("unroll 3")                                                                  \
    for (cell_sel_rl=1; cell_sel_rl <= 3; cell_sel_rl++)                                 \
    {                                                                                    \
        state.d8[cell_sel_rl] = shared_cell[cell_sel_rl].d8[(pos + cell_sel_rl) & 0x3];  \
    }                                                                                    \
    barrier(CLK_GLOBAL_MEM_FENCE);                                                        \
                                                                                         \
    PARALLEL_G(state.d8[0], state.d8[1], state.d8[2], state.d8[3]);                      \
                                                                                         \
    _Pragma("unroll 3")                                                                  \
    for (cell_sel_rl=1; cell_sel_rl<= 3; cell_sel_rl++)                                  \
    {                                                                                    \
        shared_cell[cell_sel_rl].d8[(pos + cell_sel_rl) & 0x3] = state.d8[cell_sel_rl];  \
    }                                                                                    \
    barrier(CLK_GLOBAL_MEM_FENCE);                                                        \
                                                                                         \
    _Pragma("unroll 3")                                                                  \
    for (cell_sel_rl=1; cell_sel_rl<4; cell_sel_rl++)                                    \
    {                                                                                    \
        state.d8[cell_sel_rl] = shared_cell[cell_sel_rl].d8[pos];                        \
    }                                                                                    \
    barrier(CLK_GLOBAL_MEM_FENCE);                                                        \
} while(0) 


#define danceOf11(swap, pos, state, state2, col_sel)  do {                                                                  \
    _Pragma("unroll 3")                                                                                                     \
    for (cell_sel_do11=0; cell_sel_do11 < 3; cell_sel_do11++)                                                               \
    {                                                                                                                       \
        state2_space[pos + cell_sel_do11 * 4] = state2[cell_sel_do11 + (col_sel * 3)];                                      \
    }                                                                                                                       \
    barrier(CLK_GLOBAL_MEM_FENCE);                                                                                          \
    if (pos == 3)                                                                                                           \
    {                                                                                                                       \
        state2_space[0] ^= state.d8[2];                                                                                     \
    }                                                                                                                       \
    _Pragma("unroll 2")                                                                                                     \
    for (cell_sel_do11=0; cell_sel_do11 < 2; cell_sel_do11++)                                                               \
    {                                                                                                                       \
        uint index = (pos != 3) ? (((1 + pos) & 0x3) + cell_sel_do11 * 4) : (((1 + pos) & 0x3) + (cell_sel_do11 + 1) * 4);  \
        state2_space[index] ^= state.d8[cell_sel_do11];                                                                     \
    }                                                                                                                       \
    if (pos != 3)                                                                                                           \
    {                                                                                                                       \
        state2_space[((1+pos) & 0x3) + 2 * 4] ^= state.d8[2];                                                               \
    }                                                                                                                       \
    barrier(CLK_GLOBAL_MEM_FENCE);                                                                                          \
    _Pragma("unroll 3")                                                                                                     \
    for (cell_sel_do11=0; cell_sel_do11 < 3; cell_sel_do11++)                                                               \
    {                                                                                                                       \
        state2[cell_sel_do11 + (col_sel * 3)] = state2_space[pos + cell_sel_do11 * 4];                                      \
    }                                                                                                                       \
    barrier(CLK_GLOBAL_MEM_FENCE);                                                                                          \
} while(0);


#define squeeze(swap, pos, state, storage)  do {                                            \
    for (col_sel_sq=0; col_sel_sq < 4; col_sel_sq++)                                        \
    {                                                                                       \
        _Pragma("unroll 3")                                                                 \
        for (cell_sel_sq=0; cell_sel_sq < 3; cell_sel_sq++)                                 \
            storage.elements[0][3 - col_sel_sq].cells[cell_sel_sq] = state.d8[cell_sel_sq]; \
        round_lyra_in_mem(pos, state);                                                      \
    }                                                                                       \
} while(0)                               

    

#define reduceDuplexf do {                                                                   \
    for (col_sel_rdf=0; col_sel_rdf < 4; col_sel_rdf++)                                      \
    {                                                                                        \
        _Pragma("unroll 3")                                                                  \
        for (uint cell_sel_rdf=0; cell_sel_rdf < 3; cell_sel_rdf++)                          \
        {                                                                                    \
            state1[cell_sel_rdf]   = storage.elements[0][col_sel_rdf].cells[cell_sel_rdf];   \
        }                                                                                    \
        _Pragma("unroll 3")                                                                  \
        for (cell_sel_rdf=0; cell_sel_rdf < 3; cell_sel_rdf++)                               \
                  state.d8[cell_sel_rdf] ^= state1[cell_sel_rdf];                            \
        round_lyra_in_mem(pos, state);                                                       \
        _Pragma("unroll 3")                                                                  \
        for (cell_sel_rdf=0; cell_sel_rdf < 3; cell_sel_rdf++)                               \
            state1[cell_sel_rdf] ^= state.d8[cell_sel_rdf];                                  \
        _Pragma("unroll 3")                                                                  \
        for (cell_sel_rdf=0; cell_sel_rdf < 3; cell_sel_rdf++)                               \
            storage.elements[1][3 - col_sel_rdf].cells[cell_sel_rdf] = state1[cell_sel_rdf]; \
    }                                                                                        \
} while(0)


#define reduceDuplexRowSetupf(rowIn, rowInOut, rowOut, swap, pos, state, storage) do {               \
    for (col_sel_rdsf=0; col_sel_rdsf < 4; col_sel_rdsf++)                                           \
    {                                                                                                \
        _Pragma("unroll 3")                                                                          \
        for (cell_sel_rdsf=0; cell_sel_rdsf<3; cell_sel_rdsf++)                                      \
        {                                                                                            \
            state1[cell_sel_rdsf] = storage.elements[rowIn][col_sel_rdsf].cells[cell_sel_rdsf];      \
        }                                                                                            \
        _Pragma("unroll 3")                                                                          \
        for (cell_sel_rdsf=0; cell_sel_rdsf<3; cell_sel_rdsf++)                                      \
        {                                                                                            \
            state2[cell_sel_rdsf] = storage.elements[rowInOut][col_sel_rdsf].cells[cell_sel_rdsf];   \
        }                                                                                            \
        _Pragma("unroll 3")                                                                          \
        for (cell_sel_rdsf=0; cell_sel_rdsf<3; cell_sel_rdsf++)                                      \
        {                                                                                            \
            state.d8[cell_sel_rdsf]  ^= (state1[cell_sel_rdsf] + state2[cell_sel_rdsf]);             \
        }                                                                                            \
        round_lyra_in_mem(pos, state);                                                               \
        _Pragma("unroll 3")                                                                          \
        for (cell_sel_rdsf=0; cell_sel_rdsf<3; cell_sel_rdsf++)                                      \
        {                                                                                            \
            state1[cell_sel_rdsf] ^= state.d8[cell_sel_rdsf];                                        \
            storage.elements[rowOut][3 - col_sel_rdsf].cells[cell_sel_rdsf] = state1[cell_sel_rdsf]; \
        }                                                                                            \
        danceOf11(swap, pos, state, state2, 0);                                                      \
        _Pragma("unroll 3")                                                                          \
        for (cell_sel_rdsf=0; cell_sel_rdsf < 3; cell_sel_rdsf++)                                    \
        {                                                                                            \
            storage.elements[rowInOut][col_sel_rdsf].cells[cell_sel_rdsf] = state2[cell_sel_rdsf];   \
        }                                                                                            \
    }                                                                                                \
} while(0)


#define reduceDuplexRowf(rowIn, rowInOut, rowOut, swap, pos, state, storage) do {                           \
	state1[0] = storage.elements[rowIn][0].cells[0];														\
	state1[1] = storage.elements[rowIn][0].cells[1];														\
	state1[2] = storage.elements[rowIn][0].cells[2];														\
	state1[3] = storage.elements[rowIn][1].cells[0];														\
	state1[4] = storage.elements[rowIn][1].cells[1];														\
	state1[5] = storage.elements[rowIn][1].cells[2];														\
	state1[6] = storage.elements[rowIn][2].cells[0];														\
	state1[7] = storage.elements[rowIn][2].cells[1];														\
	state1[8] = storage.elements[rowIn][2].cells[2];														\
	state1[9]  = storage.elements[rowIn][3].cells[0];														\
	state1[10] = storage.elements[rowIn][3].cells[1];														\
	state1[11] = storage.elements[rowIn][3].cells[2];														\
	state2[0] = storage.elements[rowInOut][0].cells[0]; 													\
	state2[1] = storage.elements[rowInOut][0].cells[1]; 													\
	state2[2] = storage.elements[rowInOut][0].cells[2]; 													\
	state2[3] = storage.elements[rowInOut][1].cells[0]; 													\
	state2[4] = storage.elements[rowInOut][1].cells[1]; 													\
	state2[5] = storage.elements[rowInOut][1].cells[2]; 													\
	state2[6] = storage.elements[rowInOut][2].cells[0]; 													\
	state2[7] = storage.elements[rowInOut][2].cells[1]; 													\
	state2[8] = storage.elements[rowInOut][2].cells[2]; 													\
	state2[9] = storage.elements[rowInOut][3].cells[0]; 													\
	state2[10] = storage.elements[rowInOut][3].cells[1]; 													\
	state2[11] = storage.elements[rowInOut][3].cells[2]; 													\
	state1[0]  += state2[0];																				\
	state1[1]  += state2[1];																				\
	state1[2]  += state2[2];																				\
	state1[3]  += state2[3];																				\
	state1[4]  += state2[4];																				\
	state1[5]  += state2[5];																				\
	state1[6]  += state2[6];																				\
	state1[7]  += state2[7];																				\
	state1[8]  += state2[8];																				\
	state1[9]  += state2[9];																				\
	state1[10] += state2[10];																				\
	state1[11] += state2[11];																				\
    for (col_sel_rdrf=0; col_sel_rdrf<4; col_sel_rdrf++)                                                    \
    {                                                                                                       \
        _Pragma("unroll 3")                                                                                 \
        for (cell_sel_rdrf=0; cell_sel_rdrf < 3; cell_sel_rdrf++)                                           \
        {                                                                                                   \
            state.d8[cell_sel_rdrf] ^= state1[cell_sel_rdrf + (col_sel_rdrf * 3)];                          \
        }																									\
        round_lyra_in_mem(pos, state);                                                                      \
        danceOf11(swap, pos, state, state2, col_sel_rdrf);                                                  \
		if (rowInOut == rowOut)                                                                             \
		{ 																									\
		    for (cell_sel_rdrf=0; cell_sel_rdrf < 3; cell_sel_rdrf++)                                      	\
		    {                                                                                               \
		        state2[cell_sel_rdrf + (col_sel_rdrf * 3)] ^= state.d8[cell_sel_rdrf];                      \
		    }                                                                                               \
		}																									\
		if (rowInOut != rowOut)	 																			\
		{						 																		    \
		    for (cell_sel_rdrf=0; cell_sel_rdrf < 3; cell_sel_rdrf++)                                       \   
		    {                                                                                              	\
		        storage.elements[rowOut][col_sel_rdrf].cells[cell_sel_rdrf] ^= state.d8[cell_sel_rdrf];     \
		    }                                                        										\
		}																								    \
    }																										\
	storage.elements[rowInOut][0].cells[0] = state2[0];      \
	storage.elements[rowInOut][0].cells[1] = state2[1];      \
	storage.elements[rowInOut][0].cells[2] = state2[2];      \
	storage.elements[rowInOut][1].cells[0] = state2[3];      \
	storage.elements[rowInOut][1].cells[1] = state2[4];      \
	storage.elements[rowInOut][1].cells[2] = state2[5];      \
	storage.elements[rowInOut][2].cells[0] = state2[6];      \
	storage.elements[rowInOut][2].cells[1] = state2[7];      \
	storage.elements[rowInOut][2].cells[2] = state2[8];      \
	storage.elements[rowInOut][3].cells[0] = state2[9];      \
	storage.elements[rowInOut][3].cells[1] = state2[10];      \
	storage.elements[rowInOut][3].cells[2] = state2[11];      \
} while(0)
        

#define get_rowa(swap, pos, state, rowa) do {      \
    swap[pos] = state.d8[0];                       \
    barrier(CLK_GLOBAL_MEM_FENCE);                  \
    rowa = swap[0] & 0x3;                          \
    barrier(CLK_GLOBAL_MEM_FENCE);                  \
} while(0)


//inline void print_matrix(uint pos, storage_t * storage)
//{
//    for (uint row_sel=0; row_sel < 4; row_sel ++)
//    {
//        for (uint col_sel = 0; col_sel < 4; col_sel++)
//        {
//            for (uint cell_sel = 0; cell_sel < 3; cell_sel++)
//            {
//                printf("[%d][%d].cell[%d] %d:%016lx\n", row_sel, col_sel, cell_sel, pos, storage->elements[row_sel][col_sel].cells[cell_sel]);
//            }
//        }
//    }
//}
//
inline void print_state(__local ulong *shared_mem, uint teamidx, uint pos, cell_t *state)
{
    __local cell_t * shared_cell = (__local cell_t *) shared_mem;

    cell_t buffer;
    if (teamidx == 0)
    {
        for (uint cell_sel=0; cell_sel < 4; cell_sel++)
        {
            buffer.d8[cell_sel] = shared_cell[cell_sel].d8[pos];
            shared_cell[cell_sel].d8[pos] = state->d8[cell_sel];
        }
        barrier(CLK_GLOBAL_MEM_FENCE);

        if (pos == 0)
        {
            for (uint cell_sel=0; cell_sel < 4; cell_sel++)
            {
                    printf("state[%d] = %016lx %016lx %016lx %016lx\n", cell_sel, shared_cell[cell_sel].d8[0], shared_cell[cell_sel].d8[1], shared_cell[cell_sel].d8[2], shared_cell[cell_sel].d8[3]);
            }
        }


        for (uint cell_sel=0; cell_sel < 4; cell_sel++)
        {
            shared_cell[cell_sel].d8[pos] = buffer.d8[cell_sel];
        }
        barrier(CLK_GLOBAL_MEM_FENCE);
    }
}


    
__constant ulong const Mask[2][4] = {
    {
	    0x20UL,
        0x20UL,
        0x20UL,
        0x01UL
    },
    {
        0x04UL,
        0x04UL,
        0x80UL,
        0x0100000000000000UL
    }
};


__constant ulong const blake_iv[2][4] = {
    {
        0x6a09e667f3bcc908UL, 
        0xbb67ae8584caa73bUL, 
        0x3c6ef372fe94f82bUL, 
        0xa54ff53a5f1d36f1UL
    },
    {
        0x510e527fade682d1UL, 
        0x9b05688c2b3e6c1fUL, 
        0x1f83d9abfb41bd6bUL, 
        0x5be0cd19137e2179UL
    }
};

#define store_state  do {                                        \
 for (uint state_sel=0; state_sel<4;state_sel++)                 \
 {  															 \
      state_mem_ptr[state_sel].d8[pos] = state.d8[state_sel];    \
 }                                                               \
 barrier(CLK_GLOBAL_MEM_FENCE);                                  \
}while(0);                                                          

#define clear_state  do {                                        \
 for (uint state_sel=0; state_sel<4;state_sel++)                 \
 {                                                               \
      state_mem_ptr[state_sel].d8[pos] = 0;                      \
 }                                                               \
 barrier(CLK_GLOBAL_MEM_FENCE);                                  \
                                                                 \
}while(0);                                                          

#define store_state1 do {                                                     \
    for (uint state1_sel=0; state1_sel < 3; state1_sel++)                     \
              state1_mem_ptr[state1_sel].d8[pos] = state1[state1_sel];        \
    barrier(CLK_GLOBAL_MEM_FENCE);                                            \
}while(0);                                                                    \

#define store_state2 do {                                                     \
	for (uint col_sel=0; col_sel<4; col_sel++)                                \
	{ \
	    for (uint state2_sel=0; state2_sel < 3; state2_sel++)                     \
	              state2_mem_ptr[(col_sel *3) + state2_sel].d8[pos] = state2[(col_sel * 3) + state2_sel];        \
	} \
    barrier(CLK_GLOBAL_MEM_FENCE);                                            \
}while(0);                                                                    \




#define store_matrix  do {                                                       \
  for (uint mat_row_index=0; mat_row_index < 4; mat_row_index++)                 \
  {                                                                              \
    for (uint mat_col_index=0; mat_col_index < 4; mat_col_index++)               \
    {                                                                            \
       for (uint mat_cel_index=0; mat_cel_index < 3; mat_cel_index++)            \
       {                                                                         \
          matrix_mem_ptr->elements[mat_row_index][mat_col_index].cells[mat_cel_index].d8[pos] = storage.elements[mat_row_index][mat_col_index].cells[mat_cel_index]; \
       }                                                                         \
    }                                                                            \
  }                                                                              \
  barrier(CLK_GLOBAL_MEM_FENCE);                                                 \
}while(0);                                                          

#define store_debug \
store_state \
store_matrix \
store_state1 \
store_state2 \
return;



//__kernel void lyra2v2(__global hash_t *hashes, __global cell_t * state_mem, __global Matrix_t * matrix_mem, __global cell_t * state1_mem, __global cell_t * state2_mem))

__attribute__((reqd_work_group_size( 64, 1, 1)))
__kernel void lyra2v2(__global hash_t *hashes)
{
    __private uint teamidx = get_local_id(0) >> 2;               // 64 WIs, 16 teams of 4 WIs 
    __private uint pos = get_local_id(0) & 0x3;                  // position on team
    __private uint hidx =  (get_group_id(0) << 4) | teamidx;
    __local ulong swap_space[32 * 16];



//    __global cell_t * state_mem_ptr = &state_mem[(hidx * 4)];
//    __global Matrix_t * matrix_mem_ptr =  &matrix_mem[hidx];
//    __global cell_t * state1_mem_ptr = &state1_mem[(hidx * 4)];
//    __global cell_t * state2_mem_ptr = &state2_mem[(hidx * 4)];
//	  __global uint * rowa_ptr = &rowa_mem[(hidx * 4)];

//	if ((hidx == 0) && (pos == 0))
//	{
//
//		printf("hidx:%d teamidx:%02d pos:%d input:%08x %08x %08x %08x %08x %08x %08x %08x\n", hidx, teamidx, pos, 
//			   									hashes[hidx].h4[0],
//			   									hashes[hidx].h4[1],
//			   									hashes[hidx].h4[2],
//			   									hashes[hidx].h4[3],
//			   									hashes[hidx].h4[4],
//			   									hashes[hidx].h4[5],
//			   									hashes[hidx].h4[6],
//			   									hashes[hidx].h4[7]);
//	}
//    	
//
//


	//		printf("teamidx:%02d pos:%d hidx:%d input:%08x %08x %08x %08x %08x %08x %08x %08x\n", teamidx, pos, hidx, hashes[hidx].h8[0], 
	//			   																									  hashes[hidx].h8[1],
	//			   																									  hashes[hidx].h8[2],
	//			   																									  hashes[hidx].h8[3],
	//			   																									  hashes[hidx].h8[4],
	//			   																									  hashes[hidx].h8[5],
	//			   																									  hashes[hidx].h8[6],

    //
    
    // worker storage  
    __private storage_t  storage = {0};  // 48 vgprs
    __private cell_t state;              //  4 vgprs

    // overhead storage
    __private ulong state2[3*4]; 
    __private ulong state1[3*4];
    // state2 space is the size of 4 ulong4s, aka 16 ulongs per team


    // local swapspace for sharing between WIs
    __local ulong *swap = &swap_space[teamidx * 32];  
    __local ulong * state2_space = swap;
    __local cell_t * shared_cell = (__local cell_t *) swap;

    // iterators
    __private uint cell_sel, cell_sel_rl, cell_sel_do11, cell_sel_sq, cell_sel_rdf, cell_sel_rdsf, cell_sel_rdrf;
    __private uint col_sel_sq, col_sel_rdf, col_sel_rdsf, col_sel_rdrf;
    __private uint i;
  
    //absorb 
    state.d8[0] = hashes[hidx].h8[pos]; //state[0].pos
    state.d8[1] = state.d8[0];          //state[1].pos
    state.d8[2] = blake_iv[0][pos];     //state[2].pos
    state.d8[3] = blake_iv[1][pos];     //state[3].pos



    for (i=0; i < 12; i++)
    {
        round_lyra_in_mem(pos, state);
    }

    state.d8[0] ^= Mask[0][pos];
    state.d8[1] ^= Mask[1][pos];
 
    for (i=0; i < 12; i++)
    {
        round_lyra_in_mem(pos, state);
    }
    
    // initialize matrix
    squeeze(swap, pos, state, storage);
    
    reduceDuplexf;
    

    reduceDuplexRowSetupf(1, 0, 2, swap, pos, state, storage);
    reduceDuplexRowSetupf(2, 1, 3, swap, pos, state, storage);

    uint rowa;
    uint prev = 3;

    for (i=0; i < 4; i++)
    {
        get_rowa(swap, pos, state, rowa);
        reduceDuplexRowf(prev, rowa, i, swap,  pos, state, storage);
        prev = i;
    }
    
    for (cell_sel=0; cell_sel < 3; cell_sel ++)
    {
        state.d8[cell_sel] ^= storage.elements[rowa][0].cells[cell_sel];
    }

    for (i=0; i < 12; i++)
      round_lyra_in_mem(pos, state);

    hashes[hidx].h8[pos] = state.d8[0];
    barrier(CLK_GLOBAL_MEM_FENCE);
//	printf("hidx:%d teamidx:%02d pos:%d output:%08x %08x %08x %08x %08x %08x %08x %08x\n", hidx, teamidx, pos, 
//			   									hashes[hidx].h4[0],
//			   									hashes[hidx].h4[1],
//			   									hashes[hidx].h4[2],
//			   									hashes[hidx].h4[3],
//			   									hashes[hidx].h4[4],
//			   									hashes[hidx].h4[5],
//			   									hashes[hidx].h4[6],
//			   									hashes[hidx].h4[7]);
//    if (pos == 0)
//        printf("%s %p nonce = %08x 0 = %08x 1 = %08x 2 = %08x 3 = %08x 4 = %08x 5 = %08x 6 = %08x 7 = %08x\n", __func__, &hashes[hidx].h4[0], hidx, hashes[hidx].h4[0], hashes[hidx].h4[1], hashes[hidx].h4[2], hashes[hidx].h4[3], hashes[hidx].h4[4], hashes[hidx].h4[5], hashes[hidx].h4[6], hashes[hidx].h4[7]);
}



