# written by @turekaj 3-4-18
.amdcl2
.gpu Ellesmere
.64bit
.arch_minor 0
.arch_stepping 4
.driver_version 203603
.compile_options "-I /home/turekaj/code/nextminer_working/nextminer/src -DFP_FAST_FMA=1 -cl-denorms-are-zero -m64 -Dcl_khr_fp64=1 -Dcl_amd_fp64=1 -Dcl_khr_global_int32_base_atomics=1 -Dcl_khr_global_int32_extended_atomics=1 -Dcl_khr_local_int32_base_atomics=1 -Dcl_khr_local_int32_extended_atomics=1 -Dcl_khr_int64_base_atomics=1 -Dcl_khr_int64_extended_atomics=1 -Dcl_khr_3d_image_writes=1 -Dcl_khr_byte_addressable_store=1 -Dcl_khr_fp16=1 -Dcl_khr_gl_sharing=1 -Dcl_amd_device_attribute_query=1 -Dcl_amd_vec3=1 -Dcl_amd_printf=1 -Dcl_amd_media_ops=1 -Dcl_amd_media_ops2=1 -Dcl_amd_popcnt=1 -Dcl_khr_image2d_from_buffer=1 -Dcl_khr_spir=1 -Dcl_khr_gl_event=1"
.acl_version "AMD-COMP-LIB-v0.8 (0.0.SC_BUILD_NUMBER)"
.globaldata

.kernel cubehash256
    .config
        .dims x
        .cws 64, 1, 1
        .sgprsnum 30
        .vgprsnum 38
        .floatmode 0xc0
        .dx10clamp
        .ieeemode
        .useargs
        .priority 0
        .arg _.global_offset_0, "size_t", long
        .arg _.global_offset_1, "size_t", long
        .arg _.global_offset_2, "size_t", long
        .arg _.printf_buffer, "size_t", void*, global, , rdonly
        .arg _.vqueue_pointer, "size_t", long
        .arg _.aqlwrap_pointer, "size_t", long
        .arg hashes, "hash_t*", structure*, 32, global, 
    .text
		.set CUBEHASH_UNROLL, 4
		.set FINALIZATION_SIXTEEN_ROUNDS_COUNT, 10
		.set HASHES_ARG_OFFSET, 0x30
		.set HASHES_SIZE, 32
		.set HASHES_INPUT_SHIFT, 5   # 32 bytes
		
		# helper symbols
		args   = %s[4:5]      # user args
		hashes = %s[4:5]      # &hashes[0]
		lc     = %s[2]        # loop count
		lc1    = %s[3]        # loop count 1
		pcs    = %s[8:9]      # PC save/restore
		temp   = %v[0:1]
		hash   = %v[2:5]  	  # &hashes[idx]
		x      = %v[6:37]     # state
		
		# total user sgprs (only counting explicit ones): 4
	    # total user vgprs: 38

		.macro ROUND_EVEN
			v_add_u32 		x[0x10], vcc, x[0x10], x[0x00]
			v_alignbit_b32  x[0x00], x[0x00], x[0x00], 25
			v_add_u32       x[0x11], vcc, x[0x11], x[0x01]
			v_alignbit_b32  x[0x01], x[0x01], x[0x01], 25
			v_add_u32 		x[0x12], vcc, x[0x12], x[0x02]
			v_alignbit_b32  x[0x02], x[0x02], x[0x02], 25
			v_add_u32       x[0x13], vcc, x[0x13], x[0x03]
			v_alignbit_b32  x[0x03], x[0x03], x[0x03], 25
			v_add_u32       x[0x14], vcc, x[0x14], x[0x04]
			v_alignbit_b32  x[0x04], x[0x04], x[0x04], 25
			v_add_u32		x[0x15], vcc, x[0x15], x[0x05]
			v_alignbit_b32  x[0x05], x[0x05], x[0x05], 25
			v_add_u32       x[0x16], vcc, x[0x16], x[0x06]
			v_alignbit_b32  x[0x06], x[0x06], x[0x06], 25
			v_add_u32 		x[0x17], vcc, x[0x17], x[0x07]
			v_alignbit_b32  x[0x07], x[0x07], x[0x07], 25
			v_add_u32       x[0x18], vcc, x[0x18], x[0x08]
			v_alignbit_b32  x[0x08], x[0x08], x[0x08], 25
			v_add_u32       x[0x19], vcc, x[0x19], x[0x09]
			v_alignbit_b32  x[0x09], x[0x09], x[0x09], 25
			v_add_u32       x[0x1A], vcc, x[0x1A], x[0x0A]
			v_alignbit_b32  x[0x0A], x[0x0A], x[0x0A], 25
			v_add_u32       x[0x1B], vcc, x[0x1B], x[0x0B]
			v_alignbit_b32  x[0x0B], x[0x0B], x[0x0B], 25
			v_add_u32       x[0x1C], vcc, x[0x1C], x[0x0C]
			v_alignbit_b32  x[0x0C], x[0x0C], x[0x0C], 25
			v_add_u32       x[0x1D], vcc, x[0x1D], x[0x0D]
			v_alignbit_b32  x[0x0D], x[0x0D], x[0x0D], 25
			v_add_u32       x[0x1E], vcc, x[0x1E], x[0x0E]
			v_alignbit_b32  x[0x0E], x[0x0E], x[0x0E], 25
			v_add_u32       x[0x1F], vcc, x[0x1F], x[0x0F]
			v_alignbit_b32  x[0x0F], x[0x0F], x[0x0F], 25
		
			v_xor_b32	    x[0x08], x[0x08], x[0x10]
			v_xor_b32       x[0x09], x[0x09], x[0x11]
			v_xor_b32       x[0x0A], x[0x0A], x[0x12]
			v_xor_b32       x[0x0B], x[0x0B], x[0x13]
			v_xor_b32       x[0x0C], x[0x0C], x[0x14]
			v_xor_b32       x[0x0D], x[0x0D], x[0x15]
			v_xor_b32       x[0x0E], x[0x0E], x[0x16]
			v_xor_b32       x[0x0F], x[0x0F], x[0x17]
			v_xor_b32       x[0x00], x[0x00], x[0x18]
			v_xor_b32       x[0x01], x[0x01], x[0x19]
			v_xor_b32       x[0x02], x[0x02], x[0x1A]
			v_xor_b32       x[0x03], x[0x03], x[0x1B]
			v_xor_b32       x[0x04], x[0x04], x[0x1C]
			v_xor_b32       x[0x05], x[0x05], x[0x1D]
			v_xor_b32       x[0x06], x[0x06], x[0x1E]
			v_xor_b32       x[0x07], x[0x07], x[0x1F]
		
			v_add_u32       x[0x12], vcc, x[0x12], x[0x08]
			v_alignbit_b32  x[0x08], x[0x08], x[0x08], 21
			v_add_u32       x[0x13], vcc, x[0x13], x[0x09]
			v_alignbit_b32  x[0x09], x[0x09], x[0x09], 21
			v_add_u32       x[0x10], vcc, x[0x10], x[0x0A]
			v_alignbit_b32  x[0x0A], x[0x0A], x[0x0A], 21
			v_add_u32       x[0x11], vcc, x[0x11], x[0x0B]
			v_alignbit_b32  x[0x0B], x[0x0B], x[0x0B], 21
			v_add_u32       x[0x16], vcc, x[0x16], x[0x0C]
			v_alignbit_b32  x[0x0C], x[0x0C], x[0x0C], 21
			v_add_u32       x[0x17], vcc, x[0x17], x[0x0D]
			v_alignbit_b32  x[0x0D], x[0x0D], x[0x0D], 21
			v_add_u32       x[0x14], vcc, x[0x14], x[0x0E]
			v_alignbit_b32  x[0x0E], x[0x0E], x[0x0E], 21
			v_add_u32       x[0x15], vcc, x[0x15], x[0x0F]
			v_alignbit_b32  x[0x0F], x[0x0F], x[0x0F], 21
			v_add_u32       x[0x1A], vcc, x[0x1A], x[0x00]
			v_alignbit_b32  x[0x00], x[0x00], x[0x00], 21
			v_add_u32       x[0x1B], vcc, x[0x1B], x[0x01]
			v_alignbit_b32  x[0x01], x[0x01], x[0x01], 21
			v_add_u32       x[0x18], vcc, x[0x18], x[0x02]
			v_alignbit_b32  x[0x02], x[0x02], x[0x02], 21
			v_add_u32       x[0x19], vcc, x[0x19], x[0x03]
			v_alignbit_b32  x[0x03], x[0x03], x[0x03], 21
			v_add_u32       x[0x1E], vcc, x[0x1E], x[0x04]
			v_alignbit_b32  x[0x04], x[0x04], x[0x04], 21
			v_add_u32       x[0x1F], vcc, x[0x1F], x[0x05]
			v_alignbit_b32  x[0x05], x[0x05], x[0x05], 21
			v_add_u32       x[0x1C], vcc, x[0x1C], x[0x06]
			v_alignbit_b32  x[0x06], x[0x06], x[0x06], 21
			v_add_u32       x[0x1D], vcc, x[0x1D], x[0x07]
			v_alignbit_b32  x[0x07], x[0x07], x[0x07], 21
		
			v_xor_b32       x[0x0C], x[0x0C], x[0x12]
			v_xor_b32       x[0x0D], x[0x0D], x[0x13]
			v_xor_b32       x[0x0E], x[0x0E], x[0x10]
			v_xor_b32       x[0x0F], x[0x0F], x[0x11]
			v_xor_b32       x[0x08], x[0x08], x[0x16]
			v_xor_b32       x[0x09], x[0x09], x[0x17]
			v_xor_b32       x[0x0A], x[0x0A], x[0x14]
			v_xor_b32       x[0x0B], x[0x0B], x[0x15]
			v_xor_b32       x[0x04], x[0x04], x[0x1A]
			v_xor_b32       x[0x05], x[0x05], x[0x1B]
			v_xor_b32       x[0x06], x[0x06], x[0x18]
			v_xor_b32       x[0x07], x[0x07], x[0x19]
			v_xor_b32       x[0x00], x[0x00], x[0x1E]
			v_xor_b32       x[0x01], x[0x01], x[0x1F]
			v_xor_b32       x[0x02], x[0x02], x[0x1C]
			v_xor_b32       x[0x03], x[0x03], x[0x1D]
		.endm

		.macro ROUND_ODD
			v_add_u32	    x[0x13], vcc, x[0x13], x[0x0C]
			v_alignbit_b32  x[0x0C], x[0x0C], x[0x0C], 25
			v_add_u32       x[0x12], vcc, x[0x12], x[0x0D]
			v_alignbit_b32  x[0x0D], x[0x0D], x[0x0D], 25
			v_add_u32       x[0x11], vcc, x[0x11], x[0x0E]
			v_alignbit_b32  x[0x0E], x[0x0E], x[0x0E], 25
			v_add_u32       x[0x10], vcc, x[0x10], x[0x0F]
			v_alignbit_b32  x[0x0F], x[0x0F], x[0x0F], 25
			v_add_u32       x[0x17], vcc, x[0x17], x[0x08]
			v_alignbit_b32  x[0x08], x[0x08], x[0x08], 25
			v_add_u32       x[0x16], vcc, x[0x16], x[0x09]
			v_alignbit_b32  x[0x09], x[0x09], x[0x09], 25
			v_add_u32       x[0x15], vcc, x[0x15], x[0x0A]
			v_alignbit_b32  x[0x0A], x[0x0A], x[0x0A], 25
			v_add_u32       x[0x14], vcc, x[0x14], x[0x0B]
			v_alignbit_b32  x[0x0B], x[0x0B], x[0x0B], 25
			v_add_u32       x[0x1B], vcc, x[0x1B], x[0x04]
			v_alignbit_b32  x[0x04], x[0x04], x[0x04], 25
			v_add_u32       x[0x1A], vcc, x[0x1A], x[0x05]
			v_alignbit_b32  x[0x05], x[0x05], x[0x05], 25
			v_add_u32       x[0x19], vcc, x[0x19], x[0x06]
			v_alignbit_b32  x[0x06], x[0x06], x[0x06], 25
			v_add_u32       x[0x18], vcc, x[0x18], x[0x07]
			v_alignbit_b32  x[0x07], x[0x07], x[0x07], 25
			v_add_u32       x[0x1F], vcc, x[0x1F], x[0x00]
			v_alignbit_b32  x[0x00], x[0x00], x[0x00], 25
			v_add_u32       x[0x1E], vcc, x[0x1E], x[0x01]
			v_alignbit_b32  x[0x01], x[0x01], x[0x01], 25
			v_add_u32       x[0x1D], vcc, x[0x1D], x[0x02]
			v_alignbit_b32  x[0x02], x[0x02], x[0x02], 25
			v_add_u32       x[0x1C], vcc, x[0x1C], x[0x03]
			v_alignbit_b32  x[0x03], x[0x03], x[0x03], 25
		
			v_xor_b32       x[0x04], x[0x04], x[0x13]
			v_xor_b32       x[0x05], x[0x05], x[0x12]
			v_xor_b32       x[0x06], x[0x06], x[0x11]
			v_xor_b32       x[0x07], x[0x07], x[0x10]
			v_xor_b32       x[0x00], x[0x00], x[0x17]
			v_xor_b32       x[0x01], x[0x01], x[0x16]
			v_xor_b32       x[0x02], x[0x02], x[0x15]
			v_xor_b32       x[0x03], x[0x03], x[0x14]
			v_xor_b32       x[0x0C], x[0x0C], x[0x1B]
			v_xor_b32       x[0x0D], x[0x0D], x[0x1A]
			v_xor_b32       x[0x0E], x[0x0E], x[0x19]
			v_xor_b32       x[0x0F], x[0x0F], x[0x18]
			v_xor_b32       x[0x08], x[0x08], x[0x1F]
			v_xor_b32       x[0x09], x[0x09], x[0x1E]
			v_xor_b32       x[0x0A], x[0x0A], x[0x1D]
			v_xor_b32       x[0x0B], x[0x0B], x[0x1C]
		
			v_add_u32       x[0x11], vcc, x[0x11], x[0x04]
			v_alignbit_b32  x[0x04], x[0x04], x[0x04], 21
			v_add_u32       x[0x10], vcc, x[0x10], x[0x05]
			v_alignbit_b32  x[0x05], x[0x05], x[0x05], 21
			v_add_u32       x[0x13], vcc, x[0x13], x[0x06]
			v_alignbit_b32  x[0x06], x[0x06], x[0x06], 21
			v_add_u32       x[0x12], vcc, x[0x12], x[0x07]
			v_alignbit_b32  x[0x07], x[0x07], x[0x07], 21
			v_add_u32       x[0x15], vcc, x[0x15], x[0x00]
			v_alignbit_b32  x[0x00], x[0x00], x[0x00], 21
			v_add_u32       x[0x14], vcc, x[0x14], x[0x01]
			v_alignbit_b32  x[0x01], x[0x01], x[0x01], 21
			v_add_u32       x[0x17], vcc, x[0x17], x[0x02]
			v_alignbit_b32  x[0x02], x[0x02], x[0x02], 21
			v_add_u32       x[0x16], vcc, x[0x16], x[0x03]
			v_alignbit_b32  x[0x03], x[0x03], x[0x03], 21
			v_add_u32       x[0x19], vcc, x[0x19], x[0x0C]
			v_alignbit_b32  x[0x0C], x[0x0C], x[0x0C], 21
			v_add_u32       x[0x18], vcc, x[0x18], x[0x0D]
			v_alignbit_b32  x[0x0D], x[0x0D], x[0x0D], 21
			v_add_u32       x[0x1B], vcc, x[0x1B], x[0x0E]
			v_alignbit_b32  x[0x0E], x[0x0E], x[0x0E], 21
			v_add_u32       x[0x1A], vcc, x[0x1A], x[0x0F]
			v_alignbit_b32  x[0x0F], x[0x0F], x[0x0F], 21
			v_add_u32       x[0x1D], vcc, x[0x1D], x[0x08]
			v_alignbit_b32  x[0x08], x[0x08], x[0x08], 21
			v_add_u32       x[0x1C], vcc, x[0x1C], x[0x09]
			v_alignbit_b32  x[0x09], x[0x09], x[0x09], 21
			v_add_u32       x[0x1F], vcc, x[0x1F], x[0x0A]
			v_alignbit_b32  x[0x0A], x[0x0A], x[0x0A], 21
			v_add_u32       x[0x1E], vcc, x[0x1E], x[0x0B]
			v_alignbit_b32  x[0x0B], x[0x0B], x[0x0B], 21
		
			v_xor_b32       x[0x00], x[0x00], x[0x11]
			v_xor_b32       x[0x01], x[0x01], x[0x10]
			v_xor_b32       x[0x02], x[0x02], x[0x13]
			v_xor_b32       x[0x03], x[0x03], x[0x12]
			v_xor_b32       x[0x04], x[0x04], x[0x15]
			v_xor_b32       x[0x05], x[0x05], x[0x14]
			v_xor_b32       x[0x06], x[0x06], x[0x17]
			v_xor_b32       x[0x07], x[0x07], x[0x16]
			v_xor_b32       x[0x08], x[0x08], x[0x19]
			v_xor_b32       x[0x09], x[0x09], x[0x18]
			v_xor_b32       x[0x0A], x[0x0A], x[0x1B]
			v_xor_b32       x[0x0B], x[0x0B], x[0x1A]
			v_xor_b32       x[0x0C], x[0x0C], x[0x1D]
			v_xor_b32       x[0x0D], x[0x0D], x[0x1C]
			v_xor_b32       x[0x0E], x[0x0E], x[0x1F]
			v_xor_b32       x[0x0F], x[0x0F], x[0x1E]
		.endm
		
		
		.macro LINK_CH_L0
			s_getpc_b64 pcs[0:1]
		.endm
		
		.macro RET_CH_L0
			s_add_u32   pcs[0], pcs[0], 4
			s_addc_u32  pcs[1], pcs[1], scc
			s_setpc_b64 pcs[0:1]
		.endm
		
		
		.ch_main:
		
			# acquire &hashes[0] from args, and index into it with idx (local_id)
			s_lshl_b32 s0, s6, 6
			v_add_u32  v0, vcc, s0, v0
			s_load_dwordx2 s[0:1], s[4:5], 0x30
			v_mov_b32      v1, 0
			v_lshlrev_b64  v[0:1], 5, v[0:1]
			s_waitcnt      lgkmcnt(0)
			v_add_u32      v0, vcc, s0, v0
			v_mov_b32      v2, s1
			v_addc_u32     v1, vcc, v2, v1, vcc
			v_add_u32      v2, vcc, v0, 16
			v_addc_u32     v3, vcc, v1, 0, vcc
			flat_load_dwordx4 x[0:3], v[0:1]
			flat_load_dwordx4 x[4:7], v[2:3]
			s_waitcnt    vmcnt(0) & lgkmcnt(0)

			# xor IV into      x[0x00 - 0x07]
			v_xor_b32 x[0x00], 0xEA2BD4B4, x[0x00]
			v_xor_b32 x[0x01], 0xCCD6F29F, x[0x01] 
			v_xor_b32 x[0x02], 0x63117E71, x[0x02] 
			v_xor_b32 x[0x03], 0x35481EAE, x[0x03] 
			v_xor_b32 x[0x04], 0x22512D5B, x[0x04] 
			v_xor_b32 x[0x05], 0xE5D94E63, x[0x05] 
			v_xor_b32 x[0x06], 0x7E624131, x[0x06] 
			v_xor_b32 x[0x07], 0xF4CC12BE, x[0x07] 
		
			# initialize the rest of state with remaining IV
			v_mov_b32 x[0x08], 0xC2D0B696 
			v_mov_b32 x[0x09], 0x42AF2070 
			v_mov_b32 x[0x0A], 0xD0720C35 
			v_mov_b32 x[0x0B], 0x3361DA8C
			v_mov_b32 x[0x0C], 0x28CCECA4 
			v_mov_b32 x[0x0D], 0x8EF8AD83 
			v_mov_b32 x[0x0E], 0x4680AC00 
			v_mov_b32 x[0x0F], 0x40E5FBAB
			v_mov_b32 x[0x10], 0xD89041C3 
			v_mov_b32 x[0x11], 0x6107FBD5 
			v_mov_b32 x[0x12], 0x6C859D41 
			v_mov_b32 x[0x13], 0xF0B26679
			v_mov_b32 x[0x14], 0x09392549 
			v_mov_b32 x[0x15], 0x5FA25603 
			v_mov_b32 x[0x16], 0x65C892FD 
			v_mov_b32 x[0x17], 0x93CB6285
			v_mov_b32 x[0x18], 0x2AF2B5AE 
			v_mov_b32 x[0x19], 0x9E4B4E60 
			v_mov_b32 x[0x1A], 0x774ABFDD 
			v_mov_b32 x[0x1B], 0x85254725
			v_mov_b32 x[0x1C], 0x15815AEB 
			v_mov_b32 x[0x1D], 0x4AB6AAD6 
			v_mov_b32 x[0x1E], 0x9CDAF8AF
			v_mov_b32 x[0x1F], 0xD6032C0A
		
			LINK_CH_L0
			s_branch  .sixteen_rounds
			v_xor_b32 x[0x00], 0x80, x[0x00]
			
			LINK_CH_L0
			s_branch  .sixteen_rounds
			v_xor_b32 x[0x1F], 0x1, x[0x1F]
		
		.finalization:
			s_mov_b32 lc1, 0
			.finalization_loop_start:
				LINK_CH_L0
				s_branch  .sixteen_rounds
			.finalization_loop_end:
				s_add_u32   lc1, lc1, 1
				s_cmpk_eq_u32 lc1, FINALIZATION_SIXTEEN_ROUNDS_COUNT
				s_cbranch_scc0 .finalization_loop_start
		
			s_branch .finish
		
		.sixteen_rounds:
			s_mov_b32 lc, 0
			.sixteen_rounds_loop_start:
				ROUND_EVEN
   			    ROUND_ODD
   			    ROUND_EVEN
   			    ROUND_ODD
			.sixteen_round_loop_end:
   		s_add_u32   lc, lc, 1
   		s_cmpk_eq_u32 lc, CUBEHASH_UNROLL
   		s_cbranch_scc0 .sixteen_rounds_loop_start
		RET_CH_L0
		
		.finish:	
			# store hash 
			v_nop
			v_nop
			flat_store_dwordx4 v[0:1], x[0:3]
			flat_store_dwordx4 v[2:3], x[4:7]
			s_waitcnt          vmcnt(0)
			s_endpgm
