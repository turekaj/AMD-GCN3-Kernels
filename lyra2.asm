/* Disassembling 'AMDOCLBin-0.bin' */
.amdcl2
.gpu Ellesmere
.64bit
.arch_minor 0
.arch_stepping 4
.driver_version 203603
.compile_options "-I /home/turekaj/code/nextminer_working/nextminer/src -DFP_FAST_FMA=1 -cl-denorms-are-zero -m64 -Dcl_khr_fp64=1 -Dcl_amd_fp64=1 -Dcl_khr_global_int32_base_atomics=1 -Dcl_khr_global_int32_extended_atomics=1 -Dcl_khr_local_int32_base_atomics=1 -Dcl_khr_local_int32_extended_atomics=1 -Dcl_khr_int64_base_atomics=1 -Dcl_khr_int64_extended_atomics=1 -Dcl_khr_3d_image_writes=1 -Dcl_khr_byte_addressable_store=1 -Dcl_khr_fp16=1 -Dcl_khr_gl_sharing=1 -Dcl_amd_device_attribute_query=1 -Dcl_amd_vec3=1 -Dcl_amd_printf=1 -Dcl_amd_media_ops=1 -Dcl_amd_media_ops2=1 -Dcl_amd_popcnt=1 -Dcl_khr_image2d_from_buffer=1 -Dcl_khr_spir=1 -Dcl_khr_gl_event=1"
.acl_version "AMD-COMP-LIB-v0.8 (0.0.SC_BUILD_NUMBER)"
.globaldata
.gdata:
    .byte 0x88, 0x6a, 0x3f, 0x24, 0xd3, 0x08, 0xa3, 0x85
    .byte 0x2e, 0x8a, 0x19, 0x13, 0x44, 0x73, 0x70, 0x03
    .byte 0x22, 0x38, 0x09, 0xa4, 0xd0, 0x31, 0x9f, 0x29
    .byte 0x98, 0xfa, 0x2e, 0x08, 0x89, 0x6c, 0x4e, 0xec
    .byte 0xe6, 0x21, 0x28, 0x45, 0x77, 0x13, 0xd0, 0x38
    .byte 0xcf, 0x66, 0x54, 0xbe, 0x6c, 0x0c, 0xe9, 0x34
    .byte 0xb7, 0x29, 0xac, 0xc0, 0xdd, 0x50, 0x7c, 0xc9
    .byte 0xb5, 0xd5, 0x84, 0x3f, 0x17, 0x09, 0x47, 0xb5
    .byte 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    .byte 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    .byte 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    .byte 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    .byte 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    .byte 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    .byte 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    .byte 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01
    .byte 0x08, 0xc9, 0xbc, 0xf3, 0x67, 0xe6, 0x09, 0x6a
    .byte 0x3b, 0xa7, 0xca, 0x84, 0x85, 0xae, 0x67, 0xbb
    .byte 0x2b, 0xf8, 0x94, 0xfe, 0x72, 0xf3, 0x6e, 0x3c
    .byte 0xf1, 0x36, 0x1d, 0x5f, 0x3a, 0xf5, 0x4f, 0xa5
    .byte 0xd1, 0x82, 0xe6, 0xad, 0x7f, 0x52, 0x0e, 0x51
    .byte 0x1f, 0x6c, 0x3e, 0x2b, 0x8c, 0x68, 0x05, 0x9b
    .byte 0x6b, 0xbd, 0x41, 0xfb, 0xab, 0xd9, 0x83, 0x1f
    .byte 0x79, 0x21, 0x7e, 0x13, 0x19, 0xcd, 0xe0, 0x5b
    .byte 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 #MASK[0][0]
    .byte 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 #MASK[0][1]
    .byte 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 #MASK[0][2]
    .byte 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 #MASK[0][3]
    .byte 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 #MASK[1][0]
    .byte 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 #MASK[1][1]
    .byte 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 #MASK[1][2]
    .byte 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01 #MASK[1][3]
    .byte 0x08, 0xc9, 0xbc, 0xf3, 0x67, 0xe6, 0x09, 0x6a #BLAKE_IV[0][0]
    .byte 0x3b, 0xa7, 0xca, 0x84, 0x85, 0xae, 0x67, 0xbb #BLAKE_IV[0][1]
    .byte 0x2b, 0xf8, 0x94, 0xfe, 0x72, 0xf3, 0x6e, 0x3c #BLAKE_IV[0][2]
    .byte 0xf1, 0x36, 0x1d, 0x5f, 0x3a, 0xf5, 0x4f, 0xa5 #BLAKE_IV[0][3]
    .byte 0xd1, 0x82, 0xe6, 0xad, 0x7f, 0x52, 0x0e, 0x51 #BLAKE_IV[1][0]
    .byte 0x1f, 0x6c, 0x3e, 0x2b, 0x8c, 0x68, 0x05, 0x9b #BLAKE_IV[1][1]
    .byte 0x6b, 0xbd, 0x41, 0xfb, 0xab, 0xd9, 0x83, 0x1f #BLAKE_IV[1][2]
    .byte 0x79, 0x21, 0x7e, 0x13, 0x19, 0xcd, 0xe0, 0x5b #BLAKE_IV[2][3]
.kernel lyra2v2
    .config
		.dims x
		.cws 64, 1, 1
		.sgprsnum 40
		.vgprsnum 172
        .floatmode 0xFF
		.localsize 1
        .dx10clamp
        .ieeemode
        .useargs
        .priority 0
        .arg _.global_offset_0, "size_t", long
        .arg _.global_offset_1, "size_t", long
        .arg _.global_offset_2, "size_t", long
        .arg _.printf_buffer, "size_t", void*, global, , rdonly
        .arg _.vqueue_pointer, "size_t", long
        .arg _.aqlwrap_pointer, "size_t", long
        .arg hashes, "hash_t*", structure*, 32, global, 
        #.arg state,  "uint*", uint*, global, 
        #.arg matrix, "uint*", uint*, global, 
        #.arg state1, "uint*", uint*, global, 
        #.arg state2, "uint*", uint*, global, 
    .text
		.set NUM_COLS, 4
		.set NUM_ROWS, 4
		.set NUM_CELLS_PER_COL, 3
		.set CELL_OFFSET,  2
		.set COL_OFFSET, (NUM_CELLS_PER_COL * CELL_OFFSET)
		.set ROW_OFFSET, (NUM_COLS * COL_OFFSET)
		.set HIDX_HASH_SHIFT, 5
		.set POS_HASH_SHIFT, 3
		
		.set BLAKE_MASK_POS_SHIFT, 3
		.set MASK_OFFSET_0, 192
		.set MASK_OFFSET_1, (MASK_OFFSET_0 + 32)
		.set BLAKE_OFFSET_0, (MASK_OFFSET_1 + 32)
		.set BLAKE_OFFSET_1, (BLAKE_OFFSET_0 + 32)

		# helper symbols
		localid         = %v[0]
		hidx            = %v[1]
		pos             = %v[2]
		v_RowInOut      = %v[3]
		state           = %v[4:11]
		matrix          = %v[12:107]
		state1          = %v[108:131]
		state2          = %v[132:155]
		hashes_hidx     = %v[156:157]
		v_rowtemp       = %v[158:163]
		v_debug         = %v[164:171]

		# aliases
		mask            = %v[14:17]
		v_offset_calc   = %v[0]
		
		userdata        = %s[0:6]
		args            = %s[4:5]
		groupid         = %s[6]
		v_offset        = %s[7]
		link_reg_l0     = %s[8:9]
		link_reg_l1     = %s[10:11]
		esave_l0    = %s[12:13]
		esave_l1    = %s[14:15]
		olc             = %s[16]
		ilc             = %s[17]
		s_RowIn         = %s[18]
		s_RowInOut		= %s[19]
		s_RowOut        = %s[20]
		stemp           = %s[20:23]
		s_state_mem     = %s[24:25]
		s_matrix_mem    = %s[26:27]
		s_state1_mem    = %s[28:29]
		s_state2_mem    = %s[30:31]
		#s_vcc           = %s[32:33]

		.macro PARALLEL_G
  			# state[0] += state[1]; state[3] ^= state[0]; state[3] = rotate(state[3], 32UL) 
  			v_add_u32       state[0], vcc, state[0], state[2]
  			v_addc_u32      state[1], vcc, state[1], state[3], vcc
  			v_xor_b32       state[6], state[6], state[0] 
  			v_xor_b32       state[7], state[7], state[1]
  			v_mov_b32       v_offset_calc, state[7]				# state[3] = rotate(state[3], 32UL) 
  			v_mov_b32       state[7], state[6] 
  			v_mov_b32       state[6], v_offset_calc
  			
  			# state[2] += state[3]; state[1] ^= state[2]; state[1] = rotate(state[1], 40UL) 
  			v_add_u32       state[4], vcc, state[4], state[6]    #  s[2] += s[3] 
  			v_addc_u32      state[5], vcc, state[5], state[7], vcc 
  			v_xor_b32       state[2], state[4], state[2]         # s[1] ^= s[2] 
  			v_xor_b32       state[3], state[3], state[5]
  			v_alignbit_b32  v_offset_calc, state[2], state[3], (32 - (40 - 32))
  			v_alignbit_b32  state[2], state[3], state[2], (32 - (40 - 32))
  			v_mov_b32       state[3], v_offset_calc
  			
  			# state[0] += state[1]; state[3] ^= state[0]; state[3] = rotate(state[3], 48UL) 
  			v_add_u32       state[0], vcc, state[0], state[2]              # s[0] += s[1] 
  			v_addc_u32      state[1], vcc, state[1], state[3], vcc              
  			v_xor_b32       state[6], state[6], state[0]                   # s[3] ^= s[0] 
  			v_xor_b32       state[7], state[1], state[7]         
  			v_alignbit_b32  v_offset_calc, state[6], state[7], (32 - (48 - 32))         # state[3] = rotate(state[3], 48UL) 
  			v_alignbit_b32  state[6], state[7], state[6], (32 - (48 - 32))
  			v_mov_b32       state[7], v_offset_calc

  			# state[2] += state[3]; state[1] ^= state[2]; state[1] = rotate(state[1], 1L) 
  			v_add_u32       state[4], vcc, state[4], state[6]              # s[2] += s[3] 
  			v_addc_u32      state[5], vcc, state[5], state[7], vcc              
  			v_xor_b32       state[2], state[2], state[4]                   # s[1] ^= s[2] 
  			v_xor_b32       state[3], state[3], state[5]    
  			v_alignbit_b32  v_offset_calc, state[2], state[3], (32 - 1)         # state[1] = rotate(state[1], 1L) 
  			v_alignbit_b32  state[3], state[3], state[2], (32 - 1)
  			v_mov_b32       state[2], v_offset_calc
		.endm

		.macro ROUND_LYRA
		  PARALLEL_G 		# PARALLEL_G(state[0], state[1], state[2], state[3])
		  
		  v_nop
		  v_nop
		  v_mov_b32        state[2], state[2] quad_perm:[1,2,3,0]  # state[1].s1230 
		  v_mov_b32        state[3], state[3] quad_perm:[1,2,3,0]  
		  v_mov_b32        state[4], state[4] quad_perm:[2,3,0,1]  # state[2].s2301
		  v_mov_b32        state[5], state[5] quad_perm:[2,3,0,1]
		  v_mov_b32        state[6], state[6] quad_perm:[3,0,1,2]  # state[3].s3012 
		  v_mov_b32        state[7], state[7] quad_perm:[3,0,1,2]
		  
		  PARALLEL_G 		# PARALLEL_G(state[2], state[1].s1230, state[2].2301, state[3].s3012)
		  v_nop
		  v_nop
		  
		  v_mov_b32        state[2], state[2] quad_perm:[3,0,1,2]
		  v_mov_b32        state[3], state[3] quad_perm:[3,0,1,2]
		  v_mov_b32        state[4], state[4] quad_perm:[2,3,0,1]
		  v_mov_b32        state[5], state[5] quad_perm:[2,3,0,1]
		  v_mov_b32        state[6], state[6] quad_perm:[1,2,3,0]
		  v_mov_b32        state[7], state[7] quad_perm:[1,2,3,0]
		.endm


		.macro LINK_L0
			s_getpc_b64 link_reg_l0[0:1]
		.endm

		.macro RET_L0
		  s_add_u32    link_reg_l0[0], link_reg_l0[0], 4
		  s_addc_u32   link_reg_l1[1], link_reg_l1[1], scc
		  s_setpc_b64 link_reg_l0[0:1]
		.endm

		.macro LINK_L1
		  s_getpc_b64 link_reg_l1[0:1]
		.endm
		
		.macro RET_L1
		  s_add_u32   link_reg_l1[0], link_reg_l1[0], 4
		  s_addc_u32  link_reg_l1[1], link_reg_l1[1], scc
		  s_setpc_b64 link_reg_l1[0:1]
		.endm

		.macro STORE_STATE
			v_lshlrev_b32 v_debug[0], 7, hidx
			v_lshlrev_b32 v_debug[1], POS_HASH_SHIFT, pos
			v_add_u32     v_debug[0], vcc, v_debug[0], v_debug[1]
			v_mov_b32     v_debug[2], s_state_mem[0]
			v_mov_b32 	  v_debug[3], s_state_mem[1]
			v_add_u32     v_debug[2], vcc, v_debug[2], v_debug[0]
			v_addc_u32    v_debug[3], vcc, v_debug[3], 0, vcc

			flat_store_dwordx2  v_debug[2:3], state[0:1]
 			s_waitcnt           vmcnt(0) & lgkmcnt(0)
			s_waitcnt           expcnt(0)

			v_add_u32 			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32 			v_debug[3], vcc, v_debug[3], 0, vcc
			flat_store_dwordx2  v_debug[2:3], state[2:3]
 			s_waitcnt           vmcnt(0) & lgkmcnt(0)
			s_waitcnt           expcnt(0)


			v_add_u32 			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32 			v_debug[3], vcc, v_debug[3], 0, vcc
			flat_store_dwordx2  v_debug[2:3], state[4:5]
 			s_waitcnt           vmcnt(0) & lgkmcnt(0)
			s_waitcnt           expcnt(0)

			v_add_u32 			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32 			v_debug[3], vcc, v_debug[3], 0, vcc
			flat_store_dwordx2  v_debug[2:3], state[6:7]
 			s_waitcnt           vmcnt(0) & lgkmcnt(0)
			s_waitcnt           expcnt(0)
		.endm
		
		.macro STORE_STATE1
			v_lshlrev_b32 v_debug[0], 7, hidx
			v_lshlrev_b32 v_debug[1], POS_HASH_SHIFT, pos
			v_add_u32     v_debug[0], vcc, v_debug[0], v_debug[1]
			v_mov_b32     v_debug[2], s_state1_mem[0]
			v_mov_b32 	  v_debug[3], s_state1_mem[1]
			v_add_u32     v_debug[2], vcc, v_debug[2], v_debug[0]
			v_addc_u32    v_debug[3], vcc, v_debug[3], 0, vcc

			flat_store_dwordx2  v_debug[2:3], state1[0:1]
 			s_waitcnt           vmcnt(0) & lgkmcnt(0)
			s_waitcnt           expcnt(0)

			v_add_u32 			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32 			v_debug[3], vcc, v_debug[3], 0, vcc
			flat_store_dwordx2  v_debug[2:3], state1[2:3]
 			s_waitcnt           vmcnt(0) & lgkmcnt(0)
			s_waitcnt           expcnt(0)


			v_add_u32 			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32 			v_debug[3], vcc, v_debug[3], 0, vcc
			flat_store_dwordx2  v_debug[2:3], state1[4:5]
 			s_waitcnt           vmcnt(0) & lgkmcnt(0)
			s_waitcnt           expcnt(0)

 			s_waitcnt           vmcnt(0) & lgkmcnt(0)
			s_waitcnt           expcnt(0)
		.endm
		
		.macro STORE_STATE2
			v_lshlrev_b32 v_debug[0], 7, hidx
			v_lshlrev_b32 v_debug[1], POS_HASH_SHIFT, pos
			v_add_u32     v_debug[0], vcc, v_debug[0], v_debug[1]
			v_mov_b32     v_debug[2], s_state2_mem[0]
			v_mov_b32 	  v_debug[3], s_state2_mem[1]
			
			v_add_u32     v_debug[2], vcc, v_debug[2], v_debug[0]
			v_addc_u32    v_debug[3], vcc, v_debug[3], 0, vcc
			flat_store_dwordx2  v_debug[2:3], state2[0:1]
 			s_waitcnt           vmcnt(0) & lgkmcnt(0)
			s_waitcnt           expcnt(0)

			v_add_u32 			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32 			v_debug[3], vcc, v_debug[3], 0, vcc
			flat_store_dwordx2  v_debug[2:3], state2[2:3]
 			s_waitcnt           vmcnt(0) & lgkmcnt(0)
			s_waitcnt           expcnt(0)

			v_add_u32 			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32 			v_debug[3], vcc, v_debug[3], 0, vcc
			flat_store_dwordx2  v_debug[2:3], state2[4:5]
 			s_waitcnt           vmcnt(0) & lgkmcnt(0)
			s_waitcnt           expcnt(0)
			
			#v_add_u32     v_debug[2], vcc, v_debug[2], v_debug[0]
			#v_addc_u32    v_debug[3], vcc, v_debug[3], 0, vcc
			#flat_store_dwordx2  v_debug[2:3], state2[6:7]
 			#s_waitcnt           vmcnt(0) & lgkmcnt(0)
			#s_waitcnt           expcnt(0)

			#v_add_u32 			v_debug[2], vcc, v_debug[2], 32
			#v_addc_u32 			v_debug[3], vcc, v_debug[3], 0, vcc
			#flat_store_dwordx2  v_debug[2:3], state2[8:9]
 			#s_waitcnt           vmcnt(0) & lgkmcnt(0)
			#s_waitcnt           expcnt(0)

			#v_add_u32 			v_debug[2], vcc, v_debug[2], 32
			#v_addc_u32 			v_debug[3], vcc, v_debug[3], 0, vcc
			#flat_store_dwordx2  v_debug[2:3], state2[10:11]
 			#s_waitcnt           vmcnt(0) & lgkmcnt(0)
			#s_waitcnt           expcnt(0)
			#
			#v_add_u32     v_debug[2], vcc, v_debug[2], v_debug[0]
			#v_addc_u32    v_debug[3], vcc, v_debug[3], 0, vcc
			#flat_store_dwordx2  v_debug[2:3], state2[12:13]
 			#s_waitcnt           vmcnt(0) & lgkmcnt(0)
			#s_waitcnt           expcnt(0)

			#v_add_u32 			v_debug[2], vcc, v_debug[2], 32
			#v_addc_u32 			v_debug[3], vcc, v_debug[3], 0, vcc
			#flat_store_dwordx2  v_debug[2:3], state2[14:15]
 			#s_waitcnt           vmcnt(0) & lgkmcnt(0)
			#s_waitcnt           expcnt(0)

			#v_add_u32 			v_debug[2], vcc, v_debug[2], 32
			#v_addc_u32 			v_debug[3], vcc, v_debug[3], 0, vcc
			#flat_store_dwordx2  v_debug[2:3], state2[16:17]
 			#s_waitcnt           vmcnt(0) & lgkmcnt(0)
			#s_waitcnt           expcnt(0)
			#
			#v_add_u32     v_debug[2], vcc, v_debug[2], v_debug[0]
			#v_addc_u32    v_debug[3], vcc, v_debug[3], 0, vcc
			#flat_store_dwordx2  v_debug[2:3], state2[18:19]
 			#s_waitcnt           vmcnt(0) & lgkmcnt(0)
			#s_waitcnt           expcnt(0)

			#v_add_u32 			v_debug[2], vcc, v_debug[2], 32
			#v_addc_u32 			v_debug[3], vcc, v_debug[3], 0, vcc
			#flat_store_dwordx2  v_debug[2:3], state2[20:21]
 			#s_waitcnt           vmcnt(0) & lgkmcnt(0)
			#s_waitcnt           expcnt(0)

			#v_add_u32 			v_debug[2], vcc, v_debug[2], 32
			#v_addc_u32 			v_debug[3], vcc, v_debug[3], 0, vcc
			#flat_store_dwordx2  v_debug[2:3], state2[22:23]
 			#s_waitcnt           vmcnt(0) & lgkmcnt(0)
			#s_waitcnt           expcnt(0)

 			s_waitcnt           vmcnt(0) & lgkmcnt(0)
			s_waitcnt           expcnt(0)
		.endm


	.macro STORE_MATRIX
			
			v_lshlrev_b32 v_debug[0], POS_HASH_SHIFT, pos #pos offset
			v_mov_b32	  v_debug[1], 1536
			v_mad_u32_u24 v_debug[1], v_debug[1], hidx, v_debug[0]
			v_mov_b32     v_debug[2], s_matrix_mem[0]
			v_mov_b32     v_debug[3], s_matrix_mem[1]
			v_add_u32	  v_debug[2], vcc, v_debug[2], v_debug[1]
			v_addc_u32 	  v_debug[3], vcc, v_debug[3], 0, vcc

			flat_store_dwordx2 	v_debug[2:3], matrix[0:1]
			v_add_u32			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32			v_debug[3], vcc, v_debug[3], 0, vcc
			
			flat_store_dwordx2 	v_debug[2:3], matrix[2:3]
			v_add_u32			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32			v_debug[3], vcc, v_debug[3], 0, vcc
			
			flat_store_dwordx2 	v_debug[2:3], matrix[4:5]
			v_add_u32			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32			v_debug[3], vcc, v_debug[3], 0, vcc
			
			flat_store_dwordx2 	v_debug[2:3], matrix[6:7]
			v_add_u32			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32			v_debug[3], vcc, v_debug[3], 0, vcc

			flat_store_dwordx2 	v_debug[2:3], matrix[8:9]
			v_add_u32			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32			v_debug[3], vcc, v_debug[3], 0, vcc

			flat_store_dwordx2 	v_debug[2:3], matrix[10:11]
			v_add_u32			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32			v_debug[3], vcc, v_debug[3], 0, vcc
			
			flat_store_dwordx2 	v_debug[2:3], matrix[12:13]
			v_add_u32			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32			v_debug[3], vcc, v_debug[3], 0, vcc
			
			flat_store_dwordx2 	v_debug[2:3], matrix[14:15]
			v_add_u32			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32			v_debug[3], vcc, v_debug[3], 0, vcc
			
			flat_store_dwordx2 	v_debug[2:3], matrix[16:17]
			v_add_u32			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32			v_debug[3], vcc, v_debug[3], 0, vcc
			
			flat_store_dwordx2 	v_debug[2:3], matrix[18:19]
			v_add_u32			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32			v_debug[3], vcc, v_debug[3], 0, vcc

			flat_store_dwordx2 	v_debug[2:3], matrix[20:21]
			v_add_u32			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32			v_debug[3], vcc, v_debug[3], 0, vcc

			flat_store_dwordx2 	v_debug[2:3], matrix[22:23]
			v_add_u32			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32			v_debug[3], vcc, v_debug[3], 0, vcc
			
			flat_store_dwordx2 	v_debug[2:3], matrix[24:25]
			v_add_u32			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32			v_debug[3], vcc, v_debug[3], 0, vcc
			
			flat_store_dwordx2 	v_debug[2:3], matrix[26:27]
			v_add_u32			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32			v_debug[3], vcc, v_debug[3], 0, vcc
			
			flat_store_dwordx2 	v_debug[2:3], matrix[28:29]
			v_add_u32			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32			v_debug[3], vcc, v_debug[3], 0, vcc
			
			flat_store_dwordx2 	v_debug[2:3], matrix[30:31]
			v_add_u32			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32			v_debug[3], vcc, v_debug[3], 0, vcc
			
			flat_store_dwordx2 	v_debug[2:3], matrix[32:33]
			v_add_u32			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32			v_debug[3], vcc, v_debug[3], 0, vcc

			flat_store_dwordx2 	v_debug[2:3], matrix[34:35]
			v_add_u32			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32			v_debug[3], vcc, v_debug[3], 0, vcc

			flat_store_dwordx2 	v_debug[2:3], matrix[36:37]
			v_add_u32			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32			v_debug[3], vcc, v_debug[3], 0, vcc
			
			flat_store_dwordx2 	v_debug[2:3], matrix[38:39]
			v_add_u32			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32			v_debug[3], vcc, v_debug[3], 0, vcc
			
			flat_store_dwordx2 	v_debug[2:3], matrix[40:41]
			v_add_u32			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32			v_debug[3], vcc, v_debug[3], 0, vcc
			
			flat_store_dwordx2 	v_debug[2:3], matrix[42:43]
			v_add_u32			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32			v_debug[3], vcc, v_debug[3], 0, vcc
			
			flat_store_dwordx2 	v_debug[2:3], matrix[44:45]
			v_add_u32			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32			v_debug[3], vcc, v_debug[3], 0, vcc

			flat_store_dwordx2 	v_debug[2:3], matrix[46:47]
			v_add_u32			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32			v_debug[3], vcc, v_debug[3], 0, vcc

			flat_store_dwordx2 	v_debug[2:3], matrix[48:49]
			v_add_u32			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32			v_debug[3], vcc, v_debug[3], 0, vcc
			
			flat_store_dwordx2 	v_debug[2:3], matrix[50:51]
			v_add_u32			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32			v_debug[3], vcc, v_debug[3], 0, vcc
			
			flat_store_dwordx2 	v_debug[2:3], matrix[52:53]
			v_add_u32			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32			v_debug[3], vcc, v_debug[3], 0, vcc
			
			flat_store_dwordx2 	v_debug[2:3], matrix[54:55]
			v_add_u32			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32			v_debug[3], vcc, v_debug[3], 0, vcc
			
			flat_store_dwordx2 	v_debug[2:3], matrix[56:57]
			v_add_u32			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32			v_debug[3], vcc, v_debug[3], 0, vcc
			
			flat_store_dwordx2 	v_debug[2:3], matrix[58:59]
			v_add_u32			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32			v_debug[3], vcc, v_debug[3], 0, vcc

			flat_store_dwordx2 	v_debug[2:3], matrix[60:61]
			v_add_u32			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32			v_debug[3], vcc, v_debug[3], 0, vcc

			flat_store_dwordx2 	v_debug[2:3], matrix[62:63]
			v_add_u32			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32			v_debug[3], vcc, v_debug[3], 0, vcc
			
			flat_store_dwordx2 	v_debug[2:3], matrix[64:65]
			v_add_u32			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32			v_debug[3], vcc, v_debug[3], 0, vcc
			
			flat_store_dwordx2 	v_debug[2:3], matrix[66:67]
			v_add_u32			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32			v_debug[3], vcc, v_debug[3], 0, vcc
			
			flat_store_dwordx2 	v_debug[2:3], matrix[68:69]
			v_add_u32			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32			v_debug[3], vcc, v_debug[3], 0, vcc
			
			flat_store_dwordx2 	v_debug[2:3], matrix[70:71]
			v_add_u32			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32			v_debug[3], vcc, v_debug[3], 0, vcc

			flat_store_dwordx2 	v_debug[2:3], matrix[72:73]
			v_add_u32			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32			v_debug[3], vcc, v_debug[3], 0, vcc

			flat_store_dwordx2 	v_debug[2:3], matrix[74:75]
			v_add_u32			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32			v_debug[3], vcc, v_debug[3], 0, vcc
			
			flat_store_dwordx2 	v_debug[2:3], matrix[76:77]
			v_add_u32			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32			v_debug[3], vcc, v_debug[3], 0, vcc
			
			flat_store_dwordx2 	v_debug[2:3], matrix[78:79]
			v_add_u32			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32			v_debug[3], vcc, v_debug[3], 0, vcc

			flat_store_dwordx2 	v_debug[2:3], matrix[80:81]
			v_add_u32			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32			v_debug[3], vcc, v_debug[3], 0, vcc

			flat_store_dwordx2 	v_debug[2:3], matrix[82:83]
			v_add_u32			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32			v_debug[3], vcc, v_debug[3], 0, vcc
			
			flat_store_dwordx2 	v_debug[2:3], matrix[84:85]
			v_add_u32			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32			v_debug[3], vcc, v_debug[3], 0, vcc
			
			flat_store_dwordx2 	v_debug[2:3], matrix[86:87]
			v_add_u32			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32			v_debug[3], vcc, v_debug[3], 0, vcc
			
			flat_store_dwordx2 	v_debug[2:3], matrix[88:89]
			v_add_u32			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32			v_debug[3], vcc, v_debug[3], 0, vcc
			
			flat_store_dwordx2 	v_debug[2:3], matrix[90:91]
			v_add_u32			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32			v_debug[3], vcc, v_debug[3], 0, vcc

			flat_store_dwordx2 	v_debug[2:3], matrix[92:93]
			v_add_u32			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32			v_debug[3], vcc, v_debug[3], 0, vcc

			flat_store_dwordx2 	v_debug[2:3], matrix[94:95]
			v_add_u32			v_debug[2], vcc, v_debug[2], 32
			v_addc_u32			v_debug[3], vcc, v_debug[3], 0, vcc
 			s_waitcnt         vmcnt(0) & lgkmcnt(0)
			 s_waitcnt          expcnt(0)
		.endm

		.macro STORE_DEBUG
			s_mov_b64	exec, 0xffffffffffffffff
			s_set_gpr_idx_off
			STORE_MATRIX
			STORE_STATE
			STORE_STATE1
			STORE_STATE2
		.endm
			
		.main:
		# test code only
		  s_load_dwordx2    s_state_mem[0:1], args[0:1], 0x38   # state pointer
		  s_load_dwordx2    s_matrix_mem[0:1], args[0:1], 0x40   # matrix pointer
		  s_load_dwordx2    s_state1_mem[0:1], args[0:1], 0x48   # state1 pointer
		  s_load_dwordx2    s_state2_mem[0:1], args[0:1], 0x50   # state2 pointer
			s_load_dwordx2        stemp[0:1], args[0:1], 0x30
			s_waitcnt             lgkmcnt(0)
			v_mov_b32             hashes_hidx[0], stemp[0]
			v_mov_b32             hashes_hidx[1], stemp[1]

			v_lshrrev_b32         v_rowtemp[0], 2, localid           # teamidx = localidx >> 2
			v_and_b32             pos, 0x3, localid              # pos = localid & 0x3
			v_lshlrev_b32         v_rowtemp[1], 4, groupid           # group_offset = group_id * 16 (16 * 4 = 64) 
			v_or_b32              hidx, v_rowtemp[1], v_rowtemp[0]       # hidx = group_offset | teamidx

			v_lshlrev_b32         v_rowtemp[0], HIDX_HASH_SHIFT, hidx
			v_lshlrev_b32         v_rowtemp[1], POS_HASH_SHIFT, pos
			v_add_u32             v_rowtemp[0], vcc, v_rowtemp[0], v_rowtemp[1]  # [hidx][pos] offset to be added to hashes*
			v_add_u32             hashes_hidx[0], vcc, hashes_hidx[0], v_rowtemp[0]
			v_addc_u32            hashes_hidx[1], vcc, hashes_hidx[1], 0, vcc    #hashes[hidx][pos]

			.sponge_init:
			flat_load_dwordx2     state[0:1], hashes_hidx[0:1]  
			s_waitcnt             vmcnt(0) & lgkmcnt(0)
			v_mov_b32             state[2], state[0]
			v_mov_b32             state[3], state[1]

			# need to keep blake IV and mask in constant memory such that each worker can fetch his unique data 

			v_lshlrev_b32         v_rowtemp[0], BLAKE_MASK_POS_SHIFT, v2
			v_mov_b32             matrix[0], (.gdata)&0xffffffff   #using matrix space as temp
			v_mov_b32             matrix[1], (.gdata)>>32
			v_add_u32             matrix[0], vcc, matrix[0], v_rowtemp[0]

			v_add_u32             state[4], vcc, BLAKE_OFFSET_0, matrix[0]
			v_addc_u32            state[5], vcc, matrix[1], 0, vcc
			
			v_add_u32             state[6], vcc, BLAKE_OFFSET_1, matrix[0]
			v_addc_u32            state[7], vcc, matrix[1], 0, vcc
			
			v_add_u32             mask[0], vcc, MASK_OFFSET_0, matrix[0]
			v_addc_u32            mask[1], vcc, matrix[1], 0, vcc
			
			v_add_u32             mask[2], vcc, MASK_OFFSET_1, matrix[0]
			v_addc_u32            mask[3], vcc, matrix[1], 0, vcc

			flat_load_dwordx2     state[4:5], state[4:5] 		#state.d8[2] = blake_iv[0][pos]
			flat_load_dwordx2     state[6:7], state[6:7]		#state.d8[3] = blake_iv[1][pos]
			flat_load_dwordx2     mask[0:1], mask[0:1]          #mask[0][pos]
			flat_load_dwordx2     mask[2:3], mask[2:3]	    	#mask[1][pos]
  			s_waitcnt             vmcnt(0) & lgkmcnt(0)

  			LINK_L0
  			s_branch 			  .round_lyra_12

  			v_xor_b32			  state[0], state[0], mask[0] 
  			v_xor_b32             state[1], state[1], mask[1]



  			v_xor_b32 			  state[2], state[2], mask[2]
  			v_xor_b32 			  state[3], state[3], mask[3]

  			LINK_L0
  			s_branch			  .round_lyra_12

  			.squeeze:
  			s_mov_b32             olc, 0      # column_sel aka loop counter
  			s_mov_b32             stemp[0], 3 

  			.squeeze_loop_start:
  				s_sub_u32 	       v_offset, stemp[0], olc  # 3 - column_sel
  				s_mul_i32          v_offset, v_offset, COL_OFFSET
  				s_set_gpr_idx_on   v_offset, 8    # voffset dest
  				v_mov_b32          matrix[0], state[0]
  				v_mov_b32          matrix[1], state[1]
  				v_mov_b32          matrix[2], state[2]
  				v_mov_b32          matrix[3], state[3]
  				v_mov_b32          matrix[4], state[4]
  				v_mov_b32          matrix[5], state[5]
  				s_set_gpr_idx_off
  				LINK_L1
  				s_branch 		   .round_lyra
  			.squeeze_loop_end:
  			s_add_u32			  olc, olc, 1
  			s_cmpk_eq_u32         olc, NUM_COLS
  			s_cbranch_scc0        .squeeze_loop_start

  			.reduce_duplex_f:                 # only operates on row[0] and row[1]
  			s_mov_b32             olc, 0      # column_sel aka loop counter
  											  # stemp[0] is still 3, from squeeze, so lets save an instruction
  			.reduce_duplex_f_loop_start:
				s_mul_i32          v_offset, olc, COL_OFFSET
				s_set_gpr_idx_on   v_offset, 1               # vgpr offset src
  				v_mov_b32          state1[0], matrix[0]
  				v_mov_b32          state1[1], matrix[1]
  				v_mov_b32          state1[2], matrix[2]
  				v_mov_b32          state1[3], matrix[3]
  				v_mov_b32          state1[4], matrix[4]
  				v_mov_b32          state1[5], matrix[5]
  				s_set_gpr_idx_off

  				v_xor_b32          state[0], state[0], state1[0]
  				v_xor_b32          state[1], state[1], state1[1]
  				v_xor_b32          state[2], state[2], state1[2]
  				v_xor_b32          state[3], state[3], state1[3]
  				v_xor_b32          state[4], state[4], state1[4]
  				v_xor_b32          state[5], state[5], state1[5]

  				LINK_L1
  				s_branch 		   .round_lyra

  				v_xor_b32          state1[0], state1[0], state[0]
  				v_xor_b32          state1[1], state1[1], state[1]
  				v_xor_b32          state1[2], state1[2], state[2]
  				v_xor_b32          state1[3], state1[3], state[3]
  				v_xor_b32          state1[4], state1[4], state[4]
  				v_xor_b32          state1[5], state1[5], state[5]

  				s_sub_i32          v_offset, stemp[0], olc
  				s_mul_i32          v_offset, v_offset, COL_OFFSET
  				s_add_u32          v_offset, v_offset, ROW_OFFSET  # Matrix[1][3 - col_sel]
  				s_set_gpr_idx_on   v_offset, 8    # voffset dest
  			    v_mov_b32          matrix[0], state1[0]
  			    v_mov_b32          matrix[1], state1[1]
  			    v_mov_b32          matrix[2], state1[2]
  			    v_mov_b32          matrix[3], state1[3]
  			    v_mov_b32          matrix[4], state1[4]
  			    v_mov_b32          matrix[5], state1[5]
  			    s_set_gpr_idx_off
  			.reduce_duplex_f_loop_end:
  			s_add_u32 			  olc, olc, 1
  			s_cmpk_eq_u32         olc, NUM_COLS
  			s_cbranch_scc0        .reduce_duplex_f_loop_start

		s_mov_b32  	  s_RowIn, 1
		s_mov_b32     s_RowInOut, 0
		s_mov_b32     s_RowOut, 2
		LINK_L0
		s_branch      .reduce_duplex_row_setup_f
		
		s_mov_b32  	  s_RowIn, 2
		s_mov_b32     s_RowInOut, 1
		s_mov_b32     s_RowOut, 3
		LINK_L0
		s_branch      .reduce_duplex_row_setup_f


    	.reduce_duplex_row_f_complete:
    	/* This part of the kernel is the most tricky.
    	 * The row to be worked on is selected via state[0] & 0x3,
    	 * Row selection is randomly distributed, and rows can be 
    	 * revisited. The chance of every team (out of 16) having 
    	 * All teams have same rowA:           1.525%
    	 * three-quarter teams have same rowA: 2.080%
    	 * half teams have same rowA: 		   3.125%
    	 * quarter teams have same rowA:       6.250%
    	 *
    	 * Because these percentages are so low, it is unlikely 
    	 * that any s_cbranch_execz will be taken. 
    	 */
    		s_mov_b32 s_RowIn, 3
    		s_mov_b32 s_RowOut, 0 
    		.rdrf_outer_loop_start:
    			# get_rowa
    			v_mov_b32 v_RowInOut, state[0] quad_perm:[0, 0, 0, 0]
    			v_and_b32 v_RowInOut, v_RowInOut, 0x3

				s_mul_i32 		     v_offset, s_RowIn, ROW_OFFSET
				s_set_gpr_idx_on	 v_offset, 1 

				# state1 = matrix[rowIn]
    			v_mov_b32 			    state1[0], matrix[0]
    			v_mov_b32 			    state1[1], matrix[1]
    			v_mov_b32 			    state1[2], matrix[2]
    			v_mov_b32 			    state1[3], matrix[3]
    			v_mov_b32 			    state1[4], matrix[4]
    			v_mov_b32 			    state1[5], matrix[5]

    			v_mov_b32 			    state1[6], matrix[6]
    			v_mov_b32 			    state1[7], matrix[7]
    			v_mov_b32 			    state1[8], matrix[8]
    			v_mov_b32 			    state1[9], matrix[9]
    			v_mov_b32 			    state1[10], matrix[10]
    			v_mov_b32 			    state1[11], matrix[11]

    			v_mov_b32 			    state1[12], matrix[12]
    			v_mov_b32 			    state1[13], matrix[13]
    			v_mov_b32 			    state1[14], matrix[14]
    			v_mov_b32 			    state1[15], matrix[15]
    			v_mov_b32 			    state1[16], matrix[16]
    			v_mov_b32 			    state1[17], matrix[17]

    			v_mov_b32 			    state1[18], matrix[18]
    			v_mov_b32 			    state1[19], matrix[19]
    			v_mov_b32 			    state1[20], matrix[20]
    			v_mov_b32 			    state1[21], matrix[21]
    			v_mov_b32 			    state1[22], matrix[22]
    			v_mov_b32 			    state1[23], matrix[23]
    			s_set_gpr_idx_off
    			
    			# state2 = matrix[rowInOut]
    			LINK_L1
				s_branch				.rowa_to_state2_all

				# state1 += state2
				v_add_u32				state1[0], vcc, state1[0], state2[0]
				v_addc_u32				state1[1], vcc, state1[1], state2[1], vcc

				v_add_u32				state1[2], vcc, state1[2], state2[2]
				v_addc_u32				state1[3], vcc, state1[3], state2[3], vcc

				v_add_u32				state1[4], vcc, state1[4], state2[4]
				v_addc_u32				state1[5], vcc, state1[5], state2[5], vcc

				v_add_u32				state1[6], vcc, state1[6], state2[6]
				v_addc_u32				state1[7], vcc, state1[7], state2[7], vcc

				v_add_u32				state1[8], vcc, state1[8], state2[8]
				v_addc_u32				state1[9], vcc, state1[9], state2[9], vcc

				v_add_u32				state1[10], vcc, state1[10], state2[10]
				v_addc_u32				state1[11], vcc, state1[11], state2[11], vcc

				v_add_u32				state1[12], vcc, state1[12], state2[12]
				v_addc_u32				state1[13], vcc, state1[13], state2[13], vcc

				v_add_u32				state1[14], vcc, state1[14], state2[14]
				v_addc_u32				state1[15], vcc, state1[15], state2[15], vcc

				v_add_u32				state1[16], vcc, state1[16], state2[16]
				v_addc_u32				state1[17], vcc, state1[17], state2[17], vcc

				v_add_u32				state1[18], vcc, state1[18], state2[18]
				v_addc_u32				state1[19], vcc, state1[19], state2[19], vcc

				v_add_u32				state1[20], vcc, state1[20], state2[20]
				v_addc_u32				state1[21], vcc, state1[21], state2[21], vcc

				v_add_u32				state1[22], vcc, state1[22], state2[22]
				v_addc_u32				state1[23], vcc, state1[23], state2[23], vcc


    			s_mov_b32 ilc, 0  #col_sel * COL_OFFSET
    			.rdrf_inner_loop_start:
    				s_set_gpr_idx_on	ilc, 1
    				v_xor_b32 			state[0], state1[0], state[0]
    				v_xor_b32 			state[1], state1[1], state[1]
    				v_xor_b32 			state[2], state1[2], state[2]
    				v_xor_b32 			state[3], state1[3], state[3]
    				v_xor_b32 			state[4], state1[4], state[4]
    				v_xor_b32 			state[5], state1[5], state[5]
    				s_set_gpr_idx_off
					
					LINK_L1
					s_branch			.round_lyra
					LINK_L1 
					s_branch 			.dance_of_12

					# (rowInOut == RowOut ?)
					v_cmp_eq_u32 		vcc, v_RowInOut, s_RowOut
						s_and_saveexec_b64  esave_l1[0:1], vcc
						## state2[col] ^= state
						s_set_gpr_idx_on    ilc, 0x9
						v_xor_b32 			state2[0], state2[0], state[0]
						v_xor_b32 			state2[1], state2[1], state[1]
						v_xor_b32 			state2[2], state2[2], state[2]
						v_xor_b32 			state2[3], state2[3], state[3]
						v_xor_b32 			state2[4], state2[4], state[4]
						v_xor_b32 			state2[5], state2[5], state[5]
						s_set_gpr_idx_off
					##else
					s_not_b64			exec, exec
						s_mul_i32			v_offset, ROW_OFFSET, s_RowOut
						s_add_u32			v_offset, v_offset, ilc
						s_set_gpr_idx_on 	v_offset, 0x9

						v_xor_b32			matrix[0], matrix[0], state[0]
						v_xor_b32			matrix[1], matrix[1], state[1]
						v_xor_b32			matrix[2], matrix[2], state[2]
						v_xor_b32			matrix[3], matrix[3], state[3]
						v_xor_b32			matrix[4], matrix[4], state[4]
						v_xor_b32			matrix[5], matrix[5], state[5]
						s_set_gpr_idx_off
					s_mov_b64			exec, esave_l1[0:1]
    			.rdrf_inner_loop_end:
    				s_add_u32			ilc, ilc, COL_OFFSET
    				s_cmpk_eq_u32		ilc, ROW_OFFSET
    				s_cbranch_scc0		.rdrf_inner_loop_start
				
			.rdrf_outer_lop_end:
				LINK_L1
				s_branch			.state2_to_rowa_all
				# rowIn = prev
				s_mov_b32 	        s_RowIn, s_RowOut
				s_add_u32 	        s_RowOut, s_RowOut, 1
				s_cmpk_eq_u32       s_RowOut, 4
				s_cbranch_scc0      .rdrf_outer_loop_start
			
		

			# at this point, state2[0]  contains matrix[rowA][0]
			# state.d8 ^= matrix[rowA][0]
			v_xor_b32			state[0], state[0], state2[0]
			v_xor_b32			state[1], state[1], state2[1]
			v_xor_b32			state[2], state[2], state2[2]
			v_xor_b32			state[3], state[3], state2[3]
			v_xor_b32			state[4], state[4], state2[4]
			v_xor_b32			state[5], state[5], state2[5]
			

			LINK_L0
			s_branch			.round_lyra_12
			s_branch            .end_of_main



	
	# branchable routines

	.round_lyra_12:
		s_mov_b32 olc, 0
		.round_lyra_12_loop_start:
			ROUND_LYRA
			ROUND_LYRA
		.round_lyra_12_loop_end:
		s_add_u32 olc, olc, 2
		s_cmpk_eq_u32 olc, 12
		s_cbranch_scc0 .round_lyra_12_loop_start
		RET_L0
	
	.round_lyra:
		ROUND_LYRA
		RET_L1
	
	.dance_of_12:
		# need a vtemp reg here but shit out of luck, so use v_offset_calc instead
		 v_cmp_eq_u32       vcc,  pos, 3             #pos == 3 ?
		 s_and_saveexec_b64 esave_l0[0:1], vcc
		 s_mov_b64          esave_l1[0:1], exec    # save pos0 exec mask for later
		
		 v_mov_b32          v_offset_calc, state[0]          #pos3.temp.x = pos3.state[0].x
		 v_mov_b32          state[0], state[4]
		 v_mov_b32          state[4], state[2]
		 v_mov_b32          state[2], v_offset_calc

		 v_mov_b32          v_offset_calc, state[1]          #pos3.temp.y = pos3.state[0].y
		 v_mov_b32          state[1], state[5]
		 v_mov_b32          state[5], state[3]
		 v_mov_b32          state[3], v_offset_calc
		
		 s_mov_b64          exec, esave_l0[0:1]

		 # want to enable GPR IDX offset for state2, so dest, and src1 (not src0 or src2) (4b'1010 = 0xA)
		 s_set_gpr_idx_on	ilc, 0xA
		 v_nop
		 v_nop
		 v_xor_b32          state2[0], state[0], state2[0] quad_perm:[3, 0, 1, 2]
		 v_xor_b32          state2[1], state[1], state2[1] quad_perm:[3, 0, 1, 2]
		 
		 v_nop
		 v_nop
		 v_xor_b32          state2[2], state[2], state2[2] quad_perm:[3, 0, 1, 2]
		 v_xor_b32          state2[3], state[3], state2[3] quad_perm:[3, 0, 1, 2]
		 
		 v_nop
		 v_nop
		 v_xor_b32          state2[4], state[4], state2[4] quad_perm:[3, 0, 1, 2]
		 v_xor_b32          state2[5], state[5], state2[5] quad_perm:[3, 0, 1, 2]
		
		 v_nop
		 v_nop
		 s_set_gpr_idx_off
		 s_mov_b64          exec, esave_l1[0:1]
		 
		 #v_cmp_eq_u32 vcc,  v2, 3             #pos == 3 ?
		 #s_and_saveexec_b64 s[12:13], vcc
		
		 v_mov_b32          v_offset_calc, state[2]    
		 v_mov_b32          state[2], state[4]  
		 v_mov_b32          state[4], state[0]  
		 v_mov_b32          state[0], v_offset_calc

		 v_mov_b32          v_offset_calc, state[3]
		 v_mov_b32          state[3], state[5]
		 v_mov_b32          state[5], state[1]
		 v_mov_b32          state[1], v_offset_calc
		
		 s_mov_b64          exec, esave_l0[0:1]
		 RET_L1

	.reduce_duplex_row_setup_f:
		s_mov_b32           olc, 0
		s_mov_b32			ilc, 0 
		# in this routine, state2[0-5] are only used. Need to make sure ilc == 0
		# so that the VGPR offset idx within dance_of_12 always targets the first column
		.reduce_duplex_row_setup_f_loop_start:
			# state1 = matrix[RowIn][col_sel]
			s_mul_i32 			v_offset, s_RowIn, ROW_OFFSET
			s_add_u32           v_offset, v_offset, olc
			s_set_gpr_idx_on    v_offset, 1               # vgpr offset src
  			v_mov_b32           state1[0], matrix[0]
  			v_mov_b32           state1[1], matrix[1]
  			v_mov_b32           state1[2], matrix[2]
  			v_mov_b32           state1[3], matrix[3]
  			v_mov_b32           state1[4], matrix[4]
  			v_mov_b32           state1[5], matrix[5]
  			s_set_gpr_idx_off

			# state2 = matrix[RowInOut][col_sel]
  			s_mul_i32           v_offset, s_RowInOut, ROW_OFFSET
  			s_add_u32           v_offset, v_offset, olc
			s_set_gpr_idx_on    v_offset, 1               # vgpr offset src
  		    v_mov_b32           state2[0], matrix[0]
  		    v_mov_b32           state2[1], matrix[1]
  		    v_mov_b32           state2[2], matrix[2]
  		    v_mov_b32           state2[3], matrix[3]
  		    v_mov_b32           state2[4], matrix[4]
  		    v_mov_b32           state2[5], matrix[5]
  			s_set_gpr_idx_off

			# state ^= (state1 + state2)
            v_add_u32           v_rowtemp[0], vcc, state1[0], state2[0]  
            v_addc_u32          v_rowtemp[1], vcc, state1[1], state2[1], vcc 
            
            v_xor_b32           state[0], state[0], v_rowtemp[0] 		
            v_xor_b32           state[1], state[1], v_rowtemp[1]     

            v_add_u32           v_rowtemp[0], vcc, state1[2], state2[2]   
            v_addc_u32          v_rowtemp[1], vcc, state1[3], state2[3], vcc 
            v_xor_b32           state[2], state[2], v_rowtemp[0] 
            v_xor_b32           state[3], state[3], v_rowtemp[1]     
            
            v_add_u32           v_rowtemp[0], vcc, state1[4], state2[4] 
            v_addc_u32          v_rowtemp[1], vcc, state1[5], state2[5], vcc 
            v_xor_b32           state[4], state[4], v_rowtemp[0] 
            v_xor_b32           state[5], state[5], v_rowtemp[1]     

            LINK_L1
            s_branch            .round_lyra
			
			# This next part is interesting:
			# -must swap between using vgpr offsets and not using them
			# -rather than using s_set_gpr_idx, use v_movreld instead
			# -only movreld will be affected by the offset

			# matrix[RowOut][3 - col_sel]
	        s_mov_b32         stemp[1], m0         /* save m0 in temp */
	        s_mul_i32         v_offset, 3, COL_OFFSET # (3*COL_OFFSET) - COL_OFFSET
	        s_sub_u32         v_offset, v_offset, olc  
	        s_mul_i32         stemp[2], s_RowOut, ROW_OFFSET
	        s_add_u32         v_offset, v_offset, stemp[2]
	        s_mov_b32         m0, v_offset            /* matrix[rowOut][3 - col_sel] */

			# state1 ^= state
	        v_xor_b32         state1[0], state1[0], state[0] 
	        v_xor_b32         state1[1], state1[1], state[1] 
	        v_movreld_b32     matrix[0], state1[0]
	        v_movreld_b32     matrix[1], state1[1]
	        v_xor_b32         state1[2], state1[2], state[2] 
	        v_xor_b32         state1[3], state1[3], state[3] 
	        v_movreld_b32     matrix[2], state1[2];
	        v_movreld_b32     matrix[3], state1[3]
	        v_xor_b32         state1[4], state1[4], state[4] 
	        v_xor_b32         state1[5], state1[5], state[5]
	        v_movreld_b32     matrix[4], state1[4]
	        v_movreld_b32     matrix[5], state1[5]
	        s_mov_b32         m0, stemp[1]  

	        LINK_L1
	        s_branch         .dance_of_12

	        s_mul_i32         v_offset, s_RowInOut, ROW_OFFSET
	        s_add_u32         v_offset, v_offset, olc

			s_set_gpr_idx_on   v_offset, 8    # voffset dest
            v_mov_b32          matrix[0], state2[0]
            v_mov_b32          matrix[1], state2[1]
            v_mov_b32          matrix[2], state2[2]
            v_mov_b32          matrix[3], state2[3]
            v_mov_b32          matrix[4], state2[4]
            v_mov_b32          matrix[5], state2[5]
            s_set_gpr_idx_off
        .reduce_duplex_row_setup_f_loop_end:
        s_add_u32 			olc, olc, COL_OFFSET
        s_cmpk_eq_u32 		olc, ROW_OFFSET
        s_cbranch_scc0      .reduce_duplex_row_setup_f_loop_start
        RET_L0


	.rowa_to_state2:
		#v_offset prepopulated with rowInOut aka rowa
		s_mul_i32 			v_offset, v_offset, ROW_OFFSET
		s_set_gpr_idx_on	v_offset, 1

		v_mov_b32 			    state2[0], matrix[0]
    	v_mov_b32 			    state2[1], matrix[1]

    	v_mov_b32 			    state2[2], matrix[2]
    	v_mov_b32 			    state2[3], matrix[3]

    	v_mov_b32 			    state2[4], matrix[4]
    	v_mov_b32 			    state2[5], matrix[5]

    	v_mov_b32 			    state2[6], matrix[6]
    	v_mov_b32 			    state2[7], matrix[7]

    	v_mov_b32 			    state2[8], matrix[8]
    	v_mov_b32 			    state2[9], matrix[9]

    	v_mov_b32 			    state2[10], matrix[10]
    	v_mov_b32 			    state2[11], matrix[11]

    	v_mov_b32 			    state2[12], matrix[12]
    	v_mov_b32 			    state2[13], matrix[13]

    	v_mov_b32 			    state2[14], matrix[14]
    	v_mov_b32 			    state2[15], matrix[15]

    	v_mov_b32 			    state2[16], matrix[16]
    	v_mov_b32 			    state2[17], matrix[17]

    	v_mov_b32 			    state2[18], matrix[18]
    	v_mov_b32 			    state2[19], matrix[19]

    	v_mov_b32 			    state2[20], matrix[20]
    	v_mov_b32 			    state2[21], matrix[21]

    	v_mov_b32 			    state2[22], matrix[22]
    	v_mov_b32 			    state2[23], matrix[23]

    	s_set_gpr_idx_off
    	RET_L0
	
	.rowa_to_state2_all:
		v_cmp_eq_u32 			vcc, v_RowInOut, 0
		s_and_saveexec_b64		esave_l0[0:1], vcc
		s_mov_b32				v_offset, 0
		LINK_L0
		s_cbranch_execnz		.rowa_to_state2
		s_mov_b64				exec, esave_l0[0:1]

		v_cmp_eq_u32 			vcc, v_RowInOut, 1
		s_and_saveexec_b64		esave_l0[0:1], vcc
		s_mov_b32				v_offset, 1
		LINK_L0
		s_cbranch_execnz     	.rowa_to_state2
		s_mov_b64				exec, esave_l0[0:1]
		
		v_cmp_eq_u32 			vcc, v_RowInOut, 2
		s_and_saveexec_b64		esave_l0[0:1], vcc
		s_mov_b32				v_offset, 2
		LINK_L0
		s_cbranch_execnz		.rowa_to_state2
		s_mov_b64				exec, esave_l0[0:1]

		v_cmp_eq_u32 			vcc, v_RowInOut, 3
		s_and_saveexec_b64		esave_l0[0:1], vcc
		s_mov_b32				v_offset, 3
		LINK_L0
		s_cbranch_execnz		.rowa_to_state2
		s_mov_b64				exec, esave_l0[0:1]
		RET_L1
	
	.state2_to_rowa:
		s_mul_i32 			v_offset, v_offset, ROW_OFFSET
		s_set_gpr_idx_on	v_offset, 8

		v_mov_b32 			    matrix[0],     state2[0]
    	v_mov_b32 			    matrix[1],     state2[1] 
    	v_mov_b32 			    matrix[2],     state2[2]
    	v_mov_b32 			    matrix[3],     state2[3] 
    	v_mov_b32 			    matrix[4],     state2[4] 
    	v_mov_b32 			    matrix[5],     state2[5] 

    	v_mov_b32 			    matrix[6],     state2[6] 
    	v_mov_b32 			    matrix[7],     state2[7] 
    	v_mov_b32 			    matrix[8],     state2[8] 
    	v_mov_b32 			    matrix[9],     state2[9] 
    	v_mov_b32 			    matrix[10],    state2[10]
    	v_mov_b32 			    matrix[11],    state2[11]
                                                          
    	v_mov_b32 			    matrix[12],    state2[12]
    	v_mov_b32 			    matrix[13],    state2[13]
    	v_mov_b32 			    matrix[14],    state2[14]
    	v_mov_b32 			    matrix[15],    state2[15]
    	v_mov_b32 			    matrix[16],    state2[16]
    	v_mov_b32 			    matrix[17],    state2[17]
                                                          
    	v_mov_b32 			    matrix[18],    state2[18]
    	v_mov_b32 			    matrix[19],    state2[19]
    	v_mov_b32 			    matrix[20],    state2[20]
    	v_mov_b32 			    matrix[21],    state2[21]
    	v_mov_b32 			    matrix[22],    state2[22]
    	v_mov_b32 			    matrix[23],    state2[23]
    	s_set_gpr_idx_off
    	RET_L0
	
	.state2_to_rowa_all:
		v_cmp_eq_u32 			vcc, v_RowInOut, 0
		s_and_saveexec_b64		esave_l0[0:1], vcc
		s_mov_b32				v_offset, 0
		LINK_L0
		s_cbranch_execnz		.state2_to_rowa
		s_mov_b64				exec, esave_l0[0:1]

		v_cmp_eq_u32 			vcc, v_RowInOut, 1
		s_and_saveexec_b64		esave_l0[0:1], vcc
		s_mov_b32				v_offset, 1
		LINK_L0
		s_cbranch_execnz		.state2_to_rowa
		s_mov_b64				exec, esave_l0[0:1]
		
		v_cmp_eq_u32 			vcc, v_RowInOut, 2
		s_and_saveexec_b64		esave_l0[0:1], vcc
		s_mov_b32				v_offset, 2
		LINK_L0
		s_cbranch_execnz		.state2_to_rowa
		s_mov_b64				exec, esave_l0[0:1]
		
		v_cmp_eq_u32 			vcc, v_RowInOut, 3
		s_and_saveexec_b64		esave_l0[0:1], vcc
		s_mov_b32				v_offset, 3
		LINK_L0
		s_cbranch_execnz		.state2_to_rowa
		s_mov_b64				exec, esave_l0[0:1]
		RET_L1

	.end_of_main:
		flat_store_dwordx2 hashes_hidx[0:1], state[0:1]
		s_waitcnt         vmcnt(0) & lgkmcnt(0)
		s_waitcnt          expcnt(0)
		s_endpgm
