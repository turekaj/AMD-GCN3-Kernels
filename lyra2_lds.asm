.amdcl2
.gpu Ellesmere
.64bit
.arch_minor 0
.arch_stepping 4
.driver_version 203603
.compile_options "-I /home/turekaj/code/nextminer_working/nextminer/src -DFP_FAST_FMA=1 -cl-denorms-are-zero -m64 -Dcl_khr_fp64=1 -Dcl_amd_fp64=1 -Dcl_khr_global_int32_base_atomics=1 -Dcl_khr_global_int32_extended_atomics=1 -Dcl_khr_local_int32_base_atomics=1 -Dcl_khr_local_int32_extended_atomics=1 -Dcl_khr_int64_base_atomics=1 -Dcl_khr_int64_extended_atomics=1 -Dcl_khr_3d_image_writes=1 -Dcl_khr_byte_addressable_store=1 -Dcl_khr_fp16=1 -Dcl_khr_gl_sharing=1 -Dcl_amd_device_attribute_query=1 -Dcl_amd_vec3=1 -Dcl_amd_printf=1 -Dcl_amd_media_ops=1 -Dcl_amd_media_ops2=1 -Dcl_amd_popcnt=1 -Dcl_khr_image2d_from_buffer=1 -Dcl_khr_spir=1 -Dcl_khr_gl_event=1"
.acl_version "AMD-COMP-LIB-v0.8 (0.0.SC_BUILD_NUMBER)"
.globaldata
.gdata:
    .byte 0x88, 0x6a, 0x3f, 0x24, 0xd3, 0x08, 0xa3, 0x85
    .byte 0x2e, 0x8a, 0x19, 0x13, 0x44, 0x73, 0x70, 0x03
    .byte 0x22, 0x38, 0x09, 0xa4, 0xd0, 0x31, 0x9f, 0x29
    .byte 0x98, 0xfa, 0x2e, 0x08, 0x89, 0x6c, 0x4e, 0xec
    .byte 0xe6, 0x21, 0x28, 0x45, 0x77, 0x13, 0xd0, 0x38
    .byte 0xcf, 0x66, 0x54, 0xbe, 0x6c, 0x0c, 0xe9, 0x34
    .byte 0xb7, 0x29, 0xac, 0xc0, 0xdd, 0x50, 0x7c, 0xc9
    .byte 0xb5, 0xd5, 0x84, 0x3f, 0x17, 0x09, 0x47, 0xb5
    .byte 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    .byte 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    .byte 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    .byte 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    .byte 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    .byte 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    .byte 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    .byte 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01
    .byte 0x08, 0xc9, 0xbc, 0xf3, 0x67, 0xe6, 0x09, 0x6a
    .byte 0x3b, 0xa7, 0xca, 0x84, 0x85, 0xae, 0x67, 0xbb
    .byte 0x2b, 0xf8, 0x94, 0xfe, 0x72, 0xf3, 0x6e, 0x3c
    .byte 0xf1, 0x36, 0x1d, 0x5f, 0x3a, 0xf5, 0x4f, 0xa5
    .byte 0xd1, 0x82, 0xe6, 0xad, 0x7f, 0x52, 0x0e, 0x51
    .byte 0x1f, 0x6c, 0x3e, 0x2b, 0x8c, 0x68, 0x05, 0x9b
    .byte 0x6b, 0xbd, 0x41, 0xfb, 0xab, 0xd9, 0x83, 0x1f
    .byte 0x79, 0x21, 0x7e, 0x13, 0x19, 0xcd, 0xe0, 0x5b
    .byte 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 #MASK[0][0]
    .byte 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 #MASK[0][1]
    .byte 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 #MASK[0][2]
    .byte 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 #MASK[0][3]
    .byte 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 #MASK[1][0]
    .byte 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 #MASK[1][1]
    .byte 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 #MASK[1][2]
    .byte 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01 #MASK[1][3]
    .byte 0x08, 0xc9, 0xbc, 0xf3, 0x67, 0xe6, 0x09, 0x6a #BLAKE_IV[0][0]
    .byte 0x3b, 0xa7, 0xca, 0x84, 0x85, 0xae, 0x67, 0xbb #BLAKE_IV[0][1]
    .byte 0x2b, 0xf8, 0x94, 0xfe, 0x72, 0xf3, 0x6e, 0x3c #BLAKE_IV[0][2]
    .byte 0xf1, 0x36, 0x1d, 0x5f, 0x3a, 0xf5, 0x4f, 0xa5 #BLAKE_IV[0][3]
    .byte 0xd1, 0x82, 0xe6, 0xad, 0x7f, 0x52, 0x0e, 0x51 #BLAKE_IV[1][0]
    .byte 0x1f, 0x6c, 0x3e, 0x2b, 0x8c, 0x68, 0x05, 0x9b #BLAKE_IV[1][1]
    .byte 0x6b, 0xbd, 0x41, 0xfb, 0xab, 0xd9, 0x83, 0x1f #BLAKE_IV[1][2]
    .byte 0x79, 0x21, 0x7e, 0x13, 0x19, 0xcd, 0xe0, 0x5b #BLAKE_IV[2][3]
.kernel lyra2
    .config
		.dims x
		.cws 64, 1, 1
		.sgprsnum 30
		.vgprsnum 80
        .floatmode 0xFF
		.localsize 32768
        .dx10clamp
        .ieeemode
        .useargs
        .priority 0
        .arg _.global_offset_0, "size_t", long
        .arg _.global_offset_1, "size_t", long
        .arg _.global_offset_2, "size_t", long
        .arg _.printf_buffer, "size_t", void*, global, , rdonly
        .arg _.vqueue_pointer, "size_t", long
        .arg _.aqlwrap_pointer, "size_t", long
        .arg hashes, "hash_t*", structure*, 32, global, 
        #.arg state,  "uint*", uint*, global, 
        #.arg matrix, "uint*", uint*, global, 
        #.arg v_state1, "uint*", uint*, global, 
        #.arg v_state2, "uint*", uint*, global, 
    .text
    	.set NUM_COLS, 4
    	.set NUM_ROWS, 4
    	.set ELEMENT_SIZE, 96
    	.set ROW_SIZE, (NUM_COLS * ELEMENT_SIZE)
    	.set HIDX_HASH_SHIFT, 5 # each input/output hash is 32B
    	.set POS_HASH_SHIFT, 3  # each WI 
		.set BLAKE_MASK_POS_SHIFT, 3
		.set MASK_OFFSET_0, 192
		.set MASK_OFFSET_1, (MASK_OFFSET_0 + 32)
		.set BLAKE_OFFSET_0, (MASK_OFFSET_1 + 32)
		.set BLAKE_OFFSET_1, (BLAKE_OFFSET_0 + 32)

    	#helper symbols
    	/* name: v_localid
    	 * description: contains this WI's index in the workgroup. Always within 0-63
    	 */
    	v_localid        = %v[0]

    	/* name: v_hidx
    	 * description: hash identifier for this WI's team (4 WIs work on the same hash
    	 */
    	v_hidx			 = %v[1]

    	/* name: v_pos
    	 * description: what position on the team this WI plays, always with 0-3
    	 */
    	v_pos			 = %v[2]

    	/* name: v_rowInOut
    	 * description: aka 'rowa', this is the per team random row selector
    	 */
    	v_rowInOut		 = %v[3]

    	/* name: v_state
    	 * description: 1/4 of the sponge state is stored in each WI's vgprs
    	 */

    	v_state			 = %v[4:11]

    	/* name: v_state1
    	 * description: temporary variables used to operate on the matrix and sponge
    	 */
    	v_state1		 = %v[12:35]

    	/* name: v_state2
    	 * description: temporary variables used to operate on the matrix and sponge
    	 */
    	v_state2		 = %v[36:59]

    	/* name: v_v_hashes_hidx
    	 * description: pointer to the input/outpu thash in global memory for this team + pos_offset
    	 */
    	v_hashes_hidx    = %v[60:61]
		

		# lds pointers
		/* name: lds_matrix
		 * description: points to the first ulong of the matrix managed by this pos
		 */
    	lds_matrix	      = %v[62]
    	lds_row			  = %v[63]

    	v_temp			  = %v[64:73]
    	v_mask			  = %v[68:71]

    	v_rowSize       = %v[72]
		v_rowIn	     	= %v[73]
		v_rowOut		= %v[74]
		v_teamidx	    = %v[75]

		# aliases
		s_userdata		= %s[0:6]
		s_args 			= %s[4:5]
		s_groupid		= %s[6]
		s_olc 			= %s[7]
		s_colOffset	    = %s[11]
		s_temp			= %s[12:15]
		s_esave_l0    	= %s[16:17]
		s_esave_l1    	= %s[18:19]
		s_state_mem     = %s[20:21]
		s_matrix_mem    = %s[22:23]
		s_state1_mem 	= %s[24:25]
		s_state2_mem    = %s[26:27]
		
		.macro STORE_STATE
			v_lshlrev_b32 v_temp[0], 7, v_hidx
			v_lshlrev_b32 v_temp[1], POS_HASH_SHIFT, v_pos
			v_add_u32     v_temp[0], vcc, v_temp[0], v_temp[1]
			v_mov_b32     v_temp[2], s_state_mem[0]
			v_mov_b32 	  v_temp[3], s_state_mem[1]
			v_add_u32     v_temp[2], vcc, v_temp[2], v_temp[0]
			v_addc_u32    v_temp[3], vcc, v_temp[3], 0, vcc

			flat_store_dwordx2  v_temp[2:3], v_state[0:1]
 			s_waitcnt           vmcnt(0) & lgkmcnt(0)
			s_waitcnt           expcnt(0)

			v_add_u32 			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32 			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2  v_temp[2:3], v_state[2:3]
 			s_waitcnt           vmcnt(0) & lgkmcnt(0)
			s_waitcnt           expcnt(0)


			v_add_u32 			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32 			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2  v_temp[2:3], v_state[4:5]
 			s_waitcnt           vmcnt(0) & lgkmcnt(0)
			s_waitcnt           expcnt(0)

			v_add_u32 			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32 			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2  v_temp[2:3], v_state[6:7]
 			s_waitcnt           vmcnt(0) & lgkmcnt(0)
			s_waitcnt           expcnt(0)
		.endm
		
		.macro STORE_STATE1
			v_lshlrev_b32 v_temp[0], 7, v_hidx
			v_lshlrev_b32 v_temp[1], POS_HASH_SHIFT, v_pos
			v_add_u32     v_temp[0], vcc, v_temp[0], v_temp[1]
			v_mov_b32     v_temp[2], s_state1_mem[0]
			v_mov_b32 	  v_temp[3], s_state1_mem[1]

			v_add_u32     v_temp[2], vcc, v_temp[2], v_temp[0]
			v_addc_u32    v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2  v_temp[2:3], v_state1[0:1]
 			s_waitcnt           vmcnt(0) & lgkmcnt(0)
			s_waitcnt           expcnt(0)

			v_add_u32 			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32 			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2  v_temp[2:3], v_state1[2:3]
 			s_waitcnt           vmcnt(0) & lgkmcnt(0)
			s_waitcnt           expcnt(0)


			v_add_u32 			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32 			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2  v_temp[2:3], v_state1[4:5]
 			s_waitcnt           vmcnt(0) & lgkmcnt(0)
			s_waitcnt           expcnt(0)
			
			v_add_u32     v_temp[2], vcc, v_temp[2], v_temp[0]
			v_addc_u32    v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2  v_temp[2:3], v_state1[6:7]
 			s_waitcnt           vmcnt(0) & lgkmcnt(0)
			s_waitcnt           expcnt(0)

			v_add_u32 			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32 			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2  v_temp[2:3], v_state1[8:9]
 			s_waitcnt           vmcnt(0) & lgkmcnt(0)
			s_waitcnt           expcnt(0)


			v_add_u32 			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32 			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2  v_temp[2:3], v_state1[10:11]
 			s_waitcnt           vmcnt(0) & lgkmcnt(0)
			s_waitcnt           expcnt(0)
			
			v_add_u32     v_temp[2], vcc, v_temp[2], v_temp[0]
			v_addc_u32    v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2  v_temp[2:3], v_state1[12:13]
 			s_waitcnt           vmcnt(0) & lgkmcnt(0)
			s_waitcnt           expcnt(0)

			v_add_u32 			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32 			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2  v_temp[2:3], v_state1[14:15]
 			s_waitcnt           vmcnt(0) & lgkmcnt(0)
			s_waitcnt           expcnt(0)


			v_add_u32 			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32 			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2  v_temp[2:3], v_state1[16:17]
 			s_waitcnt           vmcnt(0) & lgkmcnt(0)
			s_waitcnt           expcnt(0)
			
			v_add_u32     v_temp[2], vcc, v_temp[2], v_temp[0]
			v_addc_u32    v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2  v_temp[2:3], v_state1[18:19]
 			s_waitcnt           vmcnt(0) & lgkmcnt(0)
			s_waitcnt           expcnt(0)

			v_add_u32 			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32 			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2  v_temp[2:3], v_state1[20:21]
 			s_waitcnt           vmcnt(0) & lgkmcnt(0)
			s_waitcnt           expcnt(0)


			v_add_u32 			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32 			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2  v_temp[2:3], v_state1[22:23]
 			s_waitcnt           vmcnt(0) & lgkmcnt(0)
			s_waitcnt           expcnt(0)
		.endm

		.macro STORE_STATE2
			v_lshlrev_b32 v_temp[0], 7, v_hidx
			v_lshlrev_b32 v_temp[1], POS_HASH_SHIFT, v_pos
			v_add_u32     v_temp[0], vcc, v_temp[0], v_temp[1]
			v_mov_b32     v_temp[2], s_state2_mem[0]
			v_mov_b32 	  v_temp[3], s_state2_mem[1]

			v_add_u32     v_temp[2], vcc, v_temp[2], v_temp[0]
			v_addc_u32    v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2  v_temp[2:3], v_state2[0:1]
 			s_waitcnt           vmcnt(0) & lgkmcnt(0)
			s_waitcnt           expcnt(0)

			v_add_u32 			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32 			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2  v_temp[2:3], v_state2[2:3]
 			s_waitcnt           vmcnt(0) & lgkmcnt(0)
			s_waitcnt           expcnt(0)


			v_add_u32 			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32 			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2  v_temp[2:3], v_state2[4:5]
 			s_waitcnt           vmcnt(0) & lgkmcnt(0)
			s_waitcnt           expcnt(0)
			
			v_add_u32     v_temp[2], vcc, v_temp[2], v_temp[0]
			v_addc_u32    v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2  v_temp[2:3], v_state2[6:7]
 			s_waitcnt           vmcnt(0) & lgkmcnt(0)
			s_waitcnt           expcnt(0)

			v_add_u32 			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32 			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2  v_temp[2:3], v_state2[8:9]
 			s_waitcnt           vmcnt(0) & lgkmcnt(0)
			s_waitcnt           expcnt(0)


			v_add_u32 			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32 			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2  v_temp[2:3], v_state2[10:11]
 			s_waitcnt           vmcnt(0) & lgkmcnt(0)
			s_waitcnt           expcnt(0)
			
			v_add_u32     v_temp[2], vcc, v_temp[2], v_temp[0]
			v_addc_u32    v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2  v_temp[2:3], v_state2[12:13]
 			s_waitcnt           vmcnt(0) & lgkmcnt(0)
			s_waitcnt           expcnt(0)

			v_add_u32 			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32 			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2  v_temp[2:3], v_state2[14:15]
 			s_waitcnt           vmcnt(0) & lgkmcnt(0)
			s_waitcnt           expcnt(0)


			v_add_u32 			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32 			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2  v_temp[2:3], v_state2[16:17]
 			s_waitcnt           vmcnt(0) & lgkmcnt(0)
			s_waitcnt           expcnt(0)
			
			v_add_u32     v_temp[2], vcc, v_temp[2], v_temp[0]
			v_addc_u32    v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2  v_temp[2:3], v_state2[18:19]
 			s_waitcnt           vmcnt(0) & lgkmcnt(0)
			s_waitcnt           expcnt(0)

			v_add_u32 			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32 			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2  v_temp[2:3], v_state2[20:21]
 			s_waitcnt           vmcnt(0) & lgkmcnt(0)
			s_waitcnt           expcnt(0)


			v_add_u32 			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32 			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2  v_temp[2:3], v_state2[22:23]
 			s_waitcnt           vmcnt(0) & lgkmcnt(0)
			s_waitcnt           expcnt(0)
		.endm
	

		.macro PARALLEL_G
  			# v_state[0] += v_state[1]; v_state[3] ^= v_state[0]; v_state[3] = rotate(v_state[3], 32UL) 
  			v_add_u32       v_state[0], vcc, v_state[0], v_state[2]
  			v_addc_u32      v_state[1], vcc, v_state[1], v_state[3], vcc
  			v_xor_b32       v_state[6], v_state[6], v_state[0] 
  			v_xor_b32       v_state[7], v_state[7], v_state[1]
  			v_mov_b32       v_temp[0], v_state[7]				# v_state[3] = rotate(v_state[3], 32UL) 
  			v_mov_b32       v_state[7], v_state[6] 
  			v_mov_b32       v_state[6], v_temp[0]
  			
  			# v_state[2] += v_state[3]; v_state[1] ^= v_state[2]; v_state[1] = rotate(v_state[1], 40UL) 
  			v_add_u32       v_state[4], vcc, v_state[4], v_state[6]    #  s[2] += s[3] 
  			v_addc_u32      v_state[5], vcc, v_state[5], v_state[7], vcc 
  			v_xor_b32       v_state[2], v_state[4], v_state[2]         # s[1] ^= s[2] 
  			v_xor_b32       v_state[3], v_state[3], v_state[5]
  			v_alignbit_b32  v_temp[0], v_state[2], v_state[3], (32 - (40 - 32))
  			v_alignbit_b32  v_state[2], v_state[3], v_state[2], (32 - (40 - 32))
  			v_mov_b32       v_state[3], v_temp[0]
  			
  			# v_state[0] += v_state[1]; v_state[3] ^= v_state[0]; v_state[3] = rotate(v_state[3], 48UL) 
  			v_add_u32       v_state[0], vcc, v_state[0], v_state[2]              # s[0] += s[1] 
  			v_addc_u32      v_state[1], vcc, v_state[1], v_state[3], vcc              
  			v_xor_b32       v_state[6], v_state[6], v_state[0]                   # s[3] ^= s[0] 
  			v_xor_b32       v_state[7], v_state[1], v_state[7]         
  			v_alignbit_b32  v_temp[0], v_state[6], v_state[7], (32 - (48 - 32))         # v_state[3] = rotate(v_state[3], 48UL) 
  			v_alignbit_b32  v_state[6], v_state[7], v_state[6], (32 - (48 - 32))
  			v_mov_b32       v_state[7], v_temp[0]

  			# v_state[2] += v_state[3]; v_state[1] ^= v_state[2]; v_state[1] = rotate(v_state[1], 1L) 
  			v_add_u32       v_state[4], vcc, v_state[4], v_state[6]              # s[2] += s[3] 
  			v_addc_u32      v_state[5], vcc, v_state[5], v_state[7], vcc              
  			v_xor_b32       v_state[2], v_state[2], v_state[4]                   # s[1] ^= s[2] 
  			v_xor_b32       v_state[3], v_state[3], v_state[5]    
  			v_alignbit_b32  v_temp[0], v_state[2], v_state[3], (32 - 1)         # v_state[1] = rotate(v_state[1], 1L) 
  			v_alignbit_b32  v_state[3], v_state[3], v_state[2], (32 - 1)
  			v_mov_b32       v_state[2], v_temp[0]
		.endm

		.macro ROUND_LYRA
		  PARALLEL_G 		# PARALLEL_G(state[0], state[1], state[2], state[3])
		  
		  v_nop
		  v_nop
		  v_mov_b32        v_state[2], v_state[2] quad_perm:[1,2,3,0]  # state[1].s1230 
		  v_mov_b32        v_state[3], v_state[3] quad_perm:[1,2,3,0]  
		  v_mov_b32        v_state[4], v_state[4] quad_perm:[2,3,0,1]  # state[2].s2301
		  v_mov_b32        v_state[5], v_state[5] quad_perm:[2,3,0,1]
		  v_mov_b32        v_state[6], v_state[6] quad_perm:[3,0,1,2]  # state[3].s3012 
		  v_mov_b32        v_state[7], v_state[7] quad_perm:[3,0,1,2]
		  
		  PARALLEL_G 		# PARALLEL_G(state[2], state[1].s1230, state[2].2301, state[3].s3012)
		  v_nop
		  v_nop
		  
		  v_mov_b32        v_state[2], v_state[2] quad_perm:[3,0,1,2]
		  v_mov_b32        v_state[3], v_state[3] quad_perm:[3,0,1,2]
		  v_mov_b32        v_state[4], v_state[4] quad_perm:[2,3,0,1]
		  v_mov_b32        v_state[5], v_state[5] quad_perm:[2,3,0,1]
		  v_mov_b32        v_state[6], v_state[6] quad_perm:[1,2,3,0]
		  v_mov_b32        v_state[7], v_state[7] quad_perm:[1,2,3,0]
		.endm

		.macro ROUND_LYRA_12
			ROUND_LYRA
			ROUND_LYRA
			ROUND_LYRA
			ROUND_LYRA
			ROUND_LYRA
			ROUND_LYRA
			ROUND_LYRA
			ROUND_LYRA
			ROUND_LYRA
			ROUND_LYRA
			ROUND_LYRA
			ROUND_LYRA
		.endm

		.macro READ_ElEMENT, dest, col, col_offs
			ds_read_b64			   \dest[(\col * 6) + 0:(\col * 6) + 1], lds_row offset:(0 * 8 * 4) + ((\col_offs) * ELEMENT_SIZE)
			ds_read_b64			   \dest[(\col * 6) + 2:(\col * 6) + 3], lds_row offset:(1 * 8 * 4) + ((\col_offs) * ELEMENT_SIZE)
			ds_read_b64			   \dest[(\col * 6) + 4:(\col * 6) + 5], lds_row offset:(2 * 8 * 4) + ((\col_offs) * ELEMENT_SIZE)
		.endm


		.macro STORE_MATRIX
			
			v_lshlrev_b32 v_temp[0], POS_HASH_SHIFT, v_pos #pos offset
			v_mov_b32	  v_temp[1], 1536
			v_mad_u32_u24 v_temp[1], v_temp[1], v_hidx, v_temp[0]
			v_mov_b32     v_temp[2], s_matrix_mem[0]
			v_mov_b32     v_temp[3], s_matrix_mem[1]

    		v_mov_b32	  		v_temp[1], 0
    		v_mad_u32_u24		lds_row, v_rowSize, v_temp[1], lds_matrix
    		READ_ELEMENT		v_state1, 0, 0
    		READ_ELEMENT		v_state1, 1, 1
			READ_ELEMENT		v_state1, 2, 2
			READ_ELEMENT		v_state1, 3, 3
 			s_waitcnt           vmcnt(0) & lgkmcnt(0)

			flat_store_dwordx2	v_temp[2:3], v_state1[0:1]
			v_add_u32			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2	v_temp[2:3], v_state1[2:3]
			v_add_u32			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2	v_temp[2:3], v_state1[4:5]
			v_add_u32			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2	v_temp[2:3], v_state1[6:7]
			v_add_u32			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2	v_temp[2:3], v_state1[8:9]
			v_add_u32			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2	v_temp[2:3], v_state1[10:11]
			v_add_u32			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2	v_temp[2:3], v_state1[12:13]
			v_add_u32			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2	v_temp[2:3], v_state1[14:15]
			v_add_u32			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2	v_temp[2:3], v_state1[16:17]
			v_add_u32			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2	v_temp[2:3], v_state1[18:19]
			v_add_u32			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2	v_temp[2:3], v_state1[20:21]
			v_add_u32			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2	v_temp[2:3], v_state1[22:23]
			
			v_mov_b32	  		v_temp[1], 1
			v_mad_u32_u24		lds_row, v_rowSize, v_temp[1], lds_matrix
			READ_ELEMENT		v_state1, 0, 0
			READ_ELEMENT		v_state1, 1, 1
			READ_ELEMENT		v_state1, 2, 2
			READ_ELEMENT		v_state1, 3, 3
 			s_waitcnt           vmcnt(0) & lgkmcnt(0)

			flat_store_dwordx2	v_temp[2:3], v_state1[0:1]
			v_add_u32			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2	v_temp[2:3], v_state1[2:3]
			v_add_u32			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2	v_temp[2:3], v_state1[4:5]
			v_add_u32			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2	v_temp[2:3], v_state1[6:7]
			v_add_u32			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2	v_temp[2:3], v_state1[8:9]
			v_add_u32			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2	v_temp[2:3], v_state1[10:11]
			v_add_u32			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2	v_temp[2:3], v_state1[12:13]
			v_add_u32			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2	v_temp[2:3], v_state1[14:15]
			v_add_u32			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2	v_temp[2:3], v_state1[16:17]
			v_add_u32			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2	v_temp[2:3], v_state1[18:19]
			v_add_u32			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2	v_temp[2:3], v_state1[20:21]
			v_add_u32			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2	v_temp[2:3], v_state1[22:23]
			
			v_mov_b32	  		v_temp[1], 2
			v_mad_u32_u24		lds_row, v_rowSize, v_temp[1], lds_matrix
			READ_ELEMENT		v_state1, 0, 0
			READ_ELEMENT		v_state1, 1, 1
			READ_ELEMENT		v_state1, 2, 2
			READ_ELEMENT		v_state1, 3, 3
 			s_waitcnt           vmcnt(0) & lgkmcnt(0)

			flat_store_dwordx2	v_temp[2:3], v_state1[0:1]
			v_add_u32			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2	v_temp[2:3], v_state1[2:3]
			v_add_u32			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2	v_temp[2:3], v_state1[4:5]
			v_add_u32			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2	v_temp[2:3], v_state1[6:7]
			v_add_u32			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2	v_temp[2:3], v_state1[8:9]
			v_add_u32			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2	v_temp[2:3], v_state1[10:11]
			v_add_u32			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2	v_temp[2:3], v_state1[12:13]
			v_add_u32			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2	v_temp[2:3], v_state1[14:15]
			v_add_u32			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2	v_temp[2:3], v_state1[16:17]
			v_add_u32			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2	v_temp[2:3], v_state1[18:19]
			v_add_u32			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2	v_temp[2:3], v_state1[20:21]
			v_add_u32			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2	v_temp[2:3], v_state1[22:23]
			
			v_mov_b32	  		v_temp[1], 3
			v_mad_u32_u24		lds_row, v_rowSize, v_temp[1], lds_matrix
			READ_ELEMENT		v_state1, 0, 0
			READ_ELEMENT		v_state1, 1, 1
			READ_ELEMENT		v_state1, 2, 2
			READ_ELEMENT		v_state1, 3, 3
 			s_waitcnt           vmcnt(0) & lgkmcnt(0)

			flat_store_dwordx2	v_temp[2:3], v_state1[0:1]
			v_add_u32			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2	v_temp[2:3], v_state1[2:3]
			v_add_u32			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2	v_temp[2:3], v_state1[4:5]
			v_add_u32			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2	v_temp[2:3], v_state1[6:7]
			v_add_u32			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2	v_temp[2:3], v_state1[8:9]
			v_add_u32			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2	v_temp[2:3], v_state1[10:11]
			v_add_u32			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2	v_temp[2:3], v_state1[12:13]
			v_add_u32			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2	v_temp[2:3], v_state1[14:15]
			v_add_u32			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2	v_temp[2:3], v_state1[16:17]
			v_add_u32			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2	v_temp[2:3], v_state1[18:19]
			v_add_u32			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2	v_temp[2:3], v_state1[20:21]
			v_add_u32			v_temp[2], vcc, v_temp[2], 32
			v_addc_u32			v_temp[3], vcc, v_temp[3], 0, vcc
			flat_store_dwordx2	v_temp[2:3], v_state1[22:23]
 			s_waitcnt           vmcnt(0) & lgkmcnt(0)
 			s_waitcnt           expcnt(0)
		.endm

		.macro STORE_DEBUG
		    s_mov_b64	exec, 0xffffffffffffffff
			flat_store_dwordx2 v_hashes_hidx[0:1], v_state[0:1]
			STORE_STATE
			STORE_STATE1
			STORE_STATE2
			STORE_MATRIX
			READ_LDS_SERIAL
			s_endpgm
		.endm

		.macro READ_LDS_SERIAL
			v_lshlrev_b32 v_temp[0], POS_HASH_SHIFT, v_pos #pos offset
			v_mov_b32	  v_temp[1], 1536
			v_mad_u32_u24 v_temp[1], v_temp[1], v_hidx, v_temp[0]
			v_mov_b32     v_temp[2], s_matrix_mem[0]
			v_mov_b32     v_temp[3], s_matrix_mem[1]
			v_add_u32	  v_temp[2], vcc, v_temp[2], v_temp[1]
			v_addc_u32	  v_temp[3], vcc, v_temp[3], 0, vcc

			v_cmp_eq_u32	   vcc, v_pos, 0
			s_and_saveexec_b64 s_temp[0:1], vcc
			
			v_mov_b32		   lds_row, lds_matrix
			s_mov_b32		   s_olc, 0
			.read_lds_serial_loop_start:
				ds_read_b64	v_temp[0:1], lds_row offset:0
				s_waitcnt	lgkmcnt(0)
				flat_store_dwordx2 v_temp[2:3], v_temp[0:1]
				v_add_u32	lds_row, vcc, lds_row, 8
				v_add_u32	v_temp[2], vcc, v_temp[2], 8
				v_addc_u32	v_temp[3], vcc, v_temp[3], 0, vcc
				s_add_u32	s_olc, s_olc, 8
				s_waitcnt	vmcnt(0) & lgkmcnt(0)
				s_cmpk_eq_u32	s_olc, 1536
				s_cbranch_scc0 .read_lds_serial_loop_start
			s_mov_b64	exec, s_temp[0:1]
		.endm

				
				



			

		
		.macro WRITE_ELEMENT, src, col, col_offs
    		ds_write_b64		  lds_row, \src[(\col * 6) + 0:(\col * 6) + 1] offset:((0 * 8 * 4) +  ((\col_offs) * ELEMENT_SIZE))
    		ds_write_b64		  lds_row, \src[(\col * 6) + 2:(\col * 6) + 3] offset:((1 * 8 * 4) +  ((\col_offs) * ELEMENT_SIZE))
    		ds_write_b64		  lds_row, \src[(\col * 6) + 4:(\col * 6) + 5] offset:((2 * 8 * 4) +  ((\col_offs) * ELEMENT_SIZE))
		.endm

		.macro XOR_ELEMENT_WITH_STATE,  col_offs
    		ds_xor_b64		  lds_row, v_state[0:1] offset:(0 * 8 * 4) +  ((\col_offs) * ELEMENT_SIZE)
    		ds_xor_b64		  lds_row, v_state[2:3] offset:(1 * 8 * 4) +  ((\col_offs) * ELEMENT_SIZE)
    		ds_xor_b64		  lds_row, v_state[4:5] offset:(2 * 8 * 4) +  ((\col_offs) * ELEMENT_SIZE)
		.endm

		.macro STATE_XOR_TEMP, temp, col
			v_xor_b32		v_state[0], v_state[0], \temp[(\col * 6) + 0]
			v_xor_b32		v_state[1], v_state[1], \temp[(\col * 6) + 1]
			v_xor_b32		v_state[2], v_state[2], \temp[(\col * 6) + 2]
			v_xor_b32		v_state[3], v_state[3], \temp[(\col * 6) + 3]
			v_xor_b32		v_state[4], v_state[4], \temp[(\col * 6) + 4]
			v_xor_b32		v_state[5], v_state[5], \temp[(\col * 6) + 5]
		.endm

		.macro TEMP_XOR_STATE, temp, col
			v_xor_b32          \temp[(\col * 6) + 0], \temp[(\col * 6) + 0], v_state[0]
  			v_xor_b32          \temp[(\col * 6) + 1], \temp[(\col * 6) + 1], v_state[1]
  			v_xor_b32          \temp[(\col * 6) + 2], \temp[(\col * 6) + 2], v_state[2]
  			v_xor_b32          \temp[(\col * 6) + 3], \temp[(\col * 6) + 3], v_state[3]
  			v_xor_b32          \temp[(\col * 6) + 4], \temp[(\col * 6) + 4], v_state[4]
  			v_xor_b32          \temp[(\col * 6) + 5], \temp[(\col * 6) + 5], v_state[5]
  		.endm
		
		.macro TEMP_EQ_S1_PLUS_S2, temp, s1, s2, col
			v_add_u32		\temp[0], vcc, \s1[(\col * 6) + 0], \s2[(\col * 6) + 0]
			v_addc_u32		\temp[1], vcc, \s1[(\col * 6) + 1], \s2[(\col * 6) + 1], vcc
			v_add_u32		\temp[2], vcc, \s1[(\col * 6) + 2], \s2[(\col * 6) + 2]
			v_addc_u32		\temp[3], vcc, \s1[(\col * 6) + 3], \s2[(\col * 6) + 3], vcc
			v_add_u32		\temp[4], vcc, \s1[(\col * 6) + 4], \s2[(\col * 6) + 4]
			v_addc_u32		\temp[5], vcc, \s1[(\col * 6) + 5], \s2[(\col * 6) + 5], vcc
		.endm
		
		.macro TEMPC_EQ_S1_PLUS_S2, temp, s1, s2, col
			v_add_u32		\temp[(\col * 6) + 0], vcc, \s1[(\col * 6) + 0], \s2[(\col * 6) + 0]
			v_addc_u32		\temp[(\col * 6) + 1], vcc, \s1[(\col * 6) + 1], \s2[(\col * 6) + 1], vcc
			v_add_u32		\temp[(\col * 6) + 2], vcc, \s1[(\col * 6) + 2], \s2[(\col * 6) + 2]
			v_addc_u32		\temp[(\col * 6) + 3], vcc, \s1[(\col * 6) + 3], \s2[(\col * 6) + 3], vcc
			v_add_u32		\temp[(\col * 6) + 4], vcc, \s1[(\col * 6) + 4], \s2[(\col * 6) + 4]
			v_addc_u32		\temp[(\col * 6) + 5], vcc, \s1[(\col * 6) + 5], \s2[(\col * 6) + 5], vcc
		.endm

		.macro DANCE_OF_12, col
		# need a vtemp reg here but shit out of luck, so use v_temp[0] instead
		 v_cmp_eq_u32       vcc,  v_pos, 3             #pos == 3 ?
		 s_and_saveexec_b64 s_esave_l0[0:1], vcc
		 s_mov_b64          s_esave_l1[0:1], exec    # save pos0 exec mask for later
		
		 v_mov_b32          v_temp[0], v_state[0]          #pos3.temp.x = pos3.state[0].x
		 v_mov_b32          v_state[0], v_state[4]
		 v_mov_b32          v_state[4], v_state[2]
		 v_mov_b32          v_state[2], v_temp[0]

		 v_mov_b32          v_temp[0], v_state[1]          #pos3.temp.y = pos3.state[0].y
		 v_mov_b32          v_state[1], v_state[5]
		 v_mov_b32          v_state[5], v_state[3]
		 v_mov_b32          v_state[3], v_temp[0]
		
		 s_mov_b64          exec, s_esave_l0[0:1]

		 # want to enable GPR IDX offset for v_state2, so dest, and src1 (not src0 or src2) (4b'1010 = 0xA)
		 v_nop
		 v_nop
		 v_xor_b32          v_state2[(\col *6 ) + 0], v_state[0], v_state2[(\col * 6) + 0] quad_perm:[3, 0, 1, 2]
		 v_xor_b32          v_state2[(\col *6 ) + 1], v_state[1], v_state2[(\col * 6) + 1] quad_perm:[3, 0, 1, 2]
		 
		 v_nop
		 v_nop
		 v_xor_b32          v_state2[(\col * 6) + 2], v_state[2], v_state2[(\col * 6) + 2] quad_perm:[3, 0, 1, 2]
		 v_xor_b32          v_state2[(\col * 6) + 3], v_state[3], v_state2[(\col * 6) + 3] quad_perm:[3, 0, 1, 2]
		 
		 v_nop
		 v_nop
		 v_xor_b32          v_state2[(\col * 6) + 4], v_state[4], v_state2[(\col * 6) + 4] quad_perm:[3, 0, 1, 2]
		 v_xor_b32          v_state2[(\col * 6) + 5], v_state[5], v_state2[(\col * 6) + 5] quad_perm:[3, 0, 1, 2]
		
		 v_nop
		 v_nop
		 s_mov_b64          exec, s_esave_l1[0:1]
		 
		 #v_cmp_eq_u32 vcc,  v2, 3             #pos == 3 ?
		 #s_and_saveexec_b64 s[12:13], vcc
		
		 v_mov_b32          v_temp[0], v_state[2]    
		 v_mov_b32          v_state[2], v_state[4]  
		 v_mov_b32          v_state[4], v_state[0]  
		 v_mov_b32          v_state[0], v_temp[0]

		 v_mov_b32          v_temp[0], v_state[3]
		 v_mov_b32          v_state[3], v_state[5]
		 v_mov_b32          v_state[5], v_state[1]
		 v_mov_b32          v_state[1], v_temp[0]
		
		 s_mov_b64          exec, s_esave_l0[0:1]
		.endm


		.macro REDUCE_DUPLEX_F_COL, col
			STATE_XOR_TEMP	v_state1, \col
  			ROUND_LYRA
			TEMP_XOR_STATE  v_state1, \col
			WRITE_ELEMENT   v_state1, \col, (3 - \col)
		.endm
		
		.macro REDUCE_DUPLEX_ROW_SETUP_F
			v_mad_u32_u24		lds_row, v_rowSize, v_rowIn, lds_matrix
			READ_ElEMENT        v_state1, 0, 0
			READ_ElEMENT        v_state1, 1, 1
			READ_ElEMENT        v_state1, 2, 2
			READ_ElEMENT        v_state1, 3, 3
			v_mad_u32_u24		lds_row, v_rowSize, v_rowInOut, lds_matrix
			READ_ElEMENT 		v_state2, 0, 0
			READ_ElEMENT 		v_state2, 1, 1
			READ_ElEMENT 		v_state2, 2, 2
			READ_ElEMENT 		v_state2, 3, 3

			# tune wait time
			s_waitcnt           lgkmcnt(0)

			# col 0
			#state ^= (v_state1[col] + v_state2[col])
			TEMP_EQ_S1_PLUS_S2	v_temp, v_state1, v_state2, 0
			STATE_XOR_TEMP		v_temp, 0

			ROUND_LYRA

			#v_state1[col] ^= state
			#matrix[RowOut][3-col_sel] = v_state1[col]

			# select RowOut
			v_mad_u32_u24		lds_row, v_rowSize, v_rowOut, lds_matrix
			TEMP_XOR_STATE		v_state1, 0
			WRITE_ELEMENT		v_state1, 0, 3 

			# dance of 12 operates on state and v_state2 only
			DANCE_OF_12			0
			
			# tune wait time
			s_waitcnt           lgkmcnt(0)

			#matrix[RowInout][col] = v_state2[col]
			v_mad_u32_u24		lds_row, v_rowSize, v_rowInOut, lds_matrix
			WRITE_ELEMENT		v_state2, 0, 0
			
			# col 1
			TEMP_EQ_S1_PLUS_S2	v_temp, v_state1, v_state2, 1
			STATE_XOR_TEMP		v_temp, 0
			ROUND_LYRA
			v_mad_u32_u24		lds_row, v_rowSize, v_rowOut, lds_matrix
			TEMP_XOR_STATE		v_state1, 1
			WRITE_ELEMENT		v_state1, 1, 2 
			DANCE_OF_12			1
			s_waitcnt           lgkmcnt(0)
			v_mad_u32_u24		lds_row, v_rowSize, v_rowInOut, lds_matrix
			WRITE_ELEMENT		v_state2, 1, 1
			
			# col 2
			TEMP_EQ_S1_PLUS_S2	v_temp, v_state1, v_state2, 2
			STATE_XOR_TEMP		v_temp, 0
			ROUND_LYRA
			v_mad_u32_u24		lds_row, v_rowSize, v_rowOut, lds_matrix
			TEMP_XOR_STATE		v_state1, 2
			WRITE_ELEMENT		v_state1, 2, 1
			DANCE_OF_12			2
			s_waitcnt           lgkmcnt(0)
			v_mad_u32_u24		lds_row, v_rowSize, v_rowInOut, lds_matrix
			WRITE_ELEMENT		v_state2, 2, 2
			
			# col 3
			TEMP_EQ_S1_PLUS_S2	v_temp, v_state1, v_state2, 3
			STATE_XOR_TEMP		v_temp, 0
			ROUND_LYRA
			v_mad_u32_u24		lds_row, v_rowSize, v_rowOut, lds_matrix
			TEMP_XOR_STATE		v_state1, 3
			WRITE_ELEMENT		v_state1, 3, 0
			DANCE_OF_12			3
			s_waitcnt           lgkmcnt(0)
			v_mad_u32_u24		lds_row, v_rowSize, v_rowInOut, lds_matrix
			WRITE_ELEMENT		v_state2, 3, 3
		.endm
		
		.macro GET_ROWA
			v_mov_b32 v_rowInOut, v_state[0] quad_perm:[0, 0, 0, 0]
			v_and_b32 v_rowInOut, v_rowInOut, 0x3
		.endm
		
		.main:
		#######################################################################3
		# test code only
		 # s_load_dwordx2    s_state_mem[0:1],  s_args[0:1], 0x38   # state pointer
		 # s_load_dwordx2    s_matrix_mem[0:1], s_args[0:1], 0x40   # matrix pointer
		 # s_load_dwordx2    s_state1_mem[0:1], s_args[0:1], 0x48   # state1 pointer
		 # s_load_dwordx2    s_state2_mem[0:1], s_args[0:1], 0x50   # state2 pointer
		#######################################################################3
			s_load_dwordx2        s_temp[0:1], s_args[0:1], 0x30
			s_waitcnt             lgkmcnt(0)
			s_mov_b32			  m0, 0x7fff
			v_mov_b32             v_hashes_hidx[0], s_temp[0]
			v_mov_b32             v_hashes_hidx[1], s_temp[1]

			v_lshrrev_b32         v_teamidx, 2, v_localid                          # teamidx = localidx >> 2
			v_and_b32             v_pos, 0x3, v_localid                            # pos = localid & 0x3
			v_lshlrev_b32         v_temp[1], 4, s_groupid                          # group_offset = group_id * 16 (16 * 4 = 64) 
			v_or_b32              v_hidx, v_temp[1], v_teamidx                     # hidx = group_offset | teamidx

			v_lshlrev_b32         v_temp[0], HIDX_HASH_SHIFT, v_hidx
			v_lshlrev_b32         v_temp[1], POS_HASH_SHIFT, v_pos
			v_add_u32             v_temp[0], vcc, v_temp[0], v_temp[1]           # [hidx][pos] offset to be added to hashes*
			v_add_u32             v_hashes_hidx[0], vcc, v_hashes_hidx[0], v_temp[0]
			v_addc_u32            v_hashes_hidx[1], vcc, v_hashes_hidx[1], 0, vcc    #hashes[hidx][pos]
			v_mov_b32			  lds_matrix, 1664   # each team has LDs memory range of matrix + padding (1536 + 128)
			v_mad_u32_u24		  lds_matrix, lds_matrix, v_teamidx, v_temp[1]
			v_mov_b32			  v_rowSize, ROW_SIZE

		.sponge_init:
			flat_load_dwordx2     v_state[0:1], v_hashes_hidx[0:1]  

			v_mov_b32             v_temp[0], (.gdata)&0xffffffff   
			v_mov_b32             v_temp[1], (.gdata)>>32
			v_lshlrev_b32         v_temp[3], BLAKE_MASK_POS_SHIFT, v_pos
			v_add_u32			  v_temp[0], vcc, v_temp[0], v_temp[3]
			v_addc_u32			  v_temp[1], vcc, v_temp[1], 0, vcc

			v_add_u32			  v_state[4], vcc, BLAKE_OFFSET_0, v_temp[0]
			v_addc_u32			  v_state[5], vcc, v_temp[1], 0, vcc
			
			v_add_u32			  v_state[6], vcc, BLAKE_OFFSET_1, v_temp[0]
			v_addc_u32			  v_state[7], vcc, v_temp[1], 0, vcc

			v_add_u32			  v_mask[0], vcc, MASK_OFFSET_0, v_temp[0]
			v_addc_u32			  v_mask[1], vcc, v_temp[1], 0, vcc
			
			v_add_u32			  v_mask[2], vcc, MASK_OFFSET_1, v_temp[0]
			v_addc_u32			  v_mask[3], vcc, v_temp[1], 0, vcc


			flat_load_dwordx2	  v_state[4:5], v_state[4:5]
			flat_load_dwordx2 	  v_state[6:7], v_state[6:7]
			flat_load_dwordx2	  v_mask[0:1], v_mask[0:1]
			flat_load_dwordx2	  v_mask[2:3], v_mask[2:3]

			s_waitcnt             vmcnt(0) & lgkmcnt(0)

			v_mov_b32             v_state[2], v_state[0]
			v_mov_b32             v_state[3], v_state[1]


			ROUND_LYRA_12
			

			v_xor_b32			  v_state[0], v_state[0], v_mask[0]
			v_xor_b32			  v_state[1], v_state[1], v_mask[1]
			v_xor_b32			  v_state[2], v_state[2], v_mask[2]
			v_xor_b32			  v_state[3], v_state[3], v_mask[3]

			ROUND_LYRA_12



		.squeeze:
			v_mov_b32			  lds_row, lds_matrix
			WRITE_ELEMENT		  v_state, 0, 3
    		ROUND_LYRA
			WRITE_ELEMENT         v_state, 0, 2 
    		ROUND_LYRA
			WRITE_ELEMENT         v_state, 0, 1
    		ROUND_LYRA
			WRITE_ELEMENT         v_state, 0, 0
    		ROUND_LYRA
			s_waitcnt             lgkmcnt(0)
			

		.reduce_duplex_f: 

			# only operates on row[0] and row[1], just unroll it

			# load row[0] to v_state1
			READ_ElEMENT		   v_state1, 0, 0
			READ_ElEMENT		   v_state1, 1, 1
			READ_ElEMENT		   v_state1, 2, 2
			READ_ElEMENT		   v_state1, 3, 3
  			
  			# change LDS target to row[1]
  			v_add_u32		      lds_row, vcc, lds_matrix, v_rowSize

  			# wait for loads to v_state1 to complete
			s_waitcnt             lgkmcnt(0)

			STATE_XOR_TEMP	v_state1, 0 
  			ROUND_LYRA
			TEMP_XOR_STATE  v_state1, 0 
			WRITE_ELEMENT   v_state1, 0, 3
			
			STATE_XOR_TEMP	v_state1, 1
  			ROUND_LYRA
			TEMP_XOR_STATE  v_state1 ,1 
			WRITE_ELEMENT   v_state1, 1, 2
			
			STATE_XOR_TEMP	v_state1, 2 
  			ROUND_LYRA
			TEMP_XOR_STATE  v_state1, 2 
			WRITE_ELEMENT   v_state1, 2, 1
			
			STATE_XOR_TEMP	v_state1, 3
  			ROUND_LYRA
			TEMP_XOR_STATE  v_state1, 3 
			WRITE_ELEMENT   v_state1, 3, 0


		.reduce_duplex_row_setup_f:
			# prepare rowIn, rowInOut, and rowOut for reduce_duplex_row_setup_f
			v_mov_b32			  v_rowIn, 1
			v_mov_b32			  v_rowInOut, 0
			v_mov_b32			  v_rowOut, 2
			
			# wait for reduce_duplex_f to complete
			s_waitcnt             lgkmcnt(0)

			REDUCE_DUPLEX_ROW_SETUP_F
			v_mov_b32			  v_rowIn, 2
			v_mov_b32			  v_rowInOut, 1
			v_mov_b32			  v_rowOut, 3

			# wait for previous REDUCE_DUPLEX_ROW_SETUP_F to complete
			s_waitcnt             lgkmcnt(0)

			
			REDUCE_DUPLEX_ROW_SETUP_F

		.reduce_duplex_row_f:
			v_mov_b32			  v_rowIn, 3
			s_mov_b32			  s_olc, 0

			.rdrf_loop_start:
				# wait for REDUCE_DUPLEX_ROW_SETUP_F to complete / previous loop
				s_waitcnt             lgkmcnt(0)
				v_mov_b32			  v_rowOut, s_olc
				GET_ROWA

				# v_state1[col] = matrix[rowIn][col]
				v_mad_u32_u24		lds_row, v_rowSize, v_rowIn, lds_matrix
				READ_ElEMENT		v_state1, 0, 0
				READ_ElEMENT		v_state1, 1, 1
				READ_ElEMENT		v_state1, 2, 2
				READ_ElEMENT		v_state1, 3, 3
    			
    			# v_state2 = matrix[rowInOut]
				v_mad_u32_u24		lds_row, v_rowSize, v_rowInOut, lds_matrix
				READ_ElEMENT		v_state2, 0, 0
				READ_ElEMENT		v_state2, 1, 1
				READ_ElEMENT		v_state2, 2, 2
				READ_ElEMENT		v_state2, 3, 3

				s_waitcnt           lgkmcnt(0)

				TEMPC_EQ_S1_PLUS_S2  v_state1, v_state1, v_state2, 0
				TEMPC_EQ_S1_PLUS_S2  v_state1, v_state1, v_state2, 1
				TEMPC_EQ_S1_PLUS_S2  v_state1, v_state1, v_state2, 2
				TEMPC_EQ_S1_PLUS_S2  v_state1, v_state1, v_state2, 3

				#col 0
				STATE_XOR_TEMP 		v_state1, 0
				ROUND_LYRA
				DANCE_OF_12			0

				# if (rowInOut == rowOut)
				v_cmp_eq_u32		vcc, v_rowInOut, v_rowOut
				s_and_saveexec_b64	s_esave_l1[0:1], vcc
					## v_state2[col] ^= state
					TEMP_XOR_STATE	v_state2, 0
				# else
				s_not_b64			exec, exec
					## matrix[RowOut][col] ^= state
					v_mad_u32_u24		lds_row, v_rowSize, v_rowOut, lds_matrix
					XOR_ELEMENT_WITH_STATE	0
				s_mov_b64			exec, s_esave_l1[0:1]
				
				#col 1
				STATE_XOR_TEMP 		v_state1, 1
				ROUND_LYRA
				DANCE_OF_12			1

				# if (rowInOut == rowOut)
				v_cmp_eq_u32		vcc, v_rowInOut, v_rowOut
				s_and_saveexec_b64	s_esave_l1[0:1], vcc
					## v_state2[col] ^= state
					TEMP_XOR_STATE	v_state2, 1
				# else
				s_not_b64			exec, exec
					## matrix[RowOut][col] ^= state
					v_mad_u32_u24		lds_row, v_rowSize, v_rowOut, lds_matrix
					XOR_ELEMENT_WITH_STATE	1
				s_mov_b64			exec, s_esave_l1[0:1]

				#col 2
				STATE_XOR_TEMP 		v_state1, 2
				ROUND_LYRA
				DANCE_OF_12			2

				# if (rowInOut == rowOut)
				v_cmp_eq_u32		vcc, v_rowInOut, v_rowOut
				s_and_saveexec_b64	s_esave_l1[0:1], vcc
					## v_state2[col] ^= state
					TEMP_XOR_STATE	v_state2, 2
				# else
				s_not_b64			exec, exec
					## matrix[RowOut][col] ^= state
					v_mad_u32_u24		lds_row, v_rowSize, v_rowOut, lds_matrix
					XOR_ELEMENT_WITH_STATE	2
				s_mov_b64			exec, s_esave_l1[0:1]

				#col 3
				STATE_XOR_TEMP 		v_state1, 3
				ROUND_LYRA
				DANCE_OF_12			3

				# if (rowInOut == rowOut)
				v_cmp_eq_u32		vcc, v_rowInOut, v_rowOut
				s_and_saveexec_b64	s_esave_l1[0:1], vcc
					## v_state2[col] ^= state
					TEMP_XOR_STATE	v_state2, 3
				# else
				s_not_b64			exec, exec
					## matrix[RowOut][col] ^= state
					v_mad_u32_u24		lds_row, v_rowSize, v_rowOut, lds_matrix
					XOR_ELEMENT_WITH_STATE	3
				s_mov_b64			exec, s_esave_l1[0:1]
			
			.rdrf_loop_end:
				v_mad_u32_u24		lds_row, v_rowSize, v_rowInOut, lds_matrix
				WRITE_ELEMENT		v_state2, 0, 0
				WRITE_ELEMENT		v_state2, 1, 1
				WRITE_ELEMENT		v_state2, 2, 2
				WRITE_ELEMENT		v_state2, 3, 3
				v_mov_b32		v_rowIn, v_rowOut
				s_add_u32		s_olc, s_olc, 1
				s_cmpk_eq_u32	s_olc, 4
				s_cbranch_scc0	.rdrf_loop_start

		.lcfu:
			# at this point, v_state2[0]  contains matrix[rowA][0]
			# state.d8 ^= matrix[rowA][0]
			STATE_XOR_TEMP	v_state2, 0
			ROUND_LYRA_12

		.end_of_main:
			flat_store_dwordx2 v_hashes_hidx[0:1], v_state[0:1]
			s_waitcnt         vmcnt(0) & lgkmcnt(0)
			s_waitcnt          expcnt(0)
			s_endpgm
