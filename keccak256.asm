/* Disassembling 'AMDOCLBin-0.bin' */
.amdcl2
.gpu Ellesmere
.64bit
.arch_minor 0
.arch_stepping 4
.driver_version 203603
.compile_options "-I /home/turekaj/code/nextminer_working/nextminer/src -DFP_FAST_FMA=1 -cl-denorms-are-zero -m64 -Dcl_khr_fp64=1 -Dcl_amd_fp64=1 -Dcl_khr_global_int32_base_atomics=1 -Dcl_khr_global_int32_extended_atomics=1 -Dcl_khr_local_int32_base_atomics=1 -Dcl_khr_local_int32_extended_atomics=1 -Dcl_khr_int64_base_atomics=1 -Dcl_khr_int64_extended_atomics=1 -Dcl_khr_3d_image_writes=1 -Dcl_khr_byte_addressable_store=1 -Dcl_khr_fp16=1 -Dcl_khr_gl_sharing=1 -Dcl_amd_device_attribute_query=1 -Dcl_amd_vec3=1 -Dcl_amd_printf=1 -Dcl_amd_media_ops=1 -Dcl_amd_media_ops2=1 -Dcl_amd_popcnt=1 -Dcl_khr_image2d_from_buffer=1 -Dcl_khr_spir=1 -Dcl_khr_gl_event=1"
.acl_version "AMD-COMP-LIB-v0.8 (0.0.SC_BUILD_NUMBER)"
.globaldata
.kernel keccak256
    .config
		.dims x
		.cws 64, 1, 1
		.sgprsnum 20
		.vgprsnum 64
        .floatmode 0xFF
		.localsize 1
        .dx10clamp
        .ieeemode
        .useargs
        .priority 0
        .arg _.global_offset_0, "size_t", long
        .arg _.global_offset_1, "size_t", long
        .arg _.global_offset_2, "size_t", long
        .arg _.printf_buffer, "size_t", void*, global, , rdonly
        .arg _.vqueue_pointer, "size_t", long
        .arg _.aqlwrap_pointer, "size_t", long
        .arg hashes, "hash_t*", structure*, 32, global, 
    .text
		.set HIDX_HASH_SHIFT, 5

	#	openCL keccak compiles to 7885 instructions
	#   this file contains :
			#336 per ROUND_KECCAK1600 + 2 scalars for RC load * 24
			#24 instructions in main
			# (336 + 2) * 24 + 24 = 8088

		
		# helper symbols

		localid          = %v[0]
		hidx             = %v[1]
		hashes_hidx      = %v[2:3]
		state            = %v[4:53]
		v_temp			 = %v[54:63]

		userdata           = %s[0:6]
		args               = %s[4:5]
		groupid            = %s[6]
		stemp			   = %s[8:9]



		.macro ROUND_KECCAK_1600 rconst1, rconst0
			# form t0 with 0,1 										T0
			#t[0] = s[0] ^ s[5] ^ s[10] ^ s[15] ^ s[20];

			v_xor_b32	v_temp[0],  state[(2 *  0) + 0], state[(2 * 5) + 0]
			v_xor_b32	v_temp[1],  state[(2 * 10) + 0], state[(2 * 15) + 0]
			v_xor_b32	v_temp[1], v_temp[1], state[(2 * 20) + 0]
			v_xor_b32	v_temp[0], v_temp[0], v_temp[1]
			
			v_xor_b32	v_temp[1],  state[(2 *  0) + 1], state[(2 * 5) + 1]
			v_xor_b32	v_temp[2],  state[(2 * 10) + 1], state[(2 * 15) + 1]
			v_xor_b32	v_temp[2], v_temp[2], state[(2 * 20) + 1]
			v_xor_b32	v_temp[1], v_temp[1], v_temp[2]

			# form t3 using 2,3									    T3
			#t[3] = s[3] ^ s[8] ^ s[13] ^ s[18] ^ s[23];
			
			v_xor_b32	v_temp[2],  state[(2 *  3) + 0], state[(2 * 8) + 0]
			v_xor_b32	v_temp[3],  state[(2 * 13) + 0], state[(2 * 18) + 0]
			v_xor_b32	v_temp[3], v_temp[3], state[(2 * 23) + 0]
			v_xor_b32	v_temp[2], v_temp[2], v_temp[3]
			
			v_xor_b32	v_temp[3],  state[(2 *  3) + 1], state[(2 * 8) + 1]
			v_xor_b32	v_temp[4],  state[(2 * 13) + 1], state[(2 * 18) + 1]
			v_xor_b32	v_temp[4], v_temp[4], state[(2 * 23) + 1]
			v_xor_b32	v_temp[3], v_temp[3], v_temp[4]

			# form u4 using 4,5 (from t3 and t0)			
			#u[4] = t[3] ^ rotate(t[0], (ulong) 1);

			# rotate
			v_alignbit_b32	v_temp[5], v_temp[0], v_temp[1], (32 - 1)
			v_alignbit_b32	v_temp[4], v_temp[1], v_temp[0], (32 - 1)

			v_xor_b32		v_temp[4], v_temp[4], v_temp[2]
			v_xor_b32		v_temp[5], v_temp[5], v_temp[3]

			# update state
			#s[4] ^= u[4]; 
			v_xor_b32		state[(2 * 4) + 0], state[(2 * 4) + 0], v_temp[4]
			v_xor_b32		state[(2 * 4) + 1], state[(2 * 4) + 1], v_temp[5]
			#s[9] ^= u[4]; 
			v_xor_b32		state[(2 * 9) + 0], state[(2 * 9) + 0], v_temp[4]
			v_xor_b32		state[(2 * 9) + 1], state[(2 * 9) + 1], v_temp[5]
			#s[14] ^= u[4]; 
			v_xor_b32		state[(2 * 14) + 0], state[(2 * 14) + 0], v_temp[4]
			v_xor_b32		state[(2 * 14) + 1], state[(2 * 14) + 1], v_temp[5]
			#s[19] ^= u[4]; 
			v_xor_b32		state[(2 * 19) + 0], state[(2 * 19) + 0], v_temp[4]
			v_xor_b32		state[(2 * 19) + 1], state[(2 * 19) + 1], v_temp[5]
			#s[24] ^= u[4];
			v_xor_b32		state[(2 * 24) + 0], state[(2 * 24) + 0], v_temp[4]
			v_xor_b32		state[(2 * 24) + 1], state[(2 * 24) + 1], v_temp[5]

			# form t[2] using  4,5 									T2
			#t[2] = s[2] ^ s[7] ^ s[12] ^ s[17] ^ s[22];
			v_xor_b32	v_temp[4],  state[(2 *  2) + 0], state[(2 * 7) + 0]
			v_xor_b32	v_temp[5],  state[(2 * 12) + 0], state[(2 * 17) + 0]
			v_xor_b32	v_temp[5], v_temp[5], state[(2 * 22) + 0]
			v_xor_b32	v_temp[4], v_temp[4], v_temp[5]
			
			v_xor_b32	v_temp[5],  state[(2 *  2) + 1], state[(2 * 7) + 1]
			v_xor_b32	v_temp[6],  state[(2 * 12) + 1], state[(2 * 17) + 1]
			v_xor_b32	v_temp[6], v_temp[6], state[(2 * 22) + 1]
			v_xor_b32	v_temp[5], v_temp[5], v_temp[6]

			# form u[1] using 6,7
			#u[1] = t[0] ^ rotate(t[2], (ulong) 1);		
			v_alignbit_b32	v_temp[7], v_temp[4], v_temp[5], (32 - 1)
			v_alignbit_b32	v_temp[6], v_temp[5], v_temp[4], (32 - 1)

			v_xor_b32		v_temp[6], v_temp[6], v_temp[4]
			v_xor_b32		v_temp[7], v_temp[7], v_temp[5]

			# update state
			#s[1] ^= u[1]; 
			v_xor_b32		state[(2 * 1) + 0], state[(2 * 1) + 0], v_temp[6]
			v_xor_b32		state[(2 * 1) + 1], state[(2 * 1) + 1], v_temp[7]
			#s[6] ^= u[1]; 
			v_xor_b32		state[(2 * 6) + 0], state[(2 * 6) + 0], v_temp[6]
			v_xor_b32		state[(2 * 6) + 1], state[(2 * 6) + 1], v_temp[7]
			#s[11] ^= u[1]; 
			v_xor_b32		state[(2 * 11) + 0], state[(2 * 11) + 0], v_temp[6]
			v_xor_b32		state[(2 * 11) + 1], state[(2 * 11) + 1], v_temp[7]
			#s[16] ^= u[1]; 
			v_xor_b32		state[(2 * 16) + 0], state[(2 * 16) + 0], v_temp[6]
			v_xor_b32		state[(2 * 16) + 1], state[(2 * 16) + 1], v_temp[7]
			#s[21] ^= u[1];
			v_xor_b32		state[(2 * 21) + 0], state[(2 * 21) + 0], v_temp[6]
			v_xor_b32		state[(2 * 21) + 1], state[(2 * 21) + 1], v_temp[7]

			# t0 can now be dropped
			# form t1 with 0,1
			#t[1] = s[1] ^ s[6] ^ s[11] ^ s[16] ^ s[21];				T1
			v_xor_b32	v_temp[0],  state[(2 *  1) + 0], state[(2 * 6) + 0]
			v_xor_b32	v_temp[1],  state[(2 * 11) + 0], state[(2 * 16) + 0]
			v_xor_b32	v_temp[1], v_temp[1], state[(2 * 21) + 0]
			v_xor_b32	v_temp[0], v_temp[0], v_temp[1]
			
			v_xor_b32	v_temp[1],  state[(2 *  1) + 1], state[(2 * 6) + 1]
			v_xor_b32	v_temp[6],  state[(2 * 11) + 1], state[(2 * 16) + 1]
			v_xor_b32	v_temp[6], v_temp[6], state[(2 * 21) + 1]
			v_xor_b32	v_temp[1], v_temp[1], v_temp[6]

			# form u[2] using 6,7 
			#u[2] = t[1] ^ rotate(t[3], (ulong) 1);
			v_alignbit_b32	v_temp[7], v_temp[2], v_temp[3], (32 - 1)
			v_alignbit_b32	v_temp[6], v_temp[3], v_temp[2], (32 - 1)

			v_xor_b32		v_temp[6], v_temp[6], v_temp[0]
			v_xor_b32		v_temp[7], v_temp[7], v_temp[1]

			# update state
			#s[2] ^= u[2]; 
			v_xor_b32		state[(2 * 2) + 0], state[(2 * 2) + 0], v_temp[6]
			v_xor_b32		state[(2 * 2) + 1], state[(2 * 2) + 1], v_temp[7]
			#s[7] ^= u[2]; 
			v_xor_b32		state[(2 * 7) + 0], state[(2 * 7) + 0], v_temp[6]
			v_xor_b32		state[(2 * 7) + 1], state[(2 * 7) + 1], v_temp[7]
			#s[12] ^= u[2]; 
			v_xor_b32		state[(2 * 12) + 0], state[(2 * 12) + 0], v_temp[6]
			v_xor_b32		state[(2 * 12) + 1], state[(2 * 12) + 1], v_temp[7]
			#s[17] ^= u[2]; 
			v_xor_b32		state[(2 * 17) + 0], state[(2 * 17) + 0], v_temp[6]
			v_xor_b32		state[(2 * 17) + 1], state[(2 * 17) + 1], v_temp[7]
			#s[22] ^= u[2];
			v_xor_b32		state[(2 * 22) + 0], state[(2 * 22) + 0], v_temp[6]
			v_xor_b32		state[(2 * 22) + 1], state[(2 * 22) + 1], v_temp[7]

			# t3 can now be dropped
			# form t4 using 2,3
			#t[4] = s[4] ^ s[9] ^ s[14] ^ s[19] ^ s[24];
			v_xor_b32	v_temp[2],  state[(2 *  4) + 0], state[(2 * 9) + 0]
			v_xor_b32	v_temp[3],  state[(2 * 14) + 0], state[(2 * 19) + 0]
			v_xor_b32	v_temp[3], v_temp[3], state[(2 * 24) + 0]
			v_xor_b32	v_temp[2], v_temp[2], v_temp[3]
			
			v_xor_b32	v_temp[3],  state[(2 *  4) + 1], state[(2 * 9) + 1]
			v_xor_b32	v_temp[6],  state[(2 * 14) + 1], state[(2 * 19) + 1]
			v_xor_b32	v_temp[6], v_temp[6], state[(2 * 24) + 1]
			v_xor_b32	v_temp[3], v_temp[3], v_temp[6]

			# form u[0] using 6,7
			#u[0] = t[4] ^ rotate(t[1], (ulong) 1);
			v_alignbit_b32	v_temp[7], v_temp[0], v_temp[1], (32 - 1)
			v_alignbit_b32	v_temp[6], v_temp[1], v_temp[0], (32 - 1)

			v_xor_b32		v_temp[6], v_temp[6], v_temp[2]
			v_xor_b32		v_temp[7], v_temp[7], v_temp[3]

			# update state
			#s[0] ^= u[0]; 
			v_xor_b32		state[(2 * 0) + 0], state[(2 * 0) + 0], v_temp[6]
			v_xor_b32		state[(2 * 0) + 1], state[(2 * 0) + 1], v_temp[7]
			#s[5] ^= u[0]; 
			v_xor_b32		state[(2 * 5) + 0], state[(2 * 5) + 0], v_temp[6]
			v_xor_b32		state[(2 * 5) + 1], state[(2 * 5) + 1], v_temp[7]
			#s[10] ^= u[0]; 
			v_xor_b32		state[(2 * 10) + 0], state[(2 * 10) + 0], v_temp[6]
			v_xor_b32		state[(2 * 10) + 1], state[(2 * 10) + 1], v_temp[7]
			#s[15] ^= u[0]; 
			v_xor_b32		state[(2 * 15) + 0], state[(2 * 15) + 0], v_temp[6]
			v_xor_b32		state[(2 * 15) + 1], state[(2 * 15) + 1], v_temp[7]
			#s[20] ^= u[0];
			v_xor_b32		state[(2 * 20) + 0], state[(2 * 20) + 0], v_temp[6]
			v_xor_b32		state[(2 * 20) + 1], state[(2 * 20) + 1], v_temp[7]
			# form u[3] using 6, 7
			#u[3] = t[2] ^ rotate(t[4], (ulong) 1);
			v_alignbit_b32	v_temp[7], v_temp[2], v_temp[3], (32 - 1)
			v_alignbit_b32	v_temp[6], v_temp[3], v_temp[2], (32 - 1)

			v_xor_b32		v_temp[6], v_temp[6], v_temp[4]
			v_xor_b32		v_temp[7], v_temp[7], v_temp[5]
			# update state
			#s[3] ^= u[3]; 
			v_xor_b32		state[(2 * 3) + 0], state[(2 * 3) + 0], v_temp[6]
			v_xor_b32		state[(2 * 3) + 1], state[(2 * 3) + 1], v_temp[7]
			#s[8] ^= u[3]; 
			v_xor_b32		state[(2 * 8) + 0], state[(2 * 8) + 0], v_temp[6]
			v_xor_b32		state[(2 * 8) + 1], state[(2 * 8) + 1], v_temp[7]
			#s[13] ^= u[3]; 
			v_xor_b32		state[(2 * 13) + 0], state[(2 * 13) + 0], v_temp[6]
			v_xor_b32		state[(2 * 13) + 1], state[(2 * 13) + 1], v_temp[7]
			#s[18] ^= u[3]; 
			v_xor_b32		state[(2 * 18) + 0], state[(2 * 18) + 0], v_temp[6]
			v_xor_b32		state[(2 * 18) + 1], state[(2 * 18) + 1], v_temp[7]
			#s[23] ^= u[3];
			v_xor_b32		state[(2 * 23) + 0], state[(2 * 23) + 0], v_temp[6]
			v_xor_b32		state[(2 * 23) + 1], state[(2 * 23) + 1], v_temp[7]

			# form v using 0, 1
			#v = s[1];
			v_mov_b32	v_temp[0], state[0]
			v_mov_b32	v_temp[1], state[1]

			#s[1] = rotate(s[6], (ulong) 44);
			v_alignbit_b32	state[(2 * 1) + 1], state[(2 * 6) + 0], state[(2 * 6) + 1], (32 - (44 - 32))
			v_alignbit_b32	state[(2 * 1) + 0], state[(2 * 6) + 1], state[(2 * 6) + 0], (32 - (44 - 32))
			#s[6] = rotate(s[9], (ulong) 20);
			v_alignbit_b32	state[(2 * 6) + 0], state[(2 * 9) + 0], state[(2 * 9) + 1], (32 - 20)
			v_alignbit_b32	state[(2 * 6) + 1], state[(2 * 9) + 1], state[(2 * 9) + 0], (32 - 20)
			#s[9] = rotate(s[22],  (ulong) 61);
			v_alignbit_b32	state[(2 * 9) + 1], state[(2 * 22) + 0], state[(2 * 22) + 1], (32 - (61 - 32))
			v_alignbit_b32	state[(2 * 9) + 0], state[(2 * 22) + 1], state[(2 * 22) + 0], (32 - (61 - 32))
			#s[22] = rotate(s[14], (ulong) 39);
			v_alignbit_b32	state[(2 * 22) + 1], state[(2 * 14) + 0], state[(2 * 14) + 1], (32 - (39 - 32))
			v_alignbit_b32	state[(2 * 22) + 0], state[(2 * 14) + 1], state[(2 * 14) + 0], (32 - (39 - 32))
			#s[14] = rotate(s[20], (ulong) 18);
			v_alignbit_b32	state[(2 * 14) + 0], state[(2 * 20) + 0], state[(2 * 20) + 1], (32 - 18)
			v_alignbit_b32	state[(2 * 14) + 1], state[(2 * 20) + 1], state[(2 * 20) + 0], (32 - 18)
			#s[20] = rotate(s[2],  (ulong) 62);
			v_alignbit_b32	state[(2 * 20) + 1], state[(2 * 2) + 0], state[(2 * 2) + 1], (32 - (62 - 32))
			v_alignbit_b32	state[(2 * 20) + 0], state[(2 * 2) + 1], state[(2 * 2) + 0], (32 - (62 - 32))
			#s[2] = rotate(s[12],  (ulong) 43);
			v_alignbit_b32	state[(2 * 2) + 1], state[(2 * 12) + 0], state[(2 * 12) + 1], (32 - (43 - 32))
			v_alignbit_b32	state[(2 * 2) + 0], state[(2 * 12) + 1], state[(2 * 12) + 0], (32 - (43 - 32))
			#s[12] = rotate(s[13], (ulong) 25);
			v_alignbit_b32	state[(2 * 12) + 0], state[(2 * 13) + 0], state[(2 * 13) + 1], (32 - 25)
			v_alignbit_b32	state[(2 * 12) + 1], state[(2 * 13) + 1], state[(2 * 13) + 0], (32 - 25)
			#s[13] = rotate(s[19], (ulong) 8);
			v_alignbit_b32	state[(2 * 13) + 0], state[(2 * 19) + 0], state[(2 * 19) + 1], (32 - 8)
			v_alignbit_b32	state[(2 * 13) + 1], state[(2 * 19) + 1], state[(2 * 19) + 0], (32 - 8)
			#s[19] = rotate(s[23], (ulong) 56);
			v_alignbit_b32	state[(2 * 19) + 1], state[(2 * 23) + 0], state[(2 * 23) + 1], (32 - (56 - 32))
			v_alignbit_b32	state[(2 * 19) + 0], state[(2 * 23) + 1], state[(2 * 23) + 0], (32 - (56 - 32))
			#s[23] = rotate(s[15], (ulong) 41);
			v_alignbit_b32	state[(2 * 23) + 1], state[(2 * 15) + 0], state[(2 * 15) + 1], (32 - (41 - 32))
			v_alignbit_b32	state[(2 * 23) + 0], state[(2 * 15) + 1], state[(2 * 15) + 0], (32 - (41 - 32))
			#s[15] = rotate(s[4],  (ulong) 27);
			v_alignbit_b32	state[(2 * 15) + 0], state[(2 * 4) + 0], state[(2 * 4) + 1], (32 - 27)
			v_alignbit_b32	state[(2 * 15) + 1], state[(2 * 4) + 1], state[(2 * 4) + 0], (32 - 27)
			#s[4] = rotate(s[24],  (ulong) 14);
			v_alignbit_b32	state[(2 * 4) + 0], state[(2 * 24) + 0], state[(2 * 24) + 1], (32 - 14)
			v_alignbit_b32	state[(2 * 4) + 1], state[(2 * 24) + 1], state[(2 * 24) + 0], (32 - 14)
			#s[24] = rotate(s[21], (ulong) 2);
			v_alignbit_b32	state[(2 * 24) + 0], state[(2 * 21) + 0], state[(2 * 21) + 1], (32 - 2)
			v_alignbit_b32	state[(2 * 24) + 1], state[(2 * 21) + 1], state[(2 * 21) + 0], (32 - 2)
			#s[21] = rotate(s[8],  (ulong) 55);
			v_alignbit_b32	state[(2 * 21) + 1], state[(2 * 8) + 0], state[(2 * 8) + 1], (32 - (55 - 32))
			v_alignbit_b32	state[(2 * 21) + 0], state[(2 * 8) + 1], state[(2 * 8) + 0], (32 - (55 - 32))
			#s[8] = rotate(s[16],  (ulong) 45);
			v_alignbit_b32	state[(2 * 8) + 1], state[(2 * 16) + 0], state[(2 * 16) + 1], (32 - (45 - 32))
			v_alignbit_b32	state[(2 * 8) + 0], state[(2 * 16) + 1], state[(2 * 16) + 0], (32 - (45 - 32))
			#s[16] = rotate(s[5],  (ulong) 36);
			v_alignbit_b32	state[(2 * 16) + 1], state[(2 * 5) + 0], state[(2 * 5) + 1], (32 - (36 - 32))
			v_alignbit_b32	state[(2 * 16) + 0], state[(2 * 5) + 1], state[(2 * 5) + 0], (32 - (36 - 32))
			#s[5] = rotate(s[3],   (ulong) 28);
			v_alignbit_b32	state[(2 * 5) + 0], state[(2 * 3) + 0], state[(2 * 3) + 1], (32 - 28)
			v_alignbit_b32	state[(2 * 5) + 1], state[(2 * 3) + 1], state[(2 * 3) + 0], (32 - 28)
			#s[3] = rotate(s[18],  (ulong) 21);
			v_alignbit_b32	state[(2 * 3) + 0], state[(2 * 18) + 0], state[(2 * 18) + 1], (32 - 21)
			v_alignbit_b32	state[(2 * 3) + 1], state[(2 * 18) + 1], state[(2 * 18) + 0], (32 - 21)
			#s[18] = rotate(s[17], (ulong) 15);
			v_alignbit_b32	state[(2 * 18) + 0], state[(2 * 17) + 0], state[(2 * 17) + 1], (32 - 15)
			v_alignbit_b32	state[(2 * 18) + 1], state[(2 * 17) + 1], state[(2 * 17) + 0], (32 - 15)
			#s[17] = rotate(s[11], (ulong) 10);
			v_alignbit_b32	state[(2 * 17) + 0], state[(2 * 11) + 0], state[(2 * 11) + 1], (32 - 10)
			v_alignbit_b32	state[(2 * 17) + 1], state[(2 * 11) + 1], state[(2 * 11) + 0], (32 - 10)
			#s[11] = rotate(s[7],  (ulong) 6);
			v_alignbit_b32	state[(2 * 11) + 0], state[(2 * 7) + 0], state[(2 * 7) + 1], (32 - 6)
			v_alignbit_b32	state[(2 * 11) + 1], state[(2 * 7) + 1], state[(2 * 7) + 0], (32 - 6)
			#s[7] = rotate(s[10],  (ulong) 3);
			v_alignbit_b32	state[(2 * 7) + 0], state[(2 * 10) + 0], state[(2 * 10) + 1], (32 - 3)
			v_alignbit_b32	state[(2 * 7) + 1], state[(2 * 10) + 1], state[(2 * 10) + 0], (32 - 3)
			#s[10] = rotate(v,     (ulong) 1);
			v_alignbit_b32	state[(2 * 10) + 0], v_temp[0], v_temp[1], (32 - 1)
			v_alignbit_b32	state[(2 * 10) + 1], v_temp[1], v_temp[0], (32 - 1)

			# v formed with 0,1
			#v = s[0];  
			v_mov_b32	v_temp[(2 * 0) + 0], state[(2 * 0) + 0]
			v_mov_b32	v_temp[(2 * 0) + 1], state[(2 * 0) + 1]
			# w formed with 2,3
			#w = s[1];  
			v_mov_b32	v_temp[(2 * 1) + 0], state[(2 * 1) + 0]
			v_mov_b32	v_temp[(2 * 1) + 1], state[(2 * 1) + 1]
			# use temp 4,5 to create partial to be xored 
			#s[0] ^= (~w) & s[2]; 
			v_not_b32	v_temp[(2 * 2) + 0], v_temp[(2 * 1) + 0]
			v_not_b32	v_temp[(2 * 2) + 1], v_temp[(2 * 1) + 1]
			v_and_b32	v_temp[(2 * 2) + 0], v_temp[(2 * 2) + 0], state[(2 * 2) + 0]
			v_and_b32	v_temp[(2 * 2) + 1], v_temp[(2 * 2) + 1], state[(2 * 2) + 1]
			v_xor_b32	state[(2 * 0) + 0], state[(2 * 0) + 0], v_temp[(2 * 2) + 0]
			v_xor_b32	state[(2 * 0) + 1], state[(2 * 1) + 0], v_temp[(2 * 2) + 1]
			#s[1] ^= (~s[2]) & s[3]; 
			v_not_b32	v_temp[(2 * 2) + 0], state[(2 * 2) + 0]
			v_not_b32	v_temp[(2 * 2) + 1], state[(2 * 2) + 1]
			v_and_b32	v_temp[(2 * 2) + 0], v_temp[(2 * 2) + 0], state[(2 * 3) + 0]
			v_and_b32	v_temp[(2 * 2) + 1], v_temp[(2 * 2) + 1], state[(2 * 3) + 1]
			v_xor_b32	state[(2 * 1) + 0], state[(2 * 1) + 0], v_temp[(2 * 2) + 0]
			v_xor_b32	state[(2 * 1) + 1], state[(2 * 1) + 1], v_temp[(2 * 2) + 1]
			#s[2] ^= (~s[3]) & s[4]; 
			v_not_b32	v_temp[(2 * 2) + 0], state[(2 * 3) + 0]
			v_not_b32	v_temp[(2 * 2) + 1], state[(2 * 3) + 1]
			v_and_b32	v_temp[(2 * 2) + 0], v_temp[(2 * 2) + 0], state[(2 * 4) + 0]
			v_and_b32	v_temp[(2 * 2) + 1], v_temp[(2 * 2) + 1], state[(2 * 4) + 1]
			v_xor_b32	state[(2 * 2) + 0], state[(2 * 2) + 0], v_temp[(2 * 2) + 0]
			v_xor_b32	state[(2 * 2) + 1], state[(2 * 2) + 1], v_temp[(2 * 2) + 1]
			#s[3] ^= (~s[4]) & v; 
			v_not_b32	v_temp[(2 * 2) + 0], state[(2 * 4) + 0]
			v_not_b32	v_temp[(2 * 2) + 1], state[(2 * 4) + 1]
			v_and_b32	v_temp[(2 * 2) + 0], v_temp[(2 * 2) + 0], v_temp[(2 * 0) + 0] 
			v_and_b32	v_temp[(2 * 2) + 1], v_temp[(2 * 2) + 1], v_temp[(2 * 0) + 1]
			v_xor_b32	state[(2 * 3) + 0], state[(2 * 3) + 0], v_temp[(2 * 2) + 0]
			v_xor_b32	state[(2 * 3) + 1], state[(2 * 3) + 1], v_temp[(2 * 2) + 1]
			#s[4] ^= (~v) & w;
			v_not_b32	v_temp[(2 * 2) + 0], v_temp[(2 * 0) + 0]
			v_not_b32	v_temp[(2 * 2) + 1], v_temp[(2 * 0) + 1]
			v_and_b32	v_temp[(2 * 2) + 0], v_temp[(2 * 2) + 0], v_temp[(2 * 1) + 0] 
			v_and_b32	v_temp[(2 * 2) + 1], v_temp[(2 * 2) + 1], v_temp[(2 * 1) + 1]
			v_xor_b32	state[(2 * 4) + 0], state[(2 * 4) + 0], v_temp[(2 * 2) + 0]
			v_xor_b32	state[(2 * 4) + 1], state[(2 * 4) + 1], v_temp[(2 * 2) + 1]

			#v = s[5];  
			v_mov_b32	v_temp[(2 * 0) + 0], state[(2 * 5) + 0]
			v_mov_b32	v_temp[(2 * 0) + 1], state[(2 * 5) + 1]
			#w = s[6];  
			v_mov_b32	v_temp[(2 * 1) + 0], state[(2 * 6) + 0]
			v_mov_b32	v_temp[(2 * 1) + 1], state[(2 * 6) + 1]
			#s[5] ^= (~w) & s[7]; 
			v_not_b32	v_temp[(2 * 2) + 0], v_temp[(2 * 1) + 0]
			v_not_b32	v_temp[(2 * 2) + 1], v_temp[(2 * 1) + 1]
			v_and_b32	v_temp[(2 * 2) + 0], v_temp[(2 * 2) + 0], state[(2 * 7) + 0]
			v_and_b32	v_temp[(2 * 2) + 1], v_temp[(2 * 2) + 1], state[(2 * 7) + 1]
			v_xor_b32	state[(2 * 5) + 0], state[(2 * 5) + 0], v_temp[(2 * 2) + 0]
			v_xor_b32	state[(2 * 5) + 1], state[(2 * 5) + 1], v_temp[(2 * 2) + 1]
			#s[6] ^= (~s[7]) & s[8]; 
			v_not_b32	v_temp[(2 * 2) + 0], state[(2 * 7) + 0]
			v_not_b32	v_temp[(2 * 2) + 1], state[(2 * 7) + 1]
			v_and_b32	v_temp[(2 * 2) + 0], v_temp[(2 * 2) + 0], state[(2 * 8) + 0]
			v_and_b32	v_temp[(2 * 2) + 1], v_temp[(2 * 2) + 1], state[(2 * 8) + 1]
			v_xor_b32	state[(2 * 6) + 0], state[(2 * 6) + 0], v_temp[(2 * 2) + 0]
			v_xor_b32	state[(2 * 6) + 1], state[(2 * 6) + 1], v_temp[(2 * 2) + 1]
			#s[7] ^= (~s[8]) & s[9]; 
			v_not_b32	v_temp[(2 * 2) + 0], state[(2 * 8) + 0]
			v_not_b32	v_temp[(2 * 2) + 1], state[(2 * 8) + 1]
			v_and_b32	v_temp[(2 * 2) + 0], v_temp[(2 * 2) + 0], state[(2 * 9) + 0]
			v_and_b32	v_temp[(2 * 2) + 1], v_temp[(2 * 2) + 1], state[(2 * 9) + 1]
			v_xor_b32	state[(2 * 7) + 0], state[(2 * 7) + 0], v_temp[(2 * 2) + 0]
			v_xor_b32	state[(2 * 7) + 1], state[(2 * 7) + 1], v_temp[(2 * 2) + 1]
			#s[8] ^= (~s[9]) & v; 
			v_not_b32	v_temp[(2 * 2) + 0], state[(2 * 9) + 0]
			v_not_b32	v_temp[(2 * 2) + 1], state[(2 * 9) + 1]
			v_and_b32	v_temp[(2 * 2) + 0], v_temp[(2 * 2) + 0], v_temp[(2 * 0) + 0]
			v_and_b32	v_temp[(2 * 2) + 1], v_temp[(2 * 2) + 1], v_temp[(2 * 0) + 1]
			v_xor_b32	state[(2 * 8) + 0], state[(2 * 8) + 0], v_temp[(2 * 2) + 0]
			v_xor_b32	state[(2 * 8) + 1], state[(2 * 8) + 1], v_temp[(2 * 2) + 1]
			#s[9] ^= (~v) & w;
			v_not_b32	v_temp[(2 * 2) + 0], v_temp[(2 * 0) + 0]
			v_not_b32	v_temp[(2 * 2) + 1], v_temp[(2 * 0) + 1]
			v_and_b32	v_temp[(2 * 2) + 0], v_temp[(2 * 2) + 0], v_temp[(2 * 1) + 0]
			v_and_b32	v_temp[(2 * 2) + 1], v_temp[(2 * 2) + 1], v_temp[(2 * 1) + 1]
			v_xor_b32	state[(2 * 9) + 0], state[(2 * 9) + 0], v_temp[(2 * 2) + 0]
			v_xor_b32	state[(2 * 9) + 1], state[(2 * 9) + 1], v_temp[(2 * 2) + 1]

			#v = s[10]; 
			v_mov_b32	v_temp[(2 * 0) + 0], state[(2 * 10) + 0]
			v_mov_b32	v_temp[(2 * 0) + 1], state[(2 * 10) + 1]
			#w = s[11]; 
			v_mov_b32	v_temp[(2 * 1) + 0], state[(2 * 11) + 0]
			v_mov_b32	v_temp[(2 * 1) + 1], state[(2 * 11) + 1]
			#s[10] ^= (~w) & s[12]; 
			v_not_b32	v_temp[(2 * 2) + 0], v_temp[(2 * 1) + 0]
			v_not_b32	v_temp[(2 * 2) + 1], v_temp[(2 * 1) + 1]
			v_and_b32	v_temp[(2 * 2) + 0], v_temp[(2 * 2) + 0], state[(2 * 12) + 0]
			v_and_b32	v_temp[(2 * 2) + 1], v_temp[(2 * 2) + 1], state[(2 * 12) + 1]
			v_xor_b32	state[(2 * 10) + 0], state[(2 * 10) + 0], v_temp[(2 * 2) + 0]
			v_xor_b32	state[(2 * 10) + 1], state[(2 * 10) + 1], v_temp[(2 * 2) + 1]
			#s[11] ^= (~s[12]) & s[13]; 
			v_not_b32	v_temp[(2 * 2) + 0], state[(2 * 12) + 0]
			v_not_b32	v_temp[(2 * 2) + 1], state[(2 * 12) + 1]
			v_and_b32	v_temp[(2 * 2) + 0], v_temp[(2 * 2) + 0], state[(2 * 13) + 0]
			v_and_b32	v_temp[(2 * 2) + 1], v_temp[(2 * 2) + 1], state[(2 * 13) + 1]
			v_xor_b32	state[(2 * 11) + 0], state[(2 * 11) + 0], v_temp[(2 * 2) + 0]
			v_xor_b32	state[(2 * 11) + 1], state[(2 * 11) + 1], v_temp[(2 * 2) + 1]
			#s[12] ^= (~s[13]) & s[14]; 
			v_not_b32	v_temp[(2 * 2) + 0], state[(2 * 13) + 0]
			v_not_b32	v_temp[(2 * 2) + 1], state[(2 * 13) + 1]
			v_and_b32	v_temp[(2 * 2) + 0], v_temp[(2 * 2) + 0], state[(2 * 14) + 0]
			v_and_b32	v_temp[(2 * 2) + 1], v_temp[(2 * 2) + 1], state[(2 * 14) + 1]
			v_xor_b32	state[(2 * 12) + 0], state[(2 * 12) + 0], v_temp[(2 * 2) + 0]
			v_xor_b32	state[(2 * 12) + 1], state[(2 * 12) + 1], v_temp[(2 * 2) + 1]
			#s[13] ^= (~s[14]) & v; 
			v_not_b32	v_temp[(2 * 2) + 0], state[(2 * 14) + 0]
			v_not_b32	v_temp[(2 * 2) + 1], state[(2 * 14) + 1]
			v_and_b32	v_temp[(2 * 2) + 0], v_temp[(2 * 2) + 0], v_temp[(2 * 0) + 0] 
			v_and_b32	v_temp[(2 * 2) + 1], v_temp[(2 * 2) + 1], v_temp[(2 * 0) + 1]
			v_xor_b32	state[(2 * 13) + 0], state[(2 * 13) + 0], v_temp[(2 * 2) + 0]
			v_xor_b32	state[(2 * 13) + 1], state[(2 * 13) + 1], v_temp[(2 * 2) + 1]
			#s[14] ^= (~v) & w;
			v_not_b32	v_temp[(2 * 2) + 0], v_temp[(2 * 0) + 0]
			v_not_b32	v_temp[(2 * 2) + 1], v_temp[(2 * 0) + 1]
			v_and_b32	v_temp[(2 * 2) + 0], v_temp[(2 * 2) + 0], v_temp[(2 * 1) + 0]
			v_and_b32	v_temp[(2 * 2) + 1], v_temp[(2 * 2) + 1], v_temp[(2 * 1) + 1]
			v_xor_b32	state[(2 * 14) + 0], state[(2 * 14) + 0], v_temp[(2 * 2) + 0]
			v_xor_b32	state[(2 * 14) + 1], state[(2 * 14) + 1], v_temp[(2 * 2) + 1]

			#v = s[15]; 
			v_mov_b32	v_temp[(2 * 0) + 0], state[(2 * 15) + 0]
			v_mov_b32	v_temp[(2 * 0) + 1], state[(2 * 15) + 1]
			#w = s[16]; 
			v_mov_b32	v_temp[(2 * 1) + 0], state[(2 * 16) + 0]
			v_mov_b32	v_temp[(2 * 1) + 1], state[(2 * 16) + 1]
			#s[15] ^= (~w) & s[17]; 
			v_not_b32	v_temp[(2 * 2) + 0], v_temp[(2 * 1) + 0]
			v_not_b32	v_temp[(2 * 2) + 1], v_temp[(2 * 1) + 1]
			v_and_b32	v_temp[(2 * 2) + 0], v_temp[(2 * 2) + 0], state[(2 * 17) + 0]
			v_and_b32	v_temp[(2 * 2) + 1], v_temp[(2 * 2) + 1], state[(2 * 17) + 1]
			v_xor_b32	state[(2 * 15) + 0], state[(2 * 15) + 0], v_temp[(2 * 2) + 0]
			v_xor_b32	state[(2 * 15) + 1], state[(2 * 15) + 1], v_temp[(2 * 2) + 1]
			#s[16] ^= (~s[17]) & s[18]; 
			v_not_b32	v_temp[(2 * 2) + 0], state[(2 * 17) + 0]
			v_not_b32	v_temp[(2 * 2) + 1], state[(2 * 17) + 1]
			v_and_b32	v_temp[(2 * 2) + 0], v_temp[(2 * 2) + 0], state[(2 * 18) + 0]
			v_and_b32	v_temp[(2 * 2) + 1], v_temp[(2 * 2) + 1], state[(2 * 18) + 1]
			v_xor_b32	state[(2 * 16) + 0], state[(2 * 16) + 0], v_temp[(2 * 2) + 0]
			v_xor_b32	state[(2 * 16) + 1], state[(2 * 16) + 1], v_temp[(2 * 2) + 1]
			#s[17] ^= (~s[18]) & s[19]; 
			v_not_b32	v_temp[(2 * 2) + 0], state[(2 * 18) + 0]
			v_not_b32	v_temp[(2 * 2) + 1], state[(2 * 18) + 1]
			v_and_b32	v_temp[(2 * 2) + 0], v_temp[(2 * 2) + 0], state[(2 * 19) + 0]
			v_and_b32	v_temp[(2 * 2) + 1], v_temp[(2 * 2) + 1], state[(2 * 19) + 1]
			v_xor_b32	state[(2 * 17) + 0], state[(2 * 17) + 0], v_temp[(2 * 2) + 0]
			v_xor_b32	state[(2 * 17) + 1], state[(2 * 17) + 1], v_temp[(2 * 2) + 1]
			#s[18] ^= (~s[19]) & v; 
			v_not_b32	v_temp[(2 * 2) + 0], state[(2 * 19) + 0]
			v_not_b32	v_temp[(2 * 2) + 1], state[(2 * 19) + 1]
			v_and_b32	v_temp[(2 * 2) + 0], v_temp[(2 * 2) + 0], v_temp[(2 * 0) + 0]
			v_and_b32	v_temp[(2 * 2) + 1], v_temp[(2 * 2) + 1], v_temp[(2 * 0) + 1]
			v_xor_b32	state[(2 * 18) + 0], state[(2 * 18) + 0], v_temp[(2 * 2) + 0]
			v_xor_b32	state[(2 * 18) + 1], state[(2 * 18) + 1], v_temp[(2 * 2) + 1]
			#s[19] ^= (~v) & w;
			v_not_b32	v_temp[(2 * 2) + 0], v_temp[(2 * 0) + 0]
			v_not_b32	v_temp[(2 * 2) + 1], v_temp[(2 * 0) + 1]
			v_and_b32	v_temp[(2 * 2) + 0], v_temp[(2 * 2) + 0], v_temp[(2 * 1) + 0]
			v_and_b32	v_temp[(2 * 2) + 1], v_temp[(2 * 2) + 1], v_temp[(2 * 1) + 1]
			v_xor_b32	state[(2 * 19) + 0], state[(2 * 19) + 0], v_temp[(2 * 2) + 0]
			v_xor_b32	state[(2 * 19) + 1], state[(2 * 19) + 1], v_temp[(2 * 2) + 1]
			
			#v = s[20]; 
			v_mov_b32	v_temp[(2 * 0) + 0], state[(2 * 20) + 0]
			v_mov_b32	v_temp[(2 * 0) + 1], state[(2 * 20) + 1]
			#w = s[21]; 
			v_mov_b32	v_temp[(2 * 1) + 0], state[(2 * 21) + 0]
			v_mov_b32	v_temp[(2 * 1) + 1], state[(2 * 21) + 1]
			#s[20] ^= (~w) & s[22]; 
			v_not_b32	v_temp[(2 * 2) + 0], v_temp[(2 * 1) + 0]
			v_not_b32	v_temp[(2 * 2) + 1], v_temp[(2 * 1) + 1]
			v_and_b32	v_temp[(2 * 2) + 0], v_temp[(2 * 2) + 0], state[(2 * 22) + 0]
			v_and_b32	v_temp[(2 * 2) + 1], v_temp[(2 * 2) + 1], state[(2 * 22) + 1]
			v_xor_b32	state[(2 * 20) + 0], state[(2 * 20) + 0], v_temp[(2 * 2) + 0]
			v_xor_b32	state[(2 * 20) + 1], state[(2 * 20) + 1], v_temp[(2 * 2) + 1]
			#s[21] ^= (~s[22]) & s[23]; 
			v_not_b32	v_temp[(2 * 2) + 0], state[(2 * 22) + 0]
			v_not_b32	v_temp[(2 * 2) + 1], state[(2 * 22) + 1]
			v_and_b32	v_temp[(2 * 2) + 0], v_temp[(2 * 2) + 0], state[(2 * 23) + 0]
			v_and_b32	v_temp[(2 * 2) + 1], v_temp[(2 * 2) + 1], state[(2 * 23) + 1]
			v_xor_b32	state[(2 * 21) + 0], state[(2 * 21) + 0], v_temp[(2 * 2) + 0]
			v_xor_b32	state[(2 * 21) + 1], state[(2 * 21) + 1], v_temp[(2 * 2) + 1]
			#s[22] ^= (~s[23]) & s[24]; 
			v_not_b32	v_temp[(2 * 2) + 0], state[(2 * 23) + 0]
			v_not_b32	v_temp[(2 * 2) + 1], state[(2 * 23) + 1]
			v_and_b32	v_temp[(2 * 2) + 0], v_temp[(2 * 2) + 0], state[(2 * 24) + 0]
			v_and_b32	v_temp[(2 * 2) + 1], v_temp[(2 * 2) + 1], state[(2 * 24) + 1]
			v_xor_b32	state[(2 * 22) + 0], state[(2 * 22) + 0], v_temp[(2 * 2) + 0]
			v_xor_b32	state[(2 * 22) + 1], state[(2 * 22) + 1], v_temp[(2 * 2) + 1]
			#s[23] ^= (~s[24]) & v; 
			v_not_b32	v_temp[(2 * 2) + 0], state[(2 * 24) + 0]
			v_not_b32	v_temp[(2 * 2) + 1], state[(2 * 24) + 1]
			v_and_b32	v_temp[(2 * 2) + 0], v_temp[(2 * 2) + 0], v_temp[(2 * 0) + 0]
			v_and_b32	v_temp[(2 * 2) + 1], v_temp[(2 * 2) + 1], v_temp[(2 * 0) + 1]
			v_xor_b32	state[(2 * 23) + 0], state[(2 * 23) + 0], v_temp[(2 * 2) + 0]
			v_xor_b32	state[(2 * 23) + 1], state[(2 * 23) + 1], v_temp[(2 * 2) + 1]
			#s[24] ^= (~v) & w;
			v_not_b32	v_temp[(2 * 2) + 0], v_temp[(2 * 0) + 0]
			v_not_b32	v_temp[(2 * 2) + 1], v_temp[(2 * 0) + 1]
			v_and_b32	v_temp[(2 * 2) + 0], v_temp[(2 * 2) + 0], v_temp[(2 * 1) + 0]
			v_and_b32	v_temp[(2 * 2) + 1], v_temp[(2 * 2) + 1], v_temp[(2 * 1) + 1]
			v_xor_b32	state[(2 * 24) + 0], state[(2 * 24) + 0], v_temp[(2 * 2) + 0]
			v_xor_b32	state[(2 * 24) + 1], state[(2 * 24) + 1], v_temp[(2 * 2) + 1]

			#s[0] ^= RC[i];
			v_xor_b32 	state[0], \rconst0, state[0]
			v_xor_b32 	state[1], \rconst1, state[1]
		.endm


		.keccak_main:
			# argument parsing
			s_load_dwordx2		stemp[0:1], args[0:1], 0x30
			s_waitcnt			lgkmcnt(0)
			v_mov_b32			hashes_hidx[0], stemp[0]
			v_mov_b32			hashes_hidx[1], stemp[1]

			# hidx determination
			v_lshlrev_b32		state[0], 6, groupid
			v_or_b32			hidx, state[0], localid

			# input/output pointers
			v_lshlrev_b32		state[1], HIDX_HASH_SHIFT, hidx
			v_add_u32			hashes_hidx[0], vcc, hashes_hidx[0], state[1]
			v_addc_u32			hashes_hidx[1], vcc, hashes_hidx[1], 0, vcc
			
			# sponge init
			flat_load_dwordx4	state[0:3], hashes_hidx[0:1]
			v_add_u32			v_temp[0], vcc, hashes_hidx[0], 16
			v_addc_u32			v_temp[1], vcc, hashes_hidx[1], 0, vcc
			flat_load_dwordx4	state[4:7], v_temp[0:1]
			v_mov_b32			state[8],  0x00000001
			v_mov_b32			state[9],  0x00000000
			v_mov_b32			state[32], 0x00000000
			v_mov_b32			state[33], 0x80000000
			s_waitcnt			lgkmcnt(0)
  			
  			ROUND_KECCAK_1600 0x00000000, 0x00000001 
		    ROUND_KECCAK_1600 0x00000000, 0x00008082
		    ROUND_KECCAK_1600 0x80000000, 0x0000808A 
		    ROUND_KECCAK_1600 0x80000000, 0x80008000
		    ROUND_KECCAK_1600 0x00000000, 0x0000808B 
		    ROUND_KECCAK_1600 0x00000000, 0x80000001
		    ROUND_KECCAK_1600 0x80000000, 0x80008081 
		    ROUND_KECCAK_1600 0x80000000, 0x00008009
		    ROUND_KECCAK_1600 0x00000000, 0x0000008A 
		    ROUND_KECCAK_1600 0x00000000, 0x00000088
		    ROUND_KECCAK_1600 0x00000000, 0x80008009 
		    ROUND_KECCAK_1600 0x00000000, 0x8000000A
		    ROUND_KECCAK_1600 0x00000000, 0x8000808B 
		    ROUND_KECCAK_1600 0x80000000, 0x0000008B
		    ROUND_KECCAK_1600 0x80000000, 0x00008089 
		    ROUND_KECCAK_1600 0x80000000, 0x00008003
		    ROUND_KECCAK_1600 0x80000000, 0x00008002 
		    ROUND_KECCAK_1600 0x80000000, 0x00000080
		    ROUND_KECCAK_1600 0x00000000, 0x0000800A 
		    ROUND_KECCAK_1600 0x80000000, 0x8000000A
		    ROUND_KECCAK_1600 0x80000000, 0x80008081 
		    ROUND_KECCAK_1600 0x80000000, 0x00008080
		    ROUND_KECCAK_1600 0x00000000, 0x80000001 
		    ROUND_KECCAK_1600 0x80000000, 0x80008008

  			s_branch	.end_of_keccak
		

		.end_of_keccak:
			flat_store_dwordx4	hashes_hidx[0:1], state[0:3]
			v_add_u32			hashes_hidx[0], vcc, hashes_hidx[0], 16
			v_addc_u32			hashes_hidx[1], vcc, hashes_hidx[1], 0, vcc
			flat_store_dwordx4	hashes_hidx[0:1], state[4:7]
			s_waitcnt           vmcnt(0) 
			s_endpgm
